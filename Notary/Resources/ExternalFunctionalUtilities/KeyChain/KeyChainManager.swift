//
//  KeyChainManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 31/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class KeyChainManager {
    
    static var sharedInstance = KeyChainManager()
    
    let itemKey = Utility.emailId
    let itemValue = "3Embed007"
    let keychainAccessGroupName = "5A693V6KR5.KCDemo"
    
    
    
    /// Method to save user credentials in Keychain
    ///
    /// - Parameters:
    ///   - password: password to save
    ///   - success: success result block
    ///   - failure: failure result block
    func saveUserPassword(password:String,
                          success: @escaping(Bool) -> Void,
                          failure: @escaping(Error) -> Void) {
        
        guard let valueData = password.data(using: String.Encoding.utf8) else {
            print("Error saving text to Keychain")
            return
        }
        
        let queryAdd: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecValueData as String: valueData as AnyObject,
            kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked,
            kSecAttrAccessGroup as String: keychainAccessGroupName as AnyObject
        ]
        
        let resultCode = SecItemAdd(queryAdd as CFDictionary, nil)
        
        if resultCode != noErr {
            
            print("Error saving to Keychain: \(resultCode)")
            
            let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey       : NSLocalizedString("Error saving to Keychain", comment: "Error saving to Keychain"),
                                                NSLocalizedFailureReasonErrorKey: NSLocalizedString("Error saving to Keychain", comment: "Error saving to Keychain")]
            
            let error:NSError = NSError(domain: "", code: Int(resultCode), userInfo: userInfo)
            
            failure(error)
        }
        
        success(true)

    }
    
    
    
    /// Method to get saved user details from keychain
    ///
    /// - Parameters:
    ///   - success: success result block
    ///   - failure: failure result block
    func getUserPassword(success: @escaping(String) -> Void,
                         failure: @escaping(Error) -> Void) {
    
        let queryLoad: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecReturnData as String: kCFBooleanTrue,
            kSecMatchLimit as String: kSecMatchLimitOne,
            kSecAttrAccessGroup as String: keychainAccessGroupName as AnyObject
        ]
        
        var result: AnyObject?
        
        let resultCodeLoad = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(queryLoad as CFDictionary, UnsafeMutablePointer($0))
        }
        
        if resultCodeLoad == noErr {
            
            if let result = result as? Data,
                let keyValue = NSString(data: result,
                                        encoding: String.Encoding.utf8.rawValue) as String? {
                
                // Found successfully
                print(keyValue)
                success(keyValue)
            }
            
        } else {
            
            print("Error loading data from Keychain: \(resultCodeLoad)")
            
            
            let userInfo: [AnyHashable: Any] = [NSLocalizedDescriptionKey       : NSLocalizedString("Error loading data from Keychain", comment: "Error loading data from Keychain"),
                                                NSLocalizedFailureReasonErrorKey: NSLocalizedString("Error loading data from Keychain", comment: "Error loading data from Keychain")]
                                                    
            let error:NSError = NSError(domain: "", code: Int(resultCodeLoad), userInfo: userInfo)
            
            failure(error)

        }

    }
}
