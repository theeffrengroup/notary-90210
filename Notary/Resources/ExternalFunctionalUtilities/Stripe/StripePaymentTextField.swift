//
//  StripePaymentTextField.swift
//  LiveM
//
//  Created by Rahul Sharma on 14/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Stripe

class StripePaymentTextField:STPPaymentCardTextField {
    
    /// Shared instance object
    static var obj:StripePaymentTextField? = nil
    
    /// Shared instance object for gettting the singleton object
    class func sharedInstance() -> StripePaymentTextField {
        
        if obj == nil {
            
            obj = StripePaymentTextField()
        }
        
        return obj!
    }

    
    /// Set Initial TextField Properties
    ///
    /// - Parameter frame: frame value of the textfield
    func setTextFieldFrame(frame:CGRect) {
        
        self.cursorColor = UIColor.blue
        self.borderColor = UIColor.clear
        self.borderWidth = 0
        self.font = UIFont.boldSystemFont(ofSize: 12)
        self.textColor = UIColor.black;
        self.cornerRadius = 2.0;
        
        self.frame = frame

    }
    
    
}
