//
//  CardIOModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 14/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


class ScanCardModel:CardIOPaymentViewController {
    
    var scanCardDelegate = ScanCardDelegates()

    
    /// Adding Scan Card View Controller Default Properties
    func setInitialProperties() {
        
        self.hideCardIOLogo = true
        self.disableManualEntryButtons = true
        self.modalPresentationStyle = UIModalPresentationStyle.formSheet
        self.title = "Scan Card"
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
}
