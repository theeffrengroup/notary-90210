//
//  Constants.swift
//  DayRunner
//
//  Created by Rahul Sharma on 15/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

let APP_NAME = "Notary-90210"

public let APP_COLOR:UIColor = #colorLiteral(red: 0.3882352941, green: 0.4235294118, blue: 0.7725490196, alpha: 1) //Helper.UIColorFromRGB(rgbValue: 0x2598ed)
let APP_HIGHLIGHT_COLOR :UIColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)  //Helper.UIColorFromRGB(rgbValue: 0xebebeb)
let BALCK_COLOR :UIColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1) //Helper.UIColorFromRGB(rgbValue: 0x333333)
let WHITE_COLOR:UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //Helper.UIColorFromRGB(rgbValue: 0xffffff)
let CLEAR_COLOR:UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) //Helper.UIColorFromRGB(rgbValue: 0x000000)
let BOOKING_FLOW_LIGHT_COLOR:UIColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1) //Helper.UIColorFromRGB(rgbValue: 0x2598ed)


let EMAIL_REG_EXP = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}|[0-9]+"
let PASSWORD_REG_EXP = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[A-Za-z\\d]{7,}"

//let PROFILE_DEFAULT_IMAGE = #imageLiteral(resourceName: "myevent_profile_default_image")

let PROFILE_DEFAULT_IMAGE = UIImage.init(data: UIImagePNGRepresentation(#imageLiteral(resourceName: "myevent_profile_default_image"))!)

let MAP_ZOOM_LEVEL = Float(16.0)


let WINDOW_DELEGATE = UIApplication.shared.delegate?.window

//let KEY_WINDOW = UIApplication.shared.keyWindow

let SCREEN_RECT = UIScreen.main.bounds

let SCREEN_SIZE = UIScreen.main.bounds.size

let SCREEN_WIDTH = WINDOW_DELEGATE??.frame.size.width

let SCREEN_HEIGHT = WINDOW_DELEGATE??.frame.size.height

let TEXTFIELD_PLACEHOLDER_DEFAULT_COLOR = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8039215686, alpha: 1)

var NavigationBarHeight:CGFloat {
    
    if SCREEN_HEIGHT == 812 { //iPhoneX
            
        return 88.0
    }
    return 64.0
    
}

//Push Type
enum PushType: Int {
    
    case BookingFlow = 1
    case Chatting = 2
    case BannedCustomer = 3
    case ZenDesk = 5
    case Other = 7
}

struct COUCH_DB {
    
    //Coch Database constants
    static let USERNAME = ""
    static let PASSWORD = ""
    static let SEARCH_ADDRESS_DOCUMENT_ID = "searchAddressDocID"
    static let MANAGE_ADDRESS_DOCUMENT_ID = "manageAddressDocID"
    static let DEFAULT_CARD_DOCUMENT_ID = "defaultcardDocID"
    static let CHAT_DOCUMENT_ID = "defaultChatDocID"
    static let CARDS_DOCUMENT_ID = "cardsDocID"
    static let NAME   = "mydb"
}




//Address
struct ADDRESS {
    
    static let AddressLine1 = "addressLine1"
    static let AddressLine2 = "addressLine2"
    static let FullAddress   = "fullAddress"
    static let AddressId = "addressId"
    static let Latitude = "latitude"
    static let Longitude = "longitude"
    static let FlatNumber   = "flatNumber"
    static let TagAddress = "tagAddress"
    static let ZipCode   = "zipCode"
    
}


//MapKeys
struct Google {
    
    static let Mapkey = "AIzaSyBS5mVYdNJUzqzZAMf0ewFozJe_5sBLwfk"
    static let ServerKey = "AIzaSyChOcdSFLNUASXIfVzkQMl6DMrg-HBebZo"
}


//Facebook
struct Facebook {
    
    static let profileImageURL = "https://graph.facebook.com/%@/picture?type=large"
}

//Notary customer
//http://18.188.184.181/admin/supportText/customer/en_termsAndConditions.php
//http://18.188.184.181/admin/supportText/customer/en_privacyPolicy.php

struct Links {
    
    static let AppWebsiteLink = "https://www.notary-90210.com"
    static let FacebookPageLink = "https://www.facebook.com/notary90210"
    static let AppstoreLink = "https://itunes.apple.com/us/app/notary-customer/id1378608199?ls=1&mt=8"//"https://itunes.apple.com/us/app/livem-artist/id1319906670?ls=1&mt=8"
    static let TermsAndCondition = "http://18.188.184.181/admin/supportText/customer/en_termsAndConditions.php"//"http://admin2.0.iserve.ind.in/termsAndCondions.php"
    static let PrivacyPolicy = "http://18.188.184.181/admin/supportText/customer/en_privacyPolicy.php"//"http://admin2.0.iserve.ind.in/privacyPolicy.php"
    static let Legal = "http://admin2.0.iserve.ind.in/legal.php"
    static let GooglePlacesAPILink = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&location=%f,%f&radius=500&amplanguage=%@&key=%@"
    static let SubscribeYoutubeChannel = "https://www.youtube.com/"

}


struct LiveChat {
    
    static let LicenceNumber = "4711811"//"7761581"
    static let GroupName = APP_NAME
    static let URLLink = "https://cdn.livechatinc.com/app/mobile/urls.json"
}


//Amazon
struct AMAZONUPLOAD {
    
    static let APPNAME          =  "Customer/"
    
    static let PROFILEIMAGE     =  APPNAME + "ProfilePicture/"
    
    static let PHOTOS           =  APPNAME + "ShipmentImage/"
}


//extension UIImage {
//
//    /// Resize the Image
//    ///
//    /// - Parameter size: New Size
//    /// - Returns: UIImage
//    func resizeImage(size: CGSize) -> UIImage {
//        UIGraphicsBeginImageContext(size)
//        self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
//        let newImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        return newImage!
//    }
//}

struct RecentDB {
    
    var customerName        = "Recent"
    var value:[AnyObject]   = []
    var nameKey             = "Name"
    var valueKey            = "Value"
}

struct PaymentMethod {
    static let Cash  = 1
    static let Card  = 2
    static let None =  12
}


/// Global Variables
struct VCIdentifier {
    
    static let splashVC                 = "SplashVC"
    static let helpVC                   = "HelpVC"
    static let loginVC                  = "LoginVC"
    static let signinVC                 = "RegisterVC"
    static let registerVC               = "RegisterVC"
    static let countryNameVC            = "countryNameVC"
    static let toForget                 = "ForgetPasswordVC"
    static let verifyMobileVC           = "VerifyMobileVC"
    static let setPasswordVC            = "SetPasswordVC"
    static let homeScreenVC             = "HomeScreenVC"
    static let filterVC                 = "FilterVC"
    static let homeListVC               = "HomeListVC"
    static let profileVC                = "ProfileVC"
    static let shareVC                  = "ShareVC"
    static let paymentVC                = "PaymentsVC"
    static let yourAddressVC            = "YourAddressVC"
    static let reviewsVC                = "ReviewsVC"
    static let supportVC                = "SupportVC"
    static let supportInnerVC           = "SupportInnerVC"
    static let helpCenterVC             = "HelpCenterVC"
    static let liveMVC                  = "LiveMVC"
    static let myEventVC                = "MyEventVC"
    static let musicGenre               = "MusicGenreVC"
    static let searchLocationVC         = "SearchLocationVC"
    static let musicianDetailsVC        = "MusicianDetailsVC"
    static let confirmBookingVC         = "ConfirmBookingVC"
    
    static let addPaymentVC             = "AddCardVC"
    static let paymentDetailVC          = "CardDetailVC"
    static let addNewAddressVC          = "AddNewAddressVC"
    
    static let pendingBookingVC         = "PendingBookingVC"
    static let bookingFlowVC            = "BookingFlowVC"
    static let liveTrackVC              = "LiveTrackVC"
    static let invoiceVC                = "InvoiceVC"
    static let pastBookingVC            = "PastBookingVC"
    static let declinedBookingVC        = "DeclineBookingVC"
    static let bookingDetailsVC         = "BookingDetailsVC"
    
    static let termsAndCondVC           = "TermsAndCondVC"
    static let termsWebVC               = "TermsWebVC"
    static let chatVC                   = "ChatVC"
    static let zenDeskVC                = "ZenDeskVC"
    static let liveChatVC               = "LiveChatVC"
    static let checkOutVC               = "CheckOutVC"
    static let recieptDetailsVC         = "RecieptDetailsVC"
    static let supportWebVC             = "SupportWebVC"
    
    static let walletVC                 = "WalletVC"

}


struct TABLEVIEW_CELL_IDENTIFIERS {
    
    static let SearchLocation           = "searchLocationCell"
}


struct SEgueIdetifiers {
    static let registerToLogin              = "RegisterToLogin"
    static let signinToOtp                  = "SignToOtpScreen"
    static let otpToNewPassword             = "setNewPassword"
    static let forgetPassToOtp              = "verifyiOTP"
    static let siginToRegister              = "fromLoginToRegisterVC"
    static let verMobileToSetPass           = "verMobileToSetPassVC"
    static let paymentToAddCard             = "addCardSegue"
    static let LoginToForgetPassordVC       = "LoginToForgetPassordVC"
    static let OTPScreenToChangePasswordVC  = "otpScreenToChangePasswordVC"
    static let profileToMusicGenre          = "ToGenre"
    static let registerToTermsOfService     = "termsOfServiceSegue"
    static let termsOfServiceToWebView      = "termsOfServiceWebViewSegue"
    static let profileToChangePass          = "profileToSetPassword"
    static let faqToWebView                 = "FaqToWebView"
    static let legalToWeb                   = "legalToWeb"


    static let profileChnageEM              = "ProfileToChangeEM"
    static let changeEMToVerify             = "ChangeEMToVerify"
    
}

//CardViewController
//StripeViewController
//CardDetailsViewController

struct Country {
    
    let country_code : String
    let dial_code: String
    let country_name : String
}


///Extra Fonts used in this project
struct FONTS {
    
    static let HindBold = "Hind-Bold"
    static let HindLight = "Hind-Light"
    static let HindMedium = "Hind-Medium"
    static let HindRegular = "Hind-Regular"
    static let HindSemibold = "Hind-Semibold"
    
    static let TitilliumWebBlack = "TitilliumWeb-Black"
    static let TitilliumWebBold = "TitilliumWeb-Bold"
    static let TitilliumWebBoldItalic = "TitilliumWeb-BoldItalic"
    static let TitilliumWebExtraLight = "TitilliumWeb-ExtraLight"
    static let TitilliumWebExtraLightItalic = "TitilliumWeb-ExtraLightItalic"
    static let TitilliumWebItalic = "TitilliumWeb-Italic"
    static let TitilliumWebLight = "TitilliumWeb-Light"
    static let TitilliumWebLightItalic = "TitilliumWeb-LightItalic"
    static let TitilliumWebRegular = "TitilliumWeb-Regular"
    static let TitilliumWebSemiBold = "TitilliumWeb-SemiBold"
    static let TitilliumWebSemiBoldItalic = "TitilliumWeb-SemiBoldItalic"

}

///Extra Booking Status color used in this project
struct BOOKING_COLOR_CODE {
    
    static let UPCOMING:UIColor = #colorLiteral(red: 0.07058823529, green: 0.3764705882, blue: 0.8392156863, alpha: 1) //Helper.UIColorFromRGB(rgbValue: 0x1260d6)
    static let COMPLETED:UIColor = #colorLiteral(red: 0.2705882353, green: 0.8156862745, blue: 0.2588235294, alpha: 1) //Helper.UIColorFromRGB(rgbValue: 0x45d042)
    static let EXPIRED:UIColor = #colorLiteral(red: 0.9176470588, green: 0.4745098039, blue: 0.1490196078, alpha: 1)//Helper.UIColorFromRGB(rgbValue: 0xEA7926)
    static let DECLINE:UIColor = #colorLiteral(red: 0.9960784314, green: 0, blue: 0, alpha: 1)//Helper.UIColorFromRGB(rgbValue: 0xFE0000)
    static let CANCELLED:UIColor = #colorLiteral(red: 0.07058823529, green: 0.3764705882, blue: 0.8392156863, alpha: 1)//Helper.UIColorFromRGB(rgbValue: 0x1260d6)
    
}

//Home List Online and Offline Color
struct HOME_LIST_COLOR_CODE {
    
    static let ONLINE:UIColor = #colorLiteral(red: 0.6156862745, green: 0.8509803922, blue: 0.1176470588, alpha: 1)
    static let OFFLINE:UIColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
}

