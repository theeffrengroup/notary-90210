//
//  ConfigurationFile.swift
//  DayRunner
//
//  Created by Raghavendra V on 17/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

struct ALERTS {

    //Alert Title 
    static let Message = "Message"
    static let Oops = "oops!"
    static let NetworkError = "Network Error!"
    static let NoNetwork = "No Network Connection!"
    static let Error = "Error!"
    static let Missing = "Alert"
    static let Wrong = "Wrong"
    static let Done = "Done"
    static let SessionExpired = "Session Expired"
    static let LogOut = "Do you wish to logout ?"
    static let UpdatePassword = "Your password has been changed successfully."
    static let ChangePassword = "Your New password and Re-entered Password did'nt matched."
    
    //Login Controller
    static let EmailOrPhoneNumberMissing = "Please enter email or phone number"
    static let EmailOrPhoneNumberInvalid = "Invalid email or phone number"
    static let PaswordMissing = "Please enter password"
    
    
    //Signup Controller
    static let FirstNameMissing = "Please enter first name"
    static let LastNameMissing = "Please enter last name"
    static let EmailMissing = "Please enter a valid email address"
    static let EmailInvalid = "Please enter a valid email address"
    static let PasswordInvalid = "Invalid Password, Password should consist of a capital letter, a small letter, a number & minimum of 7 characters."
    static let PhoneNumberMissing = "Please enter phone number"
    static let PhoneNumberInvalid = "Please enter phone number"
    static let ReferralCodeInvalid = "Invalid referral code"
    static let BirthDateMissing = "Please provide your birth date"
    static let InvalidPassword = "Please ensure the password has at least a capital letter, a small letter and a number consisting of minimum 7 characters"
    
    
    static let FacebookError = "Please provide your birth date"
    static let EmailError = "Your device could not send e-mail.  Please check e-mail configuration and try again."
    
    static let MessageError = "Your device could not send message.  Please check message configuration and try again."


    
    //OTP Controller
    static let OPTMissing = "Please enter the code sent to your registered mobile number"
   
    
    //Select Image
    static let SelectImage = "Please Select Image"
    static let SelectImageOption = "Option to select"
    static let Cancel = "Cancel"
    static let Gallery = "Gallery"
    static let Camera = "Camera"
    static let RemoveImage = "Remove Image"
    
    
    static let NoCamera = "No Camera"
    static let CameraMissing = "Sorry, this device has no camera"
    static let Ok = "Ok"
    
    
    //Select Image
    static let ForgotPassword = "Forgot Password"
    static let ForgotPasswordMessage = "Help with using"
    static let Email = "Email"
    static let PhoneNumber = "Phone Number"
    
    
    static let edit = "Edit"
    static let save = "Save"
    static let aboutMe = "We would like to know a little about you."
    static let preference = "Your genre preference."
    
    
    //Remove Address
    static let RemoveAddress = "Remove Address?"
    static let RemoveAddressMessage = "Are you sure you wish to remove this address?"
    static let YES = "YES"
    static let NO = "NO"
    static let SelectAddressTag = "Please provide a name for the address"
    
    
    // Delet CArd Allert
    static let deleteCard = "Card Deleted successfully"
    
    //Invite message
    static let ShareMessage = "Hi, i am using Notary-90210 app which is available in AppStore/PlayStore. Signup with refferel Code: "
    
    
    //Call Feature
    static let MissingCallFeature = "Call feature is unavailable!"
    
    //Message Feature
    static let MissingMessageFeature = "Message feature is unavailable!"
    
    
    //HelpCenter
    static let HelpCenterMessageBoxPlaceHolder = "In my case was not helping to clean Derived Data. I noticed that I have had opened storyboard in two tabs. So closed active one and opened inactive did the trick for me."
    
    //Invoice
    static let invoiceReviewPlaceholder = "how was the performance ?"
    
    //Chat
    static let ChatMessage = "You got new message from bookingId:"
    
    //ZenDeskMessage
    static let ZenDeskMessage = "You got new message from zendesk"

    
    static let VIEW = "VIEW"
    
    //Home Screen
    static let NoArtistMessage = "Sorry, Currently We are not Operational in this region. Please Contact Notary-90210 Support."

    
    struct PAYMENT {
        
        static let MissingCardDetails = "Please Enter Valid Card Details"
        static let MissingStripeKey   = "Please specify a Stripe Publishable Key"
        static let CardAdded          = "Card added successfully"
        static let DeleteCardMessage  = "Card added successfully"
        
        static let RemovePayment = "Remove Payment?"
        static let RemovePaymentMessage = "Are you sure you wish to remove this payment?"
        
        static let MakeDefaultPaymentMessage = "Are you sure you want to make this payment default?"
    }

    
    struct MUSICIAN_DETAILS {
        
        static let MissingGigTime      = "Gig time is not available!"
    }
    
    
    struct CONFIRM_BOOKING {
        
        static let BookButton          = "CONFIRM AND BOOK"
        static let MissingGigTime      = "Please Select Gig Time"
        static let MissingEvent        = "Please select an event"
        static let MissingPayment      = "Please select a payment method"
        static let MissingAddress      = "Please select address"
        
    }
    
    struct BOOKING_FLOW {
        
        static let Ok                  = "OK"
        static let View                = "VIEW"
        static let EventID             = "bid"
        
        static let BookingID           = "Booking Id"
        
        static let ArtistRequested     = "Notary requested"
        static let ArtistAccepted      = "Notary accepted"
        static let ArtistOnTheWay      = "Notary on the way"
        static let ArtistArrived       = "Notary has arrived"
        static let EventStarted        = "Job started"
        static let EventCompleted      = "Job completed"
        static let InvoiceRaised       = "Invoice raised"
        static let IgnoredOrExpired    = "Expired"
        
        static let PendingStatusMessage = "WAITING FOR A NOTARY TO RESPOND"
        
        struct TABLE_HEADER_TITLES {
            
            static let YourPaymentBreakDown = "YOUR PAYMENT BREAKDOWN"
            static let RequestedServices    = "REQUESTED SERVICES"
            static let PaymentMethod        = "PAYMENT METHOD"
            static let Signature            = "SIGNATURE"
            static let BookingStatus        = "BOOKING STATUS"
            static let TotalBillAmount      = "TOTAL BILL AMOUNT"
            static let NotaryDetails        = "YOUR NOTARY WAS"
            static let JobLocation          = "JOB LOCATION"
        }
        
        struct FEES_TITLES {
            
            static let VisitFee             = "Visit Fee"
            static let TravelFee            = "Travel Fee"
            static let CancellationFee      = "Cancellation Fee"
            static let Discount             = "Discount"
            static let LastDue              = "Last Due"
            static let Total                = "Total"
            static let BillTotal            = "Total Bill"
        }
        
    }
    
    struct HOME_SCREEN {
    
        static let MusicianMissing     = "No notaries are available!"
        static let MissingCategory     = "Currently we are not operational in your region, please contact Notary-90210 support."
        static let NoNotaries      = "No\nNotary"
        static let Min             = "MIN"
    }
    
    struct HOME_LIST_SCREEN {
        
        static let AvailableNowMessage = "AVAILABLE NOW"
        static let AvailableLaterMessage = "AVAILABLE LATER"
    }
    
    
    struct CANCEL_BOOKING {
        
        static let CancelReasonMissing = "Please select a cancellation reason"
    }
    
    struct CHECK_OUT {
        
        static let MaximumQuantity = "Reached maximum quantity"
    }
    
    struct SET_NEW_PASSWORD {
        
        static let WrongOldPassword    = "Please enter correct old password"
        static let NewAndConfirmPasswordMisMatch  = "Confirm password should be same as new password"
        static let NewPasswordInvalid = "Invalid new password, New password should consist of a capital letter, a small letter, a number & minimum of 7 characters."
    }
    
    struct WALLET {
        
        static let MissingAmount    = "Please enter amount"
        static let SelectCard       = "Please select the card"
    }
        
}


struct SHARE_MSG {
    
    //Register with Notary 90210 using my referral code " " to earn discounts on the Notary 90210 app. Download the app @ "link".
    
    //"Register with Notary 90210 using my referral code \"%@\" to earn discounts on the Notary 90210 app.\n\nDownload the app @ \"%@\""

    /*static let  Twitter = "Register with Notary 90210 using my referral code \"%@\" to earn discounts on the Notary 90210 app.\n\nDownload the app @ \"link\""
    
    static let  Email = "Hi, if you sign-up to new notary booking app Notary-90210 using the code '%@' you'll receive 15%% off your first booking. Give it a try now and discover amazing notaries in your local area.\n\nDownload Link:- %@"
    
    static let  Messenger = "Hi, if you sign-up to new notary booking app Notary-90210 using the code '%@' you'll receive 15%% off your first booking. Give it a try now and discover amazing notaries in your local area.\n\nDownload Link:- %@"
    
    static let  Facebook = "Sign-up to new notary booking app Notary-90210 using my code '%@' and receive 15%% off your first use. Discover notaries in your area now."
    
    static let  Whatsapp = "Hi, if you sign-up to new notary booking app Notary-90210 using the code '%@' you'll receive 15%% off your first booking. Give it a try now and discover amazing notaries in your local area.\n\nDownload Link:- %@"*/
    
    static let  Twitter = "Register with Notary 90210 using my referral code \"%@\" to earn discounts on the Notary 90210 app.\n\nDownload the app @ \"%@\""//"Register with Notary 90210 and earn discounts on the Notary 90210 app.\n\nDownload the app @ \"%@\""
    
    static let  Email = "Register with Notary 90210 using my referral code \"%@\" to earn discounts on the Notary 90210 app.\n\nDownload the app @ \"%@\""
    
    static let  Messenger = "Register with Notary 90210 using my referral code \"%@\" to earn discounts on the Notary 90210 app.\n\nDownload the app @ \"%@\""
    
    static let  Facebook = "Register with Notary 90210 using my referral code \"%@\" to earn discounts on the Notary 90210 app.\n\nDownload the app @ \"%@\""
    
    static let  Whatsapp = "Register with Notary 90210 using my referral code \"%@\" to earn discounts on the Notary 90210 app.\n\nDownload the app @ \"%@\""
}


struct CHANGE_ME {
    
    static let emailTittle = "change email"
    static let emaileText = "We will send a verification link to this email address. Post verification, it will be added to your account."
    static let phoneTittle = "change phone number"
    static let phoneText = "Enter the new mobile number and you will get a verification code to reset."
}

struct MissingAlert {
    static let passwordMissing = "Enter password."
    static let password = "Minimum of 7 characters with a number,small letter & capital letter."
    
    static let dobMissing = "Enter your date of birth."
    static let dob = "you need to be 18 or older to use Notary-90210 service."
}

struct HELP {
    
    static let NotaryTitle = "OnDemand Notary Service"
    static let NotaryDisc = "We offer notary and apostille services for many different documents including Real Estate, Acknowledgments, Oaths and Affirmations, and many more."
    
    static let discover = "Discover"
    static let discoverDisc = "Get instant access to hundreds of artists, browse their profiles, watch their videos, read reviews of past gigs and begin your unique experience with live music."
    
    static let book = "Book"
    static let bookDisc = "When you open the Notary-90210 app, notaries near your location ready to play will be shown. Check them out, request a booking and pay quickly and easily."
    
    static let enjoy = "Enjoy"
    static let enjoyDisc = "The artist will come to your location on the chosen date and play. Simply relax and enjoy the live music!"
    
    static let review = "Review"
    static let reviewDisc = "When the booking is finished, review the performance. Your feedback helps the Notary-90210 community to build a better experience for everyone."
}



let months      =   ["Jan",
                     "Feb",
                     "Mar",
                     "Apr",
                     "May",
                     "June",
                     "July",
                     "Aug",
                     "Sep",
                     "Oct",
                     "Nov",
                     "Dec"]

struct PROGRESS_MESSAGE {
    
    static let Loading                      = "Loading..."
    static let Saving                       = "Saving..."
    static let Adding                       = "Adding..."
    static let Deleting                     = "Deleting..."
    static let LogOut                       = "Logging out..."

    
    //LoginVC
    static let Login                        = "Logging in..."
    static let FBLogin                      = "Facebook Login..."
    
    //Facebook
    static let FetchingDetailsFromFB        = "Fetching Details..."
    
    
    //Forgot Password VC
    static let ForgotPassword               = "Fetching Password..."
    
    //RegisterVC
    static let ValidatingReferralCode       = "Validating Referral Code..."
    static let ValidatingPhoneNumber        = "Validating Phone Number..."
    static let ValidatingEmail              = "Validating Email..."
    
    static let ChangingEmail                = "Changing Email..."
    static let ChangingPhoneNumber          = "Changing Phone Number..."
    static let ChangingPassword             = "Changing Password..."
    
    //OTP VC
    static let VerifyingOTP                 = "Verifying Phone Number..."
    static let SignUP                       = "Register..."
    static let SendingOTP                   = "Sending..."
    
    //Checkout Screen
    static let AddingService                = "Adding service..."
    static let RemovingService              = "Removing service..."
    
    //Confirm Booking
    static let ConfirmBooking               = "Booking provider..."//"Confirm Booking..."
    
    //Validating Promo code
    static let ValidatingPromoCode          = "Validating Promo Code..."
    
    
    //Review
    static let SubmittingReview             = "Submitting Review..."
    
    //CancelBooking
    static let CancelBooking                = "Cancel Booking..."
    
    //ZenDesk
    static let CreatingTicket               = "Creating ticket..."
    
    static let GettingTicket                = "getting tickets.."
    
    static let SendingTicket                = "sending.."

}

struct MESSAGETEXT {

    static let PLACEHOLDER_TEXT = "Add comments here...".localizedWith()

}

