//
//  ContryNameModelClass.swift
//  Iserve
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class ContryNameModelClass: NSObject {
    
    // MARK: - Variable Deceleration -
    var countries = [[String: String]]()
    var countriesFiltered = [Country]()
    var countriesModel = [Country]()
   
    
    /// Read data from jason File
    ///
    /// - Returns: Return Dictionary of Countries
    func getCoutryDetailsFormjsonSerial() -> [[String: String]]{
        let data = try? Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "countries", ofType: "json")!))
        do {
            let parsedObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
            countries = parsedObject as! [[String : String]]
        }catch{
            print("not able to parse")
        }
        return countries
    }
    
   
    
}
