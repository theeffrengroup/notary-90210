//
//  DistanceAndETAManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 11/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import RxCocoa
import RxSwift

protocol DistanceAndETAManagerDelegate {
    
    
    /// Delegate to show musician Distance and ETA details
    func distanceAndETARsponse(timeInMinute:String, distance:String)
}

class DistanceAndETAManager {
    
    static let sharedInstance = DistanceAndETAManager()
    var delegate:DistanceAndETAManagerDelegate? = nil
    
    var mileageMatric = 0

    let rxDistanceAndETAAPICall = DistanceAndETAAPI()
    let disposebag = DisposeBag()
    
    var currentGoogleServerKey = ""
    
    var srcLat = 0.0
    var srcLong = 0.0
    var destLat = 0.0
    var destLong = 0.0
    
    
    /// Method to call get list of saved address service API
    func getDistanceAndETA() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            return
        }
        
        if currentGoogleServerKey.length == 0 {
            
            self.sendEmptyResponse()
            
        } else {
            
            if !rxDistanceAndETAAPICall.getDistanceAndETA_Response.hasObservers {
                
                rxDistanceAndETAAPICall.getDistanceAndETA_Response
                    .subscribe(onNext: {response in
                        
                        self.WebServiceResponse(response: response, requestType: RequestType.GetDistanceAndETA)
                        
                    }, onError: {error in
                        
                    }).disposed(by: disposebag)
                
            }
            
            rxDistanceAndETAAPICall.getDistanceAndETAServiceAPICall(appointmentLat: srcLat,
                                                                    appointmentLong: srcLong,
                                                                    musicianLat: destLat,
                                                                    musicianLong: destLong,
                                                                    googleServerKey:  currentGoogleServerKey)
        }
        
    }
    
    
    //MARK - WebService Response -
    func WebServiceResponse(response:APIResponseModel, requestType:RequestType)
    {
        if (response.data[SERVICE_RESPONSE.Error] != nil) {
            
            sendEmptyResponse()
            return
        }
        
        switch response.httpStatusCode
        {
            case HTTPSResponseCodes.SuccessResponse.rawValue:
            
            switch requestType
            {
                case RequestType.GetDistanceAndETA:
                
                    parseDistanceAndETAResponse(response: response)
                
                default:
                    break
            }
            break
            
        default:
            
            break
        }
        
    }
    
    func parseDistanceAndETAResponse(response:APIResponseModel) {
        
        if let routesesponse:[Any] = response.data["routes"] as? [Any] {
            
            if routesesponse.count > 0 {
                
                if let innerDict = routesesponse[0] as? [String:Any] {
                    
                    if let legsArray = innerDict["legs"] as? [Any] {
                        
                        if let legsDict = legsArray[0] as? [String:Any] {
                            
                            var timeInMinute = 1
                            
                            if let duration = legsDict["duration"] as? [String:Any] {
                                
                                if let timeInSeconds = duration["value"] as? Int {
                                    
                                    timeInMinute = self.convertTimeFromSecToMinute(timeinSeconds: timeInSeconds)
                                }
                                
                            }
                            
                            var distanceInMeter = 0
                            
                            if let distance = legsDict["distance"] as? [String:Any] {
                                
                                if let distInMeter = distance["value"] as? Int {
                                    
                                    distanceInMeter = distInMeter
                                }
                                
                            }
                            
                            if delegate != nil {
                                
                                delegate?.distanceAndETARsponse(timeInMinute: "\(timeInMinute) MIN", distance: self.getDistanceDependingMileageMetricFromServer(distance: Double(distanceInMeter), mileageMatric: mileageMatric))
                            }
                            
                        }
                        
                    }
                    
                } else {
                    
                    sendEmptyResponse()
                }

            } else {
                
                currentGoogleServerKey = getNextGoogleServerKey()
                
                if currentGoogleServerKey.length > 0 {
                    
                    getDistanceAndETA()
                    
                } else {
                    
                    sendEmptyResponse()
                }
            }
            
            
        } else {
            
            sendEmptyResponse()
        }
        
    }
    
    func getNextGoogleServerKey() -> String {
        
        if currentGoogleServerKey.length > 0 {
            
            if MusiciansListManager.sharedInstance().googleServerKeys.count > 0 {
                
                if let index = MusiciansListManager.sharedInstance().googleServerKeys.index(where: {$0 == currentGoogleServerKey}) {
                    
                    /*if (index + 1) < MusiciansListManager.sharedInstance().googleServerKeys.count {
                        
                        return MusiciansListManager.sharedInstance().googleServerKeys[(index + 1)]
                    } else {
                        
                        return ""
                    }*/
                    
                    MusiciansListManager.sharedInstance().googleServerKeys.remove(at: index)
                    
                    if MusiciansListManager.sharedInstance().googleServerKeys.count > 0 {
                        
                        return MusiciansListManager.sharedInstance().googleServerKeys[0]
                        
                    } else {
                        
                        return ""
                    }
                    
                    
                } else {
                    
                    return MusiciansListManager.sharedInstance().googleServerKeys[0]
                }
            } else {
                
                return ""
            }
            
        } else {
            
            return ""
        }
    }
    
    func sendEmptyResponse() {
        
        if delegate != nil {
            
            delegate?.distanceAndETARsponse(timeInMinute: "1 MIN", distance: self.getDistanceDependingMileageMetricFromServer(distance: 0, mileageMatric: mileageMatric))
        }
    }
    
    func convertTimeFromSecToMinute(timeinSeconds: Int) -> Int {
        
        var min: Int
        
        if timeinSeconds < 60 && timeinSeconds <= 0 {
            min = 1
        }
        else if timeinSeconds > 60 {
            
            min = timeinSeconds / 60
            let remainingTime: Int = timeinSeconds % 60
            if remainingTime < 60 && remainingTime >= 30 {
                min += 1
            }
        }
        else {
            
            min = 1
        }
        
        return min
    }
    
    
    /// Method to get distance string depending on config mileage matric
    ///
    /// - Parameter distance: input distance in meter
    /// - Returns: output distamce string
    func getDistanceDependingMileageMetricFromServer(distance:Double, mileageMatric:Int) -> String {
        
        if mileageMatric == 0 {
            
            return String(format:"%.2f kms away",Helper.convertMeter(toKiloMeter: distance))
            
        } else {
            
            return String(format:"%.2f miles away",Helper.convertMeter(toMiles: distance))
        }
        
    }
    
}
