//
//  String+AllValidations.swift
//  Sales Paddock
//
//  Created by 3Embed on 16/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

extension String {
    
    var isValidEmail: Bool {
        if self.isEmpty {
            return false
        }
        let EMAIL_REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", EMAIL_REGEX)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        if self.isEmpty {
            return false
        }
        let PASSWORD_REGEX = "(?!.*pass|.*word|.*1234|.*qwer|.*asdf) exclude common passwords" //"^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", PASSWORD_REGEX)
        return passwordTest.evaluate(with: self)
    }

    func isValidMobile() -> Bool {
        if self.isEmpty {
            return false
        }
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return phoneTest.evaluate(with: self)
    }
}



