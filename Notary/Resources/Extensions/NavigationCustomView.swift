//
//  NavigationCustomView.swift
//  LiveM
//
//  Created by Rahul Sharma on 02/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

class NavigationCustomView:UIView {
    
    override var intrinsicContentSize: CGSize {
        _ = super.intrinsicContentSize
        return UILayoutFittingExpandedSize
    }
    
}
