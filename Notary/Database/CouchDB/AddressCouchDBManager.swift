//
//  AddressCouchDBManager.swift
//  LiveM
//
//  Created by Rahul Sharma on 24/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class AddressCouchDBManager {
    
    static let sharedInstance = AddressCouchDBManager()
    
    //MARK: - Get Address Details -
    
    /// Method to get manage address details from couch DB Document
    ///
    /// - Returns: manage address details
    func getManageAddressDetailsFromCouchDB() -> [Any] {
        
        let doc: CBLDocument = CouchDBDocument.sharedInstance().getDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.manageAddressDocId)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        
        if let dicArray = dic["Value"] as? [Any] {
            
            return dicArray
            
        } else {
            
            return []
        }
        
        
        //        return (dic["Value"] as? [Any])!
    }
    
    
    /// Method to get search address details from couch DB Document
    ///
    /// - Returns: search address details
    func getSearchAddressDetailsFromCouchDB() -> [Any] {
        
        let doc: CBLDocument = CouchDBDocument.sharedInstance().getDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.searchAddressDocId)
        let dic: [String: AnyObject] = doc.properties! as [String : AnyObject]
        
        if let dicArray = dic["Value"] as? [Any] {
            
            return dicArray
            
        } else {
            
            return []
        }
        
        
        //        return (dic["Value"] as? [Any])!
    }
    
    
    //MARK: - Update Address Details -
    
    /// Method to update manage address details in couch DB Document
    func updateManageAddressDetailsToCouchDBDocument(manageAddressArray:[Any]) {
        
        CouchDBDocument.sharedInstance().updateDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.manageAddressDocId, documentArray: manageAddressArray as [AnyObject])
    }
    
    
    /// Method to update search address details in couch DB Document
    func updateSearchAddressDetailsToCouchDBDocument(searchAddressArray:[Any]) {
        
        CouchDBDocument.sharedInstance().updateDocument(database: CouchDBObject.sharedInstance().database, documentId: Utility.searchAddressDocId, documentArray: searchAddressArray as [AnyObject])
    }
    
    
    //MARK: - Remove Address Details -
    
    
    /// Remove previous search addresses document from couchDB
    func deletePreviousSearchAddressCouchDBDocument() {
        
        CouchDBDocument.sharedInstance().deleteDocument(database: CouchDBObject.sharedInstance().database, id: Utility.searchAddressDocId)
        
    }
    
    /// Remove manage addresses document from couchDB
    func deleteManageAddressCouchDBDocument() {
        
        CouchDBDocument.sharedInstance().deleteDocument(database: CouchDBObject.sharedInstance().database, id: Utility.manageAddressDocId)
        
    }
    
    
}
