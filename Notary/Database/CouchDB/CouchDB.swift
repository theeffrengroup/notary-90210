//
//  CouchDBObject.swift
//  Rogi
//
//  Created by NABEEL on 14/06/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CouchDBObject: NSObject {

    var database : CBLDatabase! = nil
    var manager : CBLManager! = nil
    
    static var obj:CouchDBObject? = nil
    
    class func sharedInstance() -> CouchDBObject {
        
        if obj == nil {
            
            obj = CouchDBObject()
        }
        
        return obj!
    }

    
    override init() {
        
        super.init()
        self.manager = CBLManager.sharedInstance()
        if !(self.manager != nil) {
            return
        }
        do {
            self.database = try self.manager.databaseNamed(COUCH_DB.NAME)
        }
        catch {
            
        }
        if !(self.database != nil) {
            return
        }
    }
    
}
