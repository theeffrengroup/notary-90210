//
//  MusicianMetaDataModel.swift
//  Notary
//
//  Created by Rahul Sharma on 31/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class MusicianMetaDataModel {
    
    var fieldTitle = ""
    var fieldType = 1
    var fieldDescription = ""
    var isMandatory = true
    
    
    init(metaDataResponse:[String:Any]) {
        
        fieldTitle = GenericUtility.strForObj(object: metaDataResponse["fieldName"])
        
        if let fieldTypeValue = metaDataResponse["fieldType"] as? Int {
            
            fieldType = fieldTypeValue
            
            var fieldDataValue = GenericUtility.strForObj(object: metaDataResponse["data"])
            
            if fieldTypeValue == 2 {
                
                fieldDataValue = fieldDataValue.replacingOccurrences(of: ",", with: "\n\u{2022} ")
                fieldDataValue = "\u{2022} " + fieldDataValue
            }
                
            fieldDescription = fieldDataValue
        }
        
        if let mandatoryValue = metaDataResponse["isManadatory"] as? Int {
            
            if mandatoryValue == 0 {
                
                isMandatory = false
            }
        }

    }
}
