//
//  ProfileDetailModal.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


struct ProfileRequestModel {
    
    var firstName = ""
    var lastName = ""
    var about = ""
    var dateOfBirth = ""
    var generes = ""
    var profilePicURL = ""
    
}
