//
//  ConfirmationBookingModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 18/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class ConfirmBookingModel {
    
    var selectedGigTimeTag = 0
    var selectedGigTimeId = ""
    var selectedGigTimeAmount = 25.0
    
    var selectedEventTag = 0
    var selectedEventId = ""
    
    var selectedEventStartTag = 0
    var selectedEventStartTime = 0
    
    var paymentmethodTag = 0
    
    var serviceFeeAmount = 0.0
    var selectedCardModel:CardDetailsModel!
    var appointmentLocationModel:AppoimtmentLocationModel!
    
    var providerModel:MusicianDetailsModel!
    var providerFullDetalsModel:MusicianDetailsModel!
    
    var discountType:String = ""
    var discountValue = 0.0
    var percentDiscountValue:Int = 0
    var promoCodeText = ""
    
    var categoryId = Utility.categoryId
    var visitFee = Utility.categoryVisitFee
    
    var bookingModel = BookingModel.OnDemand
    
    var cartId = ""
    var servicesTotalAmount = 0.0
    
    var isScheduleBookingAvailable = false

    
    static var Obj:ConfirmBookingModel? = nil
    
    class func sharedInstance() -> ConfirmBookingModel {
        
        if Obj == nil {
            
            return ConfirmBookingModel()
            
        } else {
            
            return Obj!
        }
    }
    
}
