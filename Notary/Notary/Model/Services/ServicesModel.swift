//
//  ServicesModel.swift
//  Notary
//
//  Created by Rahul Sharma on 14/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class ServicesModel {
    
    var serviceId = ""
    var serviceName = ""
    var serviceDescription = ""
    var serviceAmount = 0.0
    var perServiceDescription = ""
    var addOnServiceDescription = ""
    
    var isForQuantity = false
    var maxQuantity = 0
    
    var isPlusOneCostAvailable = false
    var plusOnePrice = 0.0
    
    var isSelected = false
    var selectedQuantity = 0
    
    var serviceTotalAmount = 0.0
    
    var currencySymbol = Utility.currencySymbol

    init() {
        
    }
    
    init(serviceDetails:[String:Any]) {//Initial Service
        
        serviceId = GenericUtility.strForObj(object: serviceDetails["_id"])
        serviceName = GenericUtility.strForObj(object: serviceDetails["ser_name"])
        serviceDescription = GenericUtility.strForObj(object: serviceDetails["ser_desc"])
        
//        currencySymbol = GenericUtility.strForObj(object: serviceDetails["currencySymbol"])

        if let amount = serviceDetails["is_unit"] as? Double {
            
            serviceAmount = amount
        }
        
        perServiceDescription = GenericUtility.strForObj(object: serviceDetails["unit"])

        addOnServiceDescription = GenericUtility.strForObj(object: serviceDetails["addOnServiceDescription"])
        
        if let isQuantityAvailable = serviceDetails["quantity"] as? String {
            
            if isQuantityAvailable == "1" {
                
                isForQuantity = true
                
                if let maxQuantityValue = serviceDetails["maxquantity"] as? Int {
                    
                    maxQuantity = maxQuantityValue
                }
                
            } else {
                
                isForQuantity = false
            }
        }
        
        if let isPlusOneAvailable = serviceDetails["plusOneCost"] as? String {
            
            if isPlusOneAvailable == "1" {
                
                isPlusOneCostAvailable = true
                
                if let plusOneValue = serviceDetails["additionalPrice"] as? Int {
                    
                    plusOnePrice = Double(plusOneValue)
                }
                
            } else {
                
                isPlusOneCostAvailable = false
            }
        }

        isSelected = false
    
    }
    
    
    init(bookingFlowServiceDetails:[String:Any]) {//Booking Flow Service
        
        serviceId = GenericUtility.strForObj(object: bookingFlowServiceDetails["serviceId"])
        serviceName = GenericUtility.strForObj(object: bookingFlowServiceDetails["serviceName"])
        
        if let amount = bookingFlowServiceDetails["unitPrice"] as? Double {
            
            serviceAmount = amount
        }
        
        if let totalAmount = bookingFlowServiceDetails["amount"] as? Double {
            
            serviceTotalAmount = totalAmount
        }
        
        if let selQuantity = bookingFlowServiceDetails["quntity"] as? Int {
            
            selectedQuantity = selQuantity
        }
        
    }

}
