//
//  MusiciansListManager.swift
//  LiveM
//
//  Created by Raghavendra V on 07/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import GoogleMaps
import RxCocoa
import RxSwift


protocol MusiciansListManagerDelegate {
    
    
    /// Show Initial Musician List
    ///
    /// - Parameter arrayOfUpdatedMusicians: array consisting of list of musicians model
    ///   - arrayOfMusiciansList: array consisting of list of Musicians
    func showInitialMusicianList(arrayOfUpdatedMusiciansModel: [MusicianDetailsModel],arrayOfMusiciansList:[Any])
    
    
    /// Method to Update the Musician List
    ///
    /// - Parameters:
    ///   - arrayOfUpdatedMusicians: array consisting of list of updated musicians model
    ///   - arrayOfRowsToAdd: array consisting of list of row numbers to add from musician list
    ///   - arrayOfRowsToRemove: array consisting of list of row numbers to remove from musician list
    ///   - arrayOfMusicianIdToRemove: array consisting of list of Musician id's to remove from musician list
    ///   - arrayOfMusiciansList: array consisting of list of Musicians
    func updateMusicianList(arrayOfUpdatedMusicians:[MusicianDetailsModel], arrayOfRowsToAdd:[Int], arrayOfRowsToRemove:[Int],arrayOfMusicianIdToRemove:[String], arrayOfMusiciansList:[Any])
    
    
    /// This method will invoke when user gets empty musician lists
    func emptyMusicianList(errMessage:String)
    
}


class MusiciansListManager {
    
    var publishTimer:Timer?
    let acessClass = AccessTokenRefresh.sharedInstance()
    var navigation:UINavigationController!
    var viewController:UIViewController!
    var delegate:MusiciansListManagerDelegate? = nil
    var arrayOfMusicians:[Any] = []
    let locationObj = LocationManager.sharedInstance()
    
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var gmsMutablePath:GMSMutablePath!
    var polygon:GMSPolygon!

    var errMessageString = ""
    
    var categoryId = Utility.categoryId
    
    let accessTokenrefreshClass = AccessTokenRefresh.sharedInstance
    
    static var obj:MusiciansListManager? = nil
    
    var fcmTopicsToSubscribe = [String:Any]()
    var googleServerKeys = [String]()

    
    class func sharedInstance() -> MusiciansListManager {
        
        if obj == nil {
            
            obj = MusiciansListManager()
        }
        
        return obj!
    }
    
    let disposebag = DisposeBag()
    
    /// Time Interval
    var timeInterval: Double = 12 {
        
        didSet {
            
            startTimer()
        }
    }
    
    
    func startTimer() {
        
        if publishTimer == nil {
            
            publishTimer = Timer.scheduledTimer(timeInterval: timeInterval,
                                                target: self,
                                                selector: #selector(handleTimer),
                                                userInfo: nil,
                                                repeats: true)
            
        }
        
    }
    
    /// Handle Timer
    @objc func handleTimer() {
        
        if (publishTimer != nil) && (publishTimer?.isValid == true) {
            
            publish(isIinitial: false)
        }
    }
    
    
    /// Stop Publishing
    func stopPublish() {
        
        if (publishTimer != nil) && (publishTimer?.isValid == true) {
            
            publishTimer?.invalidate()
            publishTimer = nil
        }
    }
    
    
    
    /// Method to maintain Refresh Access Token
    func methodTorefreshAccessToken(requestType:RequestType) {
        
        switch viewController {
            
            case is HomeScreenViewController:
            
                let homeVC = HomeScreenViewController.sharedInstance()
            
                homeVC.uninitializeMQTTMethods()
                homeVC.apiTag = requestType.rawValue
                break
            
            case is HomeListViewController:
            
                let homeListVC = HomeListViewController.sharedInstance()
            
                homeListVC.uninitializeMQTTMethods()
                homeListVC.apiTag = requestType.rawValue
                break
            
            default:
                break
        }
        
        self.acessClass.getAcessToken(progressMessage: "")
        
    }
    
    /// GetCategories
    func getCategoriesServiceAPI(currentLat:Double,
                                 currentLong:Double) {
        
        
        let rxCategoriesListAPICall = CategoriesListAPI()
        
        if !rxCategoriesListAPICall.categoriesList_Response.hasObservers {
            
            rxCategoriesListAPICall.categoriesList_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        if response.httpStatusCode == 401 {
                            
                            if let viewController:UIViewController = UIApplication.shared.delegate?.window??.rootViewController {
                                
                                if let VC = viewController as? ExSlideMenuController
                                {
                                    print(VC)
                                    self.stopPublish()
                                    Helper.logOutMethod()
                                    
                                    if let errorMessage = response.data[SERVICE_RESPONSE.ErrorMessage] as? String {
                                        
                                        Helper.showAlert(head: ALERTS.Message, message: errorMessage)
                                    }
                                    
                                }
                            }
                            
                        }
                        
                        return
                        
                    }
                    
                    self.WebServiceResponse(response: response, isInitial: true,requestType: .getAllCategories)
                    
                }, onError: {error in
                    
                    Helper.hidePI()
                    
                }).disposed(by: disposebag)
            
            
        }
        
        rxCategoriesListAPICall.getCategoriesServiceAPICall(pickupLat: currentLat,
                                                            pickupLong: currentLong)
            
    }
    
    
    /// Publish
    func publish(isIinitial:Bool) {
                
        let appointmentLocationModel = AppoimtmentLocationModel.sharedInstance
        
        if (AppoimtmentLocationModel.sharedInstance.pickupLatitude == 0.0 || AppoimtmentLocationModel.sharedInstance.pickupLongitude == 0.0) {
            
            return
            
        } else if categoryId.length == 0 {
            
            self.noMusiciansResponse(message: ALERTS.HOME_SCREEN.MissingCategory)
            return
        }
        
        let rxMusiciansListAPICall = MusiciansListAPI()
        
        if !rxMusiciansListAPICall.musiciansList_Response.hasObservers {
            
            rxMusiciansListAPICall.musiciansList_Response
                .subscribe(onNext: {response in
                    
                    Helper.hidePI()
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        if response.httpStatusCode == 401 {
                            
                            if let viewController:UIViewController = UIApplication.shared.delegate?.window??.rootViewController {
                                
                                if let VC = viewController as? ExSlideMenuController
                                {
                                    print(VC)
                                    self.stopPublish()
                                    Helper.logOutMethod()
                                    
                                    if let errorMessage = response.data[SERVICE_RESPONSE.ErrorMessage] as? String {
                                        
                                        Helper.showAlert(head: ALERTS.Message, message: errorMessage)
                                    }
                                                                        
                                }
                            }
                            
                        }
                        
                        return

                    }
                    
                    self.WebServiceResponse(response: response, isInitial: isIinitial, requestType: .getAllMusicians)
                    
                }, onError: {error in
                    
                    Helper.hidePI()
                    
                }).disposed(by: disposebag)
            
            
        }
        
        if isIinitial {
            
            
            rxMusiciansListAPICall.musiciansListGETServiceAPICall(pickupLat: appointmentLocationModel.pickupLatitude, pickupLong: appointmentLocationModel.pickupLongitude)
            
        } else {
            rxMusiciansListAPICall.musiciansListPOSTServiceAPICall(appointmentLocationModel:appointmentLocationModel)
            
        }
        
        
        
    }
    
    //MARK - Web Service Response -
    func WebServiceResponse(response:APIResponseModel, isInitial:Bool, requestType:RequestType)
    {
        let serviceResponseDate = Date()
        
        switch response.httpStatusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                AppDelegate().defaults.set(GenericUtility.strForObj(object: response.data[SERVICE_RESPONSE.DataResponse]), forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
                self.methodTorefreshAccessToken(requestType:requestType)
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                if let viewController:UIViewController = UIApplication.shared.delegate?.window??.rootViewController {
                    
                    if let VC = viewController as? ExSlideMenuController
                    {
                        print(VC)
                        self.stopPublish()
                        Helper.logOutMethod()
                        
                        if let errorMessage = response.data[SERVICE_RESPONSE.ErrorMessage] as? String {
                            
                            Helper.showAlert(head: ALERTS.Message, message: errorMessage)
                        }
                        
                    }
                }
                
                break
                
            
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                if requestType == RequestType.getAllMusicians {

                    print("Get All Musician Response:\(response)")
                    
                    if isInitial {
                        
                        if let responseData = response.data[SERVICE_RESPONSE.DataResponse] as? [Any] {
                            
                            self.recievedListOfMusiciansFromMQTT(listOfNewMusicians: responseData, listOfCurrentMusicians: arrayOfMusicians)
                            
                        }
                    }
                    
                } else {//Category response
                    
                    print("Get All Categories Response:\(response.data)")
                    
                    if let responseData = response.data[SERVICE_RESPONSE.DataResponse] as? [String:Any] {
                        
                        appDelegate?.isDateChanged = false
                        
                        if let catResponse = responseData[SERVICE_RESPONSE.CategoryArrayResponse] as? [[String:Any]] {
                            
                            self.recievedListOfCategories(arrayOfCategories: catResponse)
                            
                        }
                        
                        if let currentGMTTimeStamp = responseData["serverGmtTime"] as? UInt64 {
                            
                            let currentDateFromServer = Date.init(timeIntervalSince1970: TimeInterval(currentGMTTimeStamp))
                            
                            let differenceTimeInterval = currentDateFromServer.timeIntervalSince(serviceResponseDate)
                            
                            print("\(differenceTimeInterval)")
                            
                            var differenceTime = differenceTimeInterval
                            
                            if differenceTimeInterval < 0 {
                                
                                differenceTime = differenceTimeInterval * -1
                            }
                            
                            if differenceTime > 60 {
                                
                                UserDefaults.standard.set(differenceTimeInterval, forKey: USER_DEFAULTS.TimeZone.differenceTimeStamp)
                                
                            } else {
                                
                                UserDefaults.standard.set(0.0, forKey: USER_DEFAULTS.TimeZone.differenceTimeStamp)
                            }
                            
                            UserDefaults.standard.synchronize()
                        }
                        
                        if let cityResponse = responseData[SERVICE_RESPONSE.CityDataResponse] as? [String:Any] {
                            
                            let userDefaults = UserDefaults.standard

                            if let curSymbol = cityResponse["currencySymbol"] as? String {
                                
//                                ConfigManager.sharedInstance.currencySymbol = curSymbol
                                
                                userDefaults.set(curSymbol,
                                             forKey: USER_DEFAULTS.CONFIG.CURRENCYSYMBOL)
                            }
                            
                            
                            if let curSymbolPrefix = cityResponse["currencyAbbrText"] as? String {
                                
                                userDefaults.set(curSymbolPrefix, forKey: USER_DEFAULTS.CONFIG.CURRENCY_PREFIX_OR_SUFFIX)
                            }
                            
                            userDefaults.synchronize()
                            
                            if let pointsProps = cityResponse["pointsProps"] as? [String:Any] {
                                
                                if let arrayOfpaths = pointsProps["paths"] as? [[String:Any]] {
                                    
                                    self.plotZonesOnMapView(pathsArray: arrayOfpaths)
                                }
                            }
                            
                            if let pushTopics = cityResponse["pushTopics"] as? [String:Any] {
                                
                                fcmTopicsToSubscribe = pushTopics
                                MQTTOnDemandAppManager.sharedInstance().subScribeToFCMTopics()
                            }
                            
                            if let googleKeys = cityResponse["custGoogleMapKeys"] as? [String] {
                                
                                googleServerKeys = googleKeys
                                
                                if googleServerKeys.count > 0 {
                                    
                                    DistanceAndETAManager.sharedInstance.currentGoogleServerKey = googleServerKeys[0]
                                } else {
                                    
                                    DistanceAndETAManager.sharedInstance.currentGoogleServerKey = ""
                                }
                                
                            }
                            
                        } else {
                            
                            MQTTOnDemandAppManager.sharedInstance().unSubScribeToFCMTopics()
                            fcmTopicsToSubscribe = [:]
                            googleServerKeys = []
                            DistanceAndETAManager.sharedInstance.currentGoogleServerKey = ""
                            gmsMutablePath = nil
                        }
                    }
                }
                
                
                break
            
            case HTTPSResponseCodes.MusicianList.NoMusicians.rawValue:
                
                if requestType == RequestType.getAllMusicians {

                    print("Get All Musician Error Response:\(response)")
                    
                    if let errMessage = response.data[SERVICE_RESPONSE.ErrorMessage] as? String {
                        
                        self.noMusiciansResponse(message: errMessage)
                    }
                }
                
                break
                
            default:
                
                if requestType == RequestType.getAllMusicians {
                    
                    print("Get All Musician Response:\(response)")
                    
                    if isInitial {
                        
                        if let responseData = response.data[SERVICE_RESPONSE.DataResponse] as? [Any] {
                            
                            self.recievedListOfMusiciansFromMQTT(listOfNewMusicians: responseData, listOfCurrentMusicians: arrayOfMusicians)
                            
                        }
                    }
                }

                break
        }
        
    }

    
    func noMusiciansResponse(message:String) {
        
        errMessageString = message
        arrayOfMusicians = []
        
        if delegate != nil {
            
            delegate?.emptyMusicianList(errMessage: message)
        }
    }
    
    /// Method recieve listOfCategories Array from Server
    ///
    /// - Parameter listOfCategories: consists array of Categories
    func recievedListOfCategories(arrayOfCategories:[[String:Any]]) {
    
        var isFoundCategory = false
        
        for eachCategory in arrayOfCategories {
            
            if let catName = eachCategory["cat_name"] as? String, let catId = eachCategory["_id"] as? String {
                
                if catName == "Notary" {
                    
                    isFoundCategory = true
                    categoryId = catId
                    
                    UserDefaults.standard.set(catId, forKey: USER_DEFAULTS.CATEGORY.id)
                    UserDefaults.standard.synchronize()
                    
                    if let visitFee = eachCategory["visit_fees"] as? Double {
                        
                        if visitFee > 0.0 {
                            
                            UserDefaults.standard.set(visitFee, forKey: USER_DEFAULTS.CATEGORY.visitFee)
                            UserDefaults.standard.synchronize()
                        }
                    }
                    
                    break
                }
            }
           
        }
        
        if !isFoundCategory {
            
            categoryId = ""
            
            UserDefaults.standard.set("", forKey: USER_DEFAULTS.CATEGORY.id)
            UserDefaults.standard.set(0.0, forKey: USER_DEFAULTS.CATEGORY.visitFee)
            UserDefaults.standard.synchronize()
        }
    }
    
    func sortNotaryListByDistance(listOfNewMusicians:[Any]) -> [Any]  {
        
        if let newNotariesList = listOfNewMusicians as? [[String:Any]] {
            
            return (newNotariesList.sorted(by: { ($0["distance"] as! Double) < ($1["distance"] as! Double) }) as [Any])
            
        } else {
            
            return listOfNewMusicians
        }
        
        
        
    }
    
    /// Method recieve listOfMusician Array from MQTT Model
    ///
    /// - Parameter listOfMusicians: consists array of Musicians who are online
    func recievedListOfMusiciansFromMQTT(listOfNewMusicians:[Any], listOfCurrentMusicians:[Any]) {
        
        if listOfNewMusicians.count > 0 {
            
            if let eachMusicianDetails = listOfNewMusicians[0] as? [String:Any] {
                
                if eachMusicianDetails["providerList"] != nil {
                    
                    return
                }
            }
        }
        
        print("List Before Sort:\(listOfNewMusicians)")
        
        let sortedListOfNewMusicians = self.sortNotaryListByDistance(listOfNewMusicians: listOfNewMusicians)
        
        
        print("List After Sort:\(sortedListOfNewMusicians)")
        
        UserDefaults.standard.set(sortedListOfNewMusicians, forKey: USER_DEFAULTS.MUSICIAN_LIST.PREVIOUS)
        UserDefaults.standard.synchronize()

        
        if sortedListOfNewMusicians.count == 0 {//checking musician list is empty or not
            
            arrayOfMusicians = sortedListOfNewMusicians
            
            if delegate != nil {
                
                delegate?.emptyMusicianList(errMessage: self.errMessageString)
            }

        }
        else {
            
            var arrayOfUpdatedMusicians:[MusicianDetailsModel] = []
        
            for Musician in sortedListOfNewMusicians {//Creating array of updated musician list
            
                let musicianDetailsModel = MusicianDetailsModel.init(musicianDetails: Musician)
                arrayOfUpdatedMusicians.append(musicianDetailsModel)
            }

            if listOfCurrentMusicians.isEmpty {//Checking previous musician list is empty or not
        
                arrayOfMusicians = sortedListOfNewMusicians
        
                if delegate != nil {
            
                    delegate?.showInitialMusicianList(arrayOfUpdatedMusiciansModel: arrayOfUpdatedMusicians,arrayOfMusiciansList:arrayOfMusicians)
                }
                
            } else if listOfCurrentMusicians.count == arrayOfUpdatedMusicians.count {
        
                let previous: NSArray = listOfCurrentMusicians as NSArray
                let updated: NSArray = sortedListOfNewMusicians as NSArray
        
                if previous.isEqual(to: updated as! [Any]) {
                    // Both are Equal
                    // Do not do anything
                    print("Musician List is Same")
            
                    arrayOfMusicians = sortedListOfNewMusicians
                    maintainUpdatedMusicianList(listOfNewMusicians: sortedListOfNewMusicians,arrayOfUpdatedMusicians:arrayOfUpdatedMusicians, listOfCurrentMusicians: listOfCurrentMusicians)
            
                }else {
            
                    // Both are not equal
                    print("Updated Musician List is not equal")
            
                    maintainUpdatedMusicianList(listOfNewMusicians: sortedListOfNewMusicians,arrayOfUpdatedMusicians:arrayOfUpdatedMusicians, listOfCurrentMusicians:listOfCurrentMusicians)
            
                }
            
            }else {
                
                maintainUpdatedMusicianList(listOfNewMusicians: sortedListOfNewMusicians,arrayOfUpdatedMusicians:arrayOfUpdatedMusicians, listOfCurrentMusicians:listOfCurrentMusicians)
            }
        }
    }
    
    
    /// Method to maintain updated musician list
    ///
    /// - Parameters:
    ///   - listOfNewMusicians: list of musican details
    ///   - arrayOfUpdatedMusicians: list of musican details model
    func maintainUpdatedMusicianList(listOfNewMusicians:[Any], arrayOfUpdatedMusicians:[MusicianDetailsModel], listOfCurrentMusicians:[Any]) {
        
        print("Musician List is Changed")
        
        var arrayOfRowsToRemove:[Int] = []
        var arrayOfRowsToAdd:[Int] = []
        var arrayOfMusicianIdToRemove:[String] = []
        
        //Finding Musicians to Add in the List
        for i in 0..<listOfNewMusicians.count {
            
            let musicianDetail = MusicianDetailsModel.init(musicianDetails: listOfNewMusicians[i])
            
            let predicate = NSPredicate(format: "id = %@", musicianDetail.providerId)
            let result = listOfCurrentMusicians.filter { predicate.evaluate(with: $0) }
            
            if result.count == 0 {
                
                arrayOfRowsToAdd.append(i)
            }
        }
        
        //Finding Musicians To Remove from the List
        for i in 0..<listOfCurrentMusicians.count {
            
            let musicianDetail = MusicianDetailsModel.init(musicianDetails: listOfCurrentMusicians[i])
            
            let predicate = NSPredicate(format: "id = %@", musicianDetail.providerId)
            let result = listOfNewMusicians.filter { predicate.evaluate(with: $0) }
            
            if result.count == 0 {
                
                arrayOfRowsToRemove.append(i)
                arrayOfMusicianIdToRemove.append(musicianDetail.providerId)
            }
        }
        
        
        arrayOfMusicians = listOfNewMusicians
        
        if delegate != nil {
            
            delegate?.updateMusicianList(arrayOfUpdatedMusicians: arrayOfUpdatedMusicians, arrayOfRowsToAdd: arrayOfRowsToAdd, arrayOfRowsToRemove: arrayOfRowsToRemove,arrayOfMusicianIdToRemove:arrayOfMusicianIdToRemove, arrayOfMusiciansList:arrayOfMusicians)
        }

    }
    
}

extension Array where Element:Any {
    func removeDuplicates() -> [Any] {
        
        var result = [Element]()
        for value in self {
            
            if !result.contains(where: {($0 as! AnyHashable  == value as! AnyHashable)}) {
                
                result.append(value)
            }
        }
        
        return (result as [Any])
    }
}


extension MusiciansListManager {
    
    func plotZonesOnMapView(pathsArray:[[String:Any]]) {
        
        DDLogVerbose("PathsArray = \(pathsArray)")
    
        gmsMutablePath = GMSMutablePath.init()
    
        for j in 0..<pathsArray.count {
            
            let lat = GenericUtility.strForObj(object:pathsArray[j]["lat"])
            let lng = GenericUtility.strForObj(object:pathsArray[j]["lng"])
            
            let position = CLLocationCoordinate2D.init(latitude: Double(lat)!,
                                                       longitude: Double(lng)!)
            
            gmsMutablePath.add(position)
        }
    
//          plotZoneOnHomeScreen()
    }
    
    func plotZoneOnHomeScreen() {
        
        if HomeScreenViewController.obj != nil {
            
            let homeVC = HomeScreenViewController.sharedInstance()
            
            polygon.map = homeVC.mapView
            polygon.map = nil
            
            polygon = GMSPolygon.init(path: gmsMutablePath)
            
            polygon.fillColor = #colorLiteral(red: 0.8745098039, green: 0.3058823529, blue: 0.5490196078, alpha: 0.3)
            polygon.strokeColor = #colorLiteral(red: 0.8745098039, green: 0.3058823529, blue: 0.5490196078, alpha: 1)
            polygon.strokeWidth = 2.0
            polygon.map = homeVC.mapView
        }
    }
    
    func checkZoneisChanged(currentLat:Double,
                            currentLong:Double) {
        
        if (currentLat == 0.0 || currentLong == 0.0 || categoryId.length == 0) {
            
            return
        }
        
        if gmsMutablePath == nil {
            
            self.getCategoriesServiceAPI(currentLat: currentLat, currentLong: currentLong)
        }
        else {
            
            if HomeScreenViewController.obj != nil {

                let position = CLLocationCoordinate2D.init(latitude: currentLat,
                                                           longitude: currentLong)
                
                if !GMSGeometryContainsLocation(position, gmsMutablePath, true) {
                    
                    self.getCategoriesServiceAPI(currentLat: currentLat, currentLong: currentLong)
                }
            }
        }
    }
}
