//
//  GetBookingUpdatesMQTTModel.swift
//  LiveM
//
//  Created by Raghavendra V on 07/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

//Booking Statuses
enum Booking_Status: Int {
    
    case Pending = 1
    case Requested = 2
    case Accepted = 3
    case Declined = 4
    case IgnoreOrExpire = 5
    case Ontheway = 6
    case Arrived = 7
    case Started = 8
    case Completed = 9
    case Raiseinvoice = 10
    case CancelByProvider = 11
    case CancelByCustomer = 12
    case UnassignedDispatch = 15
}


class BookingStatusResponseManager {
    
    var arrayOfOngoingBookings:[Any] = []
    var bookingId:Int64 = 0
    var bookingStatus = 0
    var navigationVC:UINavigationController!
    var bookingAlertController:UIAlertController!
    var bookingFlowVC:BookingFlowViewController!
    var arrayOfPendingInvoices:[Int64] = []
    var arrayOfPendingBookingStatus:[BookingStatusResponseModel] = []
    
    
    static var obj:BookingStatusResponseManager? = nil
    
    class func sharedInstance() -> BookingStatusResponseManager {
        
        if obj == nil {
            
            obj = BookingStatusResponseManager()
        }
        
        return obj!
    }
    
    
    /// Method to maintain Booking status messages from push or mqtt
    ///
    /// - Parameters:
    ///   - isFromPush: Boolean value for message from push or mqtt
    ///   - bookingStatusModel: BookingStatus response model
    func bookingStatusMessageFromPushOrMQTT(isFromPush:Bool, bookingStatusModel:BookingStatusResponseModel) {
        
        bookingStatus = 0;
        bookingId = bookingStatusModel.bookingId
        
        if let savedDetails = UserDefaults.standard.object(forKey: USER_DEFAULTS.BOOKINGS.ON_GOING_BOOKINGS) {
            
            arrayOfOngoingBookings =  savedDetails as! [Any]
            
        } else {
            
            arrayOfOngoingBookings = []
        }
        
        
        let predicate = NSPredicate(format: "bookingId = %@", String(bookingId))
        let result = arrayOfOngoingBookings.filter { predicate.evaluate(with: $0) }
        
        if result.count == 0 {//Booking id is already available in arrayOfOngoingBookings or not
            
            print("Adding New Booking Staus")
            
            let dict = [
                "bookingId": String(bookingId),
                "status": bookingStatusModel.bookingStatus
                ] as [String : Any]
            
            //Adding New Booking Details To List
            arrayOfOngoingBookings.append(dict)
            UserDefaults.standard.set(arrayOfOngoingBookings, forKey: USER_DEFAULTS.BOOKINGS.ON_GOING_BOOKINGS)
            UserDefaults.standard.synchronize()
            
        } else {
            
            //Updating Booking Status
            for i in 0..<arrayOfOngoingBookings.count {
                
                if let bookingDict:[String:Any] = arrayOfOngoingBookings[i] as? [String:Any] {
                    
                    if bookingDict["bookingId"] as? String == String(bookingId) {
                        
                        bookingStatus = bookingDict["status"] as! Int
                        
                        if bookingStatus != bookingStatusModel.bookingStatus {
                            
                            print("Updated Booking Staus")
                            
                            arrayOfOngoingBookings.remove(at: i)
                            
                            let dict = [
                                "bookingId": String(bookingId),
                                "status": bookingStatusModel.bookingStatus
                                ] as [String : Any]
                            
                            //Booking Status has Changed
                            arrayOfOngoingBookings.append(dict)
                            UserDefaults.standard.set(arrayOfOngoingBookings, forKey: USER_DEFAULTS.BOOKINGS.ON_GOING_BOOKINGS)
                            UserDefaults.standard.synchronize()
                        }
                        break
                    }
                    
                }
                
            }
            
        }
        
        //Checking Booking status is same or not
        if bookingStatus != bookingStatusModel.bookingStatus || bookingStatusModel.bookingStatus == Booking_Status.Started.rawValue {
            
            if SplashLoading.obj != nil {//Checking currently showing Splash loading or not
                
                var isNewBooking = true
                
                for index in 0..<arrayOfPendingBookingStatus.count {
                    
                    if arrayOfPendingBookingStatus[index].bookingId == bookingId {
                        
                        arrayOfPendingBookingStatus[index] = bookingStatusModel
                        isNewBooking = false
                        break
                    }
                }
                
                if isNewBooking {
                    
                    arrayOfPendingBookingStatus.append(bookingStatusModel)
                }
                
                
            } else {
                
                self.showBookingFlowMessages(bookingStatusModel: bookingStatusModel)
            }
            
        }
        
    }
    
    
    
    /// Show Pending booking statuses after Splash loading
    func showPendingBookingStatusAfterSplashLoading() {
        
        for i in 0..<arrayOfPendingBookingStatus.count {
            
            bookingId = arrayOfPendingBookingStatus[i].bookingId
            self.showBookingFlowMessages(bookingStatusModel: arrayOfPendingBookingStatus[i])
        }
        
        arrayOfPendingBookingStatus = []
        
        showPendingReviewsAfterSplashLoading()
        
    }
    
    
    /// Method to show respective booking status messages
    ///
    /// - Parameter bookingStatusModel: booking status response model
    /// 1: requestl, 2: received, 3: accept, 4: reject, 5: ignore/Expire, 6- OntheWay , 7- Arrived, 8- started, 9- raise invoice, 10- completed, 11 - cancel by provider, 12 - cancel by customer
    
    func showBookingFlowMessages(bookingStatusModel:BookingStatusResponseModel) {
        
        
        bookingStatus = bookingStatusModel.bookingStatus
        
        
        print("Showing Updated Booking Staus")
        
        switch bookingStatus {
                
            case Booking_Status.Pending.rawValue://NewBooking created from dispatch
                
                UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.NewBookingisCreated)
                UserDefaults.standard.synchronize()
                
                if !(Helper.getCurrentVC() is HomeScreenViewController) {
                    
                    Helper.getCurrentVC().navigationController?.popToRootViewController(animated: false)
                }
                
                LeftMenuTableViewController.sharedInstance().changeViewController(LeftMenu.myEvent)
            
                showBookingFlowMessageCustomNotification(bookingStatusModel: bookingStatusModel)
            
            
            case Booking_Status.UnassignedDispatch.rawValue://Booking created back from dispatch
                
                UserDefaults.standard.set(true, forKey: USER_DEFAULTS.USER.NewBookingisCreated)
                UserDefaults.standard.synchronize()
                
                if !(Helper.getCurrentVC() is HomeScreenViewController) {
                    
                    Helper.getCurrentVC().navigationController?.popToRootViewController(animated: false)
                }
                
                LeftMenuTableViewController.sharedInstance().changeViewController(LeftMenu.myEvent)
                
                showBookingFlowMessageCustomNotification(bookingStatusModel: bookingStatusModel)
                
            case Booking_Status.CancelByProvider.rawValue://Declined by Musician
                
//                Helper.showAlert(head: ALERTS.Message, message: bookingStatusModel.bookingStatusMessage)
                
                if Helper.getCurrentVC() is BookingFlowViewController {
                    
                    bookingFlowVC = BookingFlowViewController.sharedInstance()
                    
                    if bookingFlowVC.bookingId == bookingId {
                        
                        //If Showing Same Booking
                        bookingFlowVC.navigationLeftButtonAction(bookingFlowVC.navigationLeftButton)
                    }
                }
                
                showBookingFlowMessageCustomNotification(bookingStatusModel: bookingStatusModel)
                updateDatainBookingHistoryClass(bookingStatusModel: bookingStatusModel)
                
                break
                
            case Booking_Status.Declined.rawValue, Booking_Status.IgnoreOrExpire.rawValue:
                
//                if bookingStatus == Booking_Status.Declined.rawValue {
//
//                    Helper.showAlert(head: ALERTS.Message, message: bookingStatusModel.bookingStatusMessage)
//                }
                
                if Helper.getCurrentVC() is BookingDetailsViewController {
                    
                    let bookingDetailsVC = BookingDetailsViewController.sharedInstance()
                    
                    if bookingDetailsVC.bookingDetailsModel.bookingId == bookingId {
                        
                        //If Showing Same Booking
                        if MyEventViewController.obj != nil {
                            
                            MyEventViewController.sharedInstance().gotoPastBooking = true
                            MyEventViewController.sharedInstance().gotoPendingBooking = false
                            MyEventViewController.sharedInstance().gotoUpcomingBooking = false
                        }
                    bookingDetailsVC.navigationLeftButtonAction(bookingDetailsVC.navigationLeftButton)
                    }
                }
    
                showBookingFlowMessageCustomNotification(bookingStatusModel: bookingStatusModel)
                updateDatainBookingHistoryClass(bookingStatusModel: bookingStatusModel)
                
                break
                
                
            default:
                //For Remaining Status
                if !(Helper.getCurrentVC() is BookingFlowViewController) {
                    
                    if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                        
                        //Booking Completed
                        if Helper.getCurrentVC() is InvoiceViewController {
                            
                            if !arrayOfPendingInvoices.contains(bookingId) {
                                
                                arrayOfPendingInvoices.append(bookingId)
                            }
                            
                        } else {
                            
                            if !arrayOfPendingInvoices.contains(bookingId) {
                                
                                arrayOfPendingInvoices.append(bookingId)
                                showInvoiceScreen(bid: bookingStatusModel.bookingId)
                            }
                            
                        }
                        
                    }
                    else {
                        
                        if bookingStatus == Booking_Status.Accepted.rawValue {
                            
                            if Helper.getCurrentVC() is BookingDetailsViewController {
                                
                                let bookingDetailsVC = BookingDetailsViewController.sharedInstance()
                                
                                if bookingDetailsVC.bookingDetailsModel.bookingId == bookingId {
                                    
                                    //If Showing Same Booking
                                    if MyEventViewController.obj != nil {
                                        
                                        MyEventViewController.sharedInstance().gotoPastBooking = false
                                        MyEventViewController.sharedInstance().gotoPendingBooking = false
                                        MyEventViewController.sharedInstance().gotoUpcomingBooking = true
                                    }
                                    bookingDetailsVC.navigationLeftButtonAction(bookingDetailsVC.navigationLeftButton)
                                }
                            }
                        } else if Helper.getCurrentVC() is LiveTrackViewController {
                            
                            let liveTrackVC = LiveTrackViewController.sharedInstance()
                            
                            if liveTrackVC.bookingDetailModel.bookingId == bookingId {
                                
                                if BookingFlowViewController.obj != nil {
                                    
                                    bookingFlowVC = BookingFlowViewController.sharedInstance()
                                    
                                    if bookingFlowVC.bookingDetailModel.bookingId == bookingId {
                                        
                                        //Call Service
                                        bookingFlowVC.getBookingDetailsAPI()
                                    }
                                }
                        
                            liveTrackVC.navigationLeftButtonAction(liveTrackVC.navigationLeftButton)
                            }
                        }
                        
                        showBookingFlowMessageCustomNotification(bookingStatusModel: bookingStatusModel)
                    }
                    
                    
                } else {
                    
                    bookingFlowVC = BookingFlowViewController.sharedInstance()
                    
                    if bookingFlowVC.bookingId != bookingId {
                        
                        //Booking Flow Controller with Different BID
                        
                        if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                            
                            //Booking Completed
                            if !arrayOfPendingInvoices.contains(bookingId) {
                                
                                arrayOfPendingInvoices.append(bookingId)
                                showInvoiceScreen(bid: bookingStatusModel.bookingId)
                            }
                        }
                        else {
                            
                            showBookingFlowMessageCustomNotification(bookingStatusModel: bookingStatusModel)
                        }
                    }
                    else {
                        
                        //Booking Flow Controller with same BID
                        bookingFlowVC.bookingStatus = bookingStatus
                        bookingFlowVC.bookingId = bookingId
                        
                        updateDatainBookingFlowClass(bookingStatusModel: bookingStatusModel)
                        
                        
                        if bookingStatus > Booking_Status.Ontheway.rawValue {
                            
                            bookingFlowVC.cancelButton.isHidden = true
                            
                        } else {
                            
                            bookingFlowVC.cancelButton.isHidden = false
                        }
                        
                        //For Booking Timer
                        if bookingStatus == Booking_Status.Started.rawValue {
                            
                            updateBookingTimerDetails(bookingStatusModel: bookingStatusModel)
                            bookingFlowVC.showBookingTimerDetails()
                            
                        } else {
                            
                            bookingFlowVC.hideBookingTimerDetailsViewAnimation()
                        }
                        
                        
                        bookingFlowVC.tableView.reloadData()
                        
                        bookingFlowVC.scrollTableViewToShowCusrrentBookingStatus()
                        
                        if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                            
                            //Show Invoice Controller
                            if !arrayOfPendingInvoices.contains(bookingId) {
                                
                                arrayOfPendingInvoices.append(bookingId)
                                showInvoiceScreen(bid: bookingStatusModel.bookingId)
                            }
                            
                        }
                    }
                }
                
                updateDatainBookingHistoryClass(bookingStatusModel: bookingStatusModel)
                
                break
        }
        
    }
    
    
    
    /// Method to show
    ///
    /// - Parameter bookingStatusModel: booking status response model
    func showBookingFlowMessageCustomNotification(bookingStatusModel:BookingStatusResponseModel) {
        
        if LocalNotificationView.share != nil {
            
            LocalNotificationView.share?.removeFromSuperview()
            LocalNotificationView.share = nil
            
        }
        
        let localNotificationView = LocalNotificationView.sharedInstance
        localNotificationView.bookingStatusModel = bookingStatusModel
        
        WINDOW_DELEGATE??.addSubview(localNotificationView)
        
        localNotificationView.showBookingNotificationDetails()
        
        
        Helper.playLocalNotificationSound()
        
        localNotificationView.springAnimationForNotificationView()
        
    }
    
    
    /// Method to Show booking status message in Booking status view controller
    func showBookingStatusinBookingFlowScreen() {
        
        if Helper.getCurrentVC() is BookingFlowViewController {//Cusrrent controller is Booking flow VC
            
            var bookingFlowVC = BookingFlowViewController.sharedInstance()
            
            bookingFlowVC = BookingFlowViewController.sharedInstance()
            
            bookingFlowVC.bookingId = bookingId
            bookingFlowVC.bookingStatus = bookingStatus
            
            bookingFlowVC.title = "\(ALERTS.BOOKING_FLOW.EventID)" + " : " + String(bookingId) //String(format:"%td", bookingId)
            
            //Call Service
            bookingFlowVC.getBookingDetailsAPI()
            
        }
        else {
            
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = storyboard.instantiateViewController(withIdentifier: VCIdentifier.bookingFlowVC) as? BookingFlowViewController
            
            vc?.bookingStatus = bookingStatus
            vc?.bookingId = bookingId
            
            TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
                                                                      subType: kCATransitionFromTop,
                                                                      for: (Helper.getCurrentVC().navigationController?.view)!,
                                                                      timeDuration: 0.3)
            
            
            Helper.getCurrentVC().navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    
    
    /// Update booking status in my events controller without call service
    ///
    /// - Parameter bookingStatusModel: booking status response model
    func updateDatainBookingHistoryClass(bookingStatusModel: BookingStatusResponseModel) {
        
        if Helper.getCurrentVC() is MyEventViewController  {
            
            let myEventClass = MyEventViewController.sharedInstance()
            
            myEventClass.updateBookingDetailsAsynchronously(bookingStatusModel: bookingStatusModel)
            
        } else if MyEventViewController.obj != nil {
            
            let myEventClass = MyEventViewController.sharedInstance()
            
            myEventClass.updateBookingDetailsAsynchronously(bookingStatusModel: bookingStatusModel)
        }
    }
    
    
    
    /// Update each booking status dates in BookingFlow VC
    ///
    /// - Parameter bookingStatusModel: booking status response model
    func updateDatainBookingFlowClass(bookingStatusModel: BookingStatusResponseModel) {
        
        switch bookingStatus {
            
        case Booking_Status.Accepted.rawValue:
            
            bookingFlowVC.bookingDetailModel.acceptedDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        case Booking_Status.Ontheway.rawValue:
            
            bookingFlowVC.bookingDetailModel.onThewayDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        case Booking_Status.Arrived.rawValue:
            
            bookingFlowVC.bookingDetailModel.arrivedDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        case Booking_Status.Started.rawValue:
            
            bookingFlowVC.bookingDetailModel.startedDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        case Booking_Status.Completed.rawValue:
            
            bookingFlowVC.bookingDetailModel.completedDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        case Booking_Status.Raiseinvoice.rawValue:
            
            bookingFlowVC.bookingDetailModel.invoiceRaisedDate = bookingStatusModel.statusUpdatedTime
            break
            
            
        default:
            break
        }
    }
    
    
    
    /// Update each booking Timer details in BookingFlow VC
    ///
    /// - Parameter bookingStatusModel: booking status response model
    func updateBookingTimerDetails(bookingStatusModel:BookingStatusResponseModel) {
        
        if !bookingStatusModel.bookingTimerDetails.isEmpty {
            
            if let timerStatusValue = bookingStatusModel.bookingTimerDetails["status"] as? Int {
                
                bookingFlowVC.bookingDetailModel.timerStatus = timerStatusValue
            }
            
            if let timerStampValue = bookingStatusModel.bookingTimerDetails["startTimeStamp"] as? Int {
                
                bookingFlowVC.bookingDetailModel.timerStamp = timerStampValue
            }
            
            if let totalTimeElapsedValue = bookingStatusModel.bookingTimerDetails["second"] as? Int {
                
                bookingFlowVC.bookingDetailModel.totalTimeElapsed = totalTimeElapsedValue
            }
            
        }
        
    }
    
    
    
    /// Show Invoice view controller
    ///
    /// - Parameter bid: booking id to show Invoice
    func showInvoiceScreen(bid:Int64) {
        
        if LocalNotificationView.share != nil {
            
            if LocalNotificationView.share?.bookingStatusModel.bookingId == bid {
                
                LocalNotificationView.share?.removeFromSuperview()
                LocalNotificationView.share = nil
                
            }
            
        }
        
        Helper.playLocalNotificationSound()
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: VCIdentifier.invoiceVC) as? InvoiceViewController
        
        vc?.bookingId = bid
        
        if (Helper.getCurrentVC().navigationController != nil) {
            
            TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
                                                                      subType: kCATransitionFromTop,
                                                                      for: (Helper.getCurrentVC().navigationController?.view)!,
                                                                      timeDuration: 0.3)
            
            Helper.getCurrentVC().navigationController?.pushViewController(vc!, animated: false)
        }
        
    }
    
    
    func updateNotReviewedBookingsArray(notReviewedBookings:[Any]) {
        
        for eachBookingId in notReviewedBookings {
            
            if let bookingId = eachBookingId as? Int64 {
                
                if !arrayOfPendingInvoices.contains(bookingId) {
                    
                    arrayOfPendingInvoices.append(bookingId)
                }
            }
        }
        
        if SplashLoading.obj == nil {//Checking currently showing Splash loading or not
            
            if arrayOfPendingInvoices.count > 0 {
                
                //Booking Completed
                if !(Helper.getCurrentVC() is InvoiceViewController) {
                    
                    showInvoiceScreen(bid: arrayOfPendingInvoices[0])
                }
            }
        }

    }
    
    func showPendingReviewsAfterSplashLoading() {
        
        if arrayOfPendingInvoices.count > 0 {
            
            //Booking Completed
            if !(Helper.getCurrentVC() is InvoiceViewController) {
                
                showInvoiceScreen(bid: arrayOfPendingInvoices[0])
            }
        }
    }
    
    
    
    /// Show remaining completed booking invoice
    ///
    /// - Parameter showedBookingId: already invoice showed booking id
    func showRemainingCompletedBookingInvoice(_ showedBookingId:Int64) {
        
        if arrayOfPendingInvoices.contains(showedBookingId) {
            
            arrayOfPendingInvoices.remove(at:arrayOfPendingInvoices.index(of: showedBookingId)!)
        }
        
        if arrayOfPendingInvoices.count > 0 {
            
            showInvoiceScreen(bid: arrayOfPendingInvoices[0])
        }
        
    }
    
}
