//
//  BookingFlowTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension BookingFlowViewController:UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if bookingDetailModel != nil {
            
            return 2
        }
        return 0

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            if bookingDetailModel != nil {
                
                return 7
                
            } else {
                
                return 0
            }
        } else {
            
            if bookingDetailModel != nil {
                
                return bookingDetailModel.arrayOfFees.count + 1
            }
            return 0
        }
    
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
            return view
            
        } else {
            
            let headerCell:CBTableHeaderViewCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! CBTableHeaderViewCell
            
            headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.YourPaymentBreakDown
            
            return headerCell.contentView
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            return 0.01
            
        } else {
            
            return UITableViewAutomaticDimension
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            switch indexPath.row {
                    
                case 1://Accepted
                    
                    if bookingStatus >= Booking_Status.Accepted.rawValue {
                        
                        if bookingStatus >= Booking_Status.Completed.rawValue {
                            
                            let cell = self.tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath) as! BookingFlowNormalTableViewCell
                            
                            let dateFormat = DateFormatter.initTimeZoneDateFormat()
                            dateFormat.dateFormat = "hh:mm\na"
                            
                            cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistAccepted
                            
                            let acceptedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.acceptedDate))
                            
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = APP_COLOR
                            cell.timeLabel.text = dateFormat.string(from: acceptedDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                            cell.statusNameLabel.textColor = APP_COLOR
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.bottomStatusLineView.backgroundColor = APP_COLOR
                            
                            cell.topStatusLineView.isHidden = false
                            cell.bottomStatusLineView.isHidden = false
                            
                            return cell
                            
                        } else {
                            
                            let cell = self.tableView.dequeueReusableCell(withIdentifier: "acceptCell", for: indexPath) as! BookingFlowAcceptedTableViewCell
                            
                            cell.callButton.addTarget(self, action: #selector(callButtonAction), for: .touchUpInside)
                            cell.messageButton.addTarget(self, action: #selector(messageButtonAction), for: .touchUpInside)
                            
                            cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistAccepted
                            
                            let dateFormat = DateFormatter.initTimeZoneDateFormat()
                            dateFormat.dateFormat = "hh:mm\na"
                            
                            let acceptedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.acceptedDate))
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.topStatusLineView.isHidden = false
                            
                            if bookingStatus == Booking_Status.Accepted.rawValue {
                                
                                cell.timeLabel.isHidden = false
                                cell.timeLabel.textColor = UIColor.darkGray
                                cell.timeLabel.text = dateFormat.string(from: acceptedDate)
                                cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                                cell.statusNameLabel.textColor = UIColor.darkGray
                                cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                                cell.bottomStatusLineView.isHidden = true
                                
                            } else {
                                
                                cell.timeLabel.isHidden = false
                                cell.timeLabel.textColor = APP_COLOR
                                cell.timeLabel.text = dateFormat.string(from: acceptedDate)
                                cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                                cell.statusNameLabel.textColor = APP_COLOR
                                cell.bottomStatusLineView.backgroundColor = APP_COLOR
                                cell.bottomStatusLineView.isHidden = false
                                
                            }
                            
                            
                            cell.acceptedMessagelabel.text = bookingDetailModel.providerName + " has accepted your request."
                            
                            if bookingDetailModel.providerImageURL.length > 0 {
                                
                                cell.musicianImageView.kf.setImage(with: URL(string: bookingDetailModel.providerImageURL),
                                                                   placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                                                   options: [.transition(ImageTransition.fade(1))],
                                                                   progressBlock: { receivedSize, totalSize in
                                },
                                                                   completionHandler: { image, error, cacheType, imageURL in
                                                                    
                                })
                                
                            } else {
                                
                                cell.musicianImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
                            }
                            
                            
                            return cell
                        }
                        
                    } else {
                        
                        let cell = self.tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath) as! BookingFlowNormalTableViewCell
                        
                        cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistAccepted
                        
                        cell.timeLabel.isHidden = true
                        cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                        cell.statusNameLabel.textColor = UIColor.lightGray
                        
                        cell.topStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                        cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                        
                        cell.topStatusLineView.isHidden = true
                        cell.bottomStatusLineView.isHidden = true

                        return cell
                        
                    }
                    
                    
                case 2://Ontheway
                    
                    if bookingStatus == Booking_Status.Ontheway.rawValue {
                        
                        let cell = self.tableView.dequeueReusableCell(withIdentifier: "liveTrackCell", for: indexPath) as! BookingFlowLiveTrackTableViewCell
                        
                        cell.liveTrackButton.addTarget(self, action: #selector(liveTrackButtonAction), for: .touchUpInside)
                        
                        cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistOnTheWay
                        
                        cell.liveTrackButton.layer.borderColor = APP_COLOR.cgColor
                        
                        let dateFormat = DateFormatter.initTimeZoneDateFormat()
                        dateFormat.dateFormat = "hh:mm\na"
                        
                        let onTheWayDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.onThewayDate))
                        
                        //                if bookingStatus == Booking_Status.Ontheway.rawValue {
                        
                        cell.timeLabel.isHidden = false
                        cell.timeLabel.textColor = UIColor.darkGray
                        cell.timeLabel.text = dateFormat.string(from: onTheWayDate)
                        cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                        cell.statusNameLabel.textColor = UIColor.darkGray
                        
                        cell.topStatusLineView.backgroundColor = APP_COLOR
                        cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                        //                }
                        
                        cell.topStatusLineView.isHidden = false
                        cell.bottomStatusLineView.isHidden = true
                        
                        return cell
                        
                    } else {
                        
                        let cell = self.tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath) as! BookingFlowNormalTableViewCell
                        
                        cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistOnTheWay
                        
                        if bookingStatus > Booking_Status.Ontheway.rawValue {
                            
                            let dateFormat = DateFormatter.initTimeZoneDateFormat()
                            dateFormat.dateFormat = "hh:mm\na"
                            
                            let onTheWayDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.onThewayDate))
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = APP_COLOR
                            cell.timeLabel.text = dateFormat.string(from: onTheWayDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                            cell.statusNameLabel.textColor = APP_COLOR
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.bottomStatusLineView.backgroundColor = APP_COLOR
                            
                            cell.topStatusLineView.isHidden = false
                            cell.bottomStatusLineView.isHidden = false
                            
                        } else {
                            
                            cell.timeLabel.isHidden = true
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                            cell.statusNameLabel.textColor = UIColor.lightGray
                            
                            cell.topStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            
                            cell.topStatusLineView.isHidden = true
                            cell.bottomStatusLineView.isHidden = true
                        }
  
                        return cell
                        
                    }
                    
                default:
                    
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath) as! BookingFlowNormalTableViewCell
                    
                    let dateFormat = DateFormatter.initTimeZoneDateFormat()
                    dateFormat.dateFormat = "hh:mm\na"
                    
                    switch indexPath.row {
                        
                    case 0://Requested
                        
                        cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistRequested
                        
                        let requestedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.bookingStartTime))
                        
                        cell.topStatusLineView.isHidden = true
                        
                        if bookingStatus < Booking_Status.Requested.rawValue {
                            
                            cell.timeLabel.isHidden = true
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                            cell.statusNameLabel.textColor = UIColor.lightGray
                            
                            cell.topStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            
                            cell.bottomStatusLineView.isHidden = true
                            
                        } else if bookingStatus == Booking_Status.Requested.rawValue {
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = UIColor.darkGray
                            cell.timeLabel.text = dateFormat.string(from: requestedDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                            cell.statusNameLabel.textColor = UIColor.darkGray
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            
                            cell.bottomStatusLineView.isHidden = true
                            
                        } else {
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = APP_COLOR
                            cell.timeLabel.text = dateFormat.string(from: requestedDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                            cell.statusNameLabel.textColor = APP_COLOR
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.bottomStatusLineView.backgroundColor = APP_COLOR
                            
                            cell.bottomStatusLineView.isHidden = false
                            
                        }
                        
                        break
                        
                    case 3://Arrived
                        
                        cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.ArtistArrived
                        
                        let arrivedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.arrivedDate))
                        
                        if bookingStatus < Booking_Status.Arrived.rawValue {
                            
                            cell.timeLabel.isHidden = true
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                            cell.statusNameLabel.textColor = UIColor.lightGray
                            
                            cell.topStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            
                            cell.topStatusLineView.isHidden = true
                            cell.bottomStatusLineView.isHidden = true
                            
                        } else if bookingStatus == Booking_Status.Arrived.rawValue {
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = UIColor.darkGray
                            cell.timeLabel.text = dateFormat.string(from: arrivedDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                            cell.statusNameLabel.textColor = UIColor.darkGray
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            
                            cell.topStatusLineView.isHidden = false
                            cell.bottomStatusLineView.isHidden = true
                            
                        } else {
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = APP_COLOR
                            cell.timeLabel.text = dateFormat.string(from: arrivedDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                            cell.statusNameLabel.textColor = APP_COLOR
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.bottomStatusLineView.backgroundColor = APP_COLOR
                            
                            cell.topStatusLineView.isHidden = false
                            cell.bottomStatusLineView.isHidden = false
                        }
                        
                        
                        break
                        
                    case 4://Started
                        
                        cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.EventStarted
                        
                        
                        let startedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.startedDate))
                        
                        if bookingStatus < Booking_Status.Started.rawValue {
                            
                            cell.timeLabel.isHidden = true
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                            cell.statusNameLabel.textColor = UIColor.lightGray
                            
                            cell.topStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            
                            cell.topStatusLineView.isHidden = true
                            cell.bottomStatusLineView.isHidden = true
                            
                            
                        } else if bookingStatus == Booking_Status.Started.rawValue {
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = UIColor.darkGray
                            cell.timeLabel.text = dateFormat.string(from: startedDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                            cell.statusNameLabel.textColor = UIColor.darkGray
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            
                            cell.topStatusLineView.isHidden = false
                            cell.bottomStatusLineView.isHidden = true
                            
                            
                        } else {
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = APP_COLOR
                            cell.timeLabel.text = dateFormat.string(from: startedDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                            cell.statusNameLabel.textColor = APP_COLOR
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.bottomStatusLineView.backgroundColor = APP_COLOR
                            
                            cell.topStatusLineView.isHidden = false
                            cell.bottomStatusLineView.isHidden = false
                            
                        }
                        
                        
                        break
                        
                    case 5://Completed
                        
                        cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.EventCompleted
                        
                        let completedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.completedDate))
                        
                        if bookingStatus < Booking_Status.Completed.rawValue {
                            
                            cell.timeLabel.isHidden = true
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                            cell.statusNameLabel.textColor = UIColor.lightGray
                            
                            cell.topStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            
                            cell.topStatusLineView.isHidden = true
                            cell.bottomStatusLineView.isHidden = true
                            
                        } else if bookingStatus == Booking_Status.Completed.rawValue {
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = UIColor.darkGray
                            cell.timeLabel.text = dateFormat.string(from: completedDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                            cell.statusNameLabel.textColor = UIColor.darkGray
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.bottomStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            
                            cell.topStatusLineView.isHidden = false
                            cell.bottomStatusLineView.isHidden = true
                            
                        } else {
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = APP_COLOR
                            cell.timeLabel.text = dateFormat.string(from: completedDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                            cell.statusNameLabel.textColor = APP_COLOR
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            cell.bottomStatusLineView.backgroundColor = APP_COLOR
                            
                            cell.topStatusLineView.isHidden = false
                            cell.bottomStatusLineView.isHidden = false
                            
                        }
                        
                        break
                        
                    case 6://Invoice Raised
                        
                        cell.statusNameLabel.text = ALERTS.BOOKING_FLOW.InvoiceRaised
                        
                        let invoiceRaisedDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailModel.invoiceRaisedDate))
                        
                        cell.bottomStatusLineView.isHidden = true
                        
                        if bookingStatus < Booking_Status.Raiseinvoice.rawValue {
                            
                            cell.timeLabel.isHidden = true
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse1")
                            cell.statusNameLabel.textColor = UIColor.lightGray
                            
                            cell.topStatusLineView.backgroundColor = BOOKING_FLOW_LIGHT_COLOR
                            
                            cell.topStatusLineView.isHidden = true
                            
                        } else if bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                            
                            //                            cell.timeLabel.isHidden = false
                            //                            cell.timeLabel.textColor = UIColor.darkGray
                            //                            cell.timeLabel.text = dateFormat.string(from: invoiceRaisedDate)
                            //                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse2")
                            //                            cell.statusNameLabel.textColor = UIColor.darkGray
                            //
                            //                        } else {
                            
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.textColor = APP_COLOR
                            cell.timeLabel.text = dateFormat.string(from: invoiceRaisedDate)
                            cell.statusImageView.image = #imageLiteral(resourceName: "Ellipse3")
                            cell.statusNameLabel.textColor = APP_COLOR
                            
                            cell.topStatusLineView.backgroundColor = APP_COLOR
                            
                            cell.topStatusLineView.isHidden = false
                            
                        }
                        
                        break
                        
                    default:
                        break
                        
                    }
                    
                    return cell
                    
            }
        } else {
            
            if indexPath.row == (self.bookingDetailModel.arrayOfFees.count + 1) - 1 {
                
                let paymentTotalCell:CBTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "paymentTotalCell", for: indexPath) as! CBTotalTableViewCell
                
                let totalValue =  bookingDetailModel.bookingTotalAmount
                
                paymentTotalCell.totalValueLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:" %.2f",totalValue), currencySymbol: bookingDetailModel.currencySymbol )
                
                return paymentTotalCell
                
            } else {
                
                let paymentBreakdownCell:CBPaymentBreakDownCell = tableView.dequeueReusableCell(withIdentifier: "paymentBreakdownCell", for: indexPath) as! CBPaymentBreakDownCell
                
                paymentBreakdownCell.titleLabel.text = (bookingDetailModel.arrayOfFees[indexPath.row].keys.first)!
                
                paymentBreakdownCell.amountLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f", (bookingDetailModel.arrayOfFees[indexPath.row].values.first)! as! CVarArg), currencySymbol: bookingDetailModel.currencySymbol)
                
                return paymentBreakdownCell
                
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            switch indexPath.row {
                    
                case 1://Accepted
                    
                    if bookingStatus >= Booking_Status.Accepted.rawValue {
                        
                        if bookingStatus >= Booking_Status.Completed.rawValue {
                            
                            return 70
                        }
                        return 150
                        
                    } else {
                        
                        return 70
                    }
                    
                case 2://Ontheway
                    
                    if bookingStatus == Booking_Status.Ontheway.rawValue {
                        
                        return 120
                        
                    } else {
                        
                        return 70
                    }
                    
                default:
                    
                    return 70
                    
            }
        } else {
            
            return 30
        }
        
    }
    
    func scrollTableViewToShowCusrrentBookingStatus() {
        
        var indexPath = IndexPath.init(row: 0, section: 0)
        
        switch bookingStatus {
                
            case Booking_Status.Accepted.rawValue:
                
                indexPath.row = 1
                break
                
                
            case Booking_Status.Ontheway.rawValue:
                
                indexPath.row = 2
                break
                
                
            case Booking_Status.Arrived.rawValue:
                
                indexPath.row = 3
                break
                
                
            case Booking_Status.Started.rawValue:
                
                indexPath.row = 4
                break
                
                
            case Booking_Status.Completed.rawValue:
                
                indexPath.row = 5
                break
                
                
            case Booking_Status.Raiseinvoice.rawValue:
                
                indexPath.row = 6
                break
                
                
            default:
                break
        }
        
        self.tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.middle, animated: true)
        
    }
    
}
