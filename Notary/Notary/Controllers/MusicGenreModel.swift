//
//  MusicGenreModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 05/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

class MusicGenreModel {
    
    var musicGenreId = ""
    var musicGenreName = ""
    
    
    init(musicGenreDetails:Any) {
        
        if let musicGenre = musicGenreDetails as? [String:Any] {
            
            musicGenreId = GenericUtility.strForObj(object: musicGenre[SERVICE_RESPONSE.Genres_Id])
            
            musicGenreName = GenericUtility.strForObj(object: musicGenre[SERVICE_RESPONSE.Genres_Name])
        }
    }
    
}
