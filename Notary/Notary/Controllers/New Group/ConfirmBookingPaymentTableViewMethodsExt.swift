//
//  ConfirmBookingPaymentTableViewMethodsExt.swift
//  Notary
//
//  Created by 3Embed on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension ConfirmBookingPaymentViewController:UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3//Card, Wallet, Cash
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return arrayOfCards.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as! PaymentTableViewCell
        
        let eachCardDetail:CardDetailsModel = arrayOfCards[indexPath.row]
        
        cell.cardNumberLabel.text = "**** **** **** " + eachCardDetail.last4Digits
        cell.cardImage.image = Helper.cardImage(with: eachCardDetail.brand)
        
        cell.tickImageWidthConstraint.constant = 0
        
        if eachCardDetail.defaultCard == true {
            
            cell.defaultCardLabel.isHidden = false
            
        } else {
            
            cell.defaultCardLabel.isHidden = true
        }
        
        return cell
    }
}

extension ConfirmBookingPaymentViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Save Selected Card details in Confirm Booking Model
        CBModel.selectedCardModel = arrayOfCards[indexPath.row]
    }
}
