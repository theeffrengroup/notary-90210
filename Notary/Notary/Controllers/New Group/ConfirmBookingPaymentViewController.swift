//
//  ConfirmBookingPaymentViewController.swift
//  Notary
//
//  Created by 3Embed on 28/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit

class ConfirmBookingPaymentViewController: UIViewController {
    
    // MARK: - Outlets -
    //TableView
    @IBOutlet weak var tableView: UITableView!
    
    var arrayOfCards = [CardDetailsModel]()
    var selectedCardIndexPath:IndexPath!
    var selectedPaymentType = 1 //1-Cash, 2-Card, 3-Wallet
    var selectedSecondPaymentTypeForWallet = 1//1-Cash, 2-Card
    
    var CBModel:ConfirmBookingModel!
    
    var isPaymentListChanged:Bool = false
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    var apiTag:Int!
    
    let confirmBookingPaymentViewModel = ConfirmBookingPaymentViewModel()
    
    static var obj:ConfirmBookingPaymentViewController? = nil
    
    
    class func sharedInstance() -> ConfirmBookingPaymentViewController {
        
        return obj!
    }
    
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ConfirmBookingPaymentViewController.obj = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
                
        if isPaymentListChanged {
            
            getCards()
        }
        
        acessClass.acessDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Methods -
    
    @IBAction func addNewCardAction(_ sender: Any) {
        
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        
        
    }
    
}

extension ConfirmBookingPaymentViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getCardDetails.rawValue:
            
            getCards()
            
            break
            
        default:
            break
        }
    }
    
}
