//
//  SupportDetailsModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 29/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

class SupportDetailsModel {
    
    var name = ""
    var innerSupport = [Any]()
    var link = ""
    
    
    init(supportResponse:Any?) {
        
        if let supportDetails = supportResponse as? [String:Any] {
            
            name = GenericUtility.strForObj(object: supportDetails["Name"])
            link = GenericUtility.strForObj(object: supportDetails["link"])
            
            if let innerSup = supportDetails["subcat"] as? [Any] {
                
                innerSupport = innerSup
            }
            
        }
        
    }
    
    
    
}
