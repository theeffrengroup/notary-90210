//
//  ConfirmBookingViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class ConfirmBookingViewModel {
    
    var CBModel:ConfirmBookingModel!
    var promoCode = ""
    let disposebag = DisposeBag()
    let rxConfirmBookingAPI = ConfirmBookingAPI()
    
    func liveBookingAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxConfirmBookingAPI.liveBookingServiceAPICall(confirmBookingModel: CBModel)
        
        if !rxConfirmBookingAPI.liveBooking_Response.hasObservers {
            
            rxConfirmBookingAPI.liveBooking_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
    func scheduleBookingAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxConfirmBookingAPI.scheduleBookingServiceAPICall(confirmBookingModel: CBModel)
        
        if !rxConfirmBookingAPI.scheduleBooking_Response.hasObservers {
            
            rxConfirmBookingAPI.scheduleBooking_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
    func customerLastDueAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxConfirmBookingAPI.getCustomerLastDuesServiceAPICall(confirmBookingModel: CBModel)
        
        if !rxConfirmBookingAPI.customerLastDue_Response.hasObservers {
            
            rxConfirmBookingAPI.customerLastDue_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
    
    func validatePromocodeAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxConfirmBookingAPI.validatePromocodeServiceAPICall(confirmBookingModel:
            CBModel,
                                                            promoCode:promoCode)
        
        if !rxConfirmBookingAPI.validatePromocode_Response.hasObservers {
            
            rxConfirmBookingAPI.validatePromocode_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
}

