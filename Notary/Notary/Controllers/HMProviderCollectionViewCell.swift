//
//  HMProviderCollectionViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 11/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class HMProviderCollectionViewCell:UICollectionViewCell {
    
    
    @IBOutlet var topView: UIView!
    @IBOutlet var coverImageView: UIImageView!
    @IBOutlet var shadowView: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var milesLabel: UILabel!
    
    
}
