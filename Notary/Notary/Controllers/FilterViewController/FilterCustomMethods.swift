//
//  FilterCustomMethods.swift
//  DayRunner
//
//  Created by Rahul Sharma on 04/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

extension FilterViewController {
    /// to move back to last controller
    func backButton() {
        
        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionReveal,
                                                                  subType: kCATransitionFromBottom,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)

        
        self.navigationController?.popViewController(animated: false)
    }
}
