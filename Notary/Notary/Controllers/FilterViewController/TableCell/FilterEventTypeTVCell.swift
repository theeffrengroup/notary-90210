//
//  FilterEventTypeTVCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 04/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class FilterEventTypeTVCell: UITableViewCell {

    @IBOutlet weak var eventTypeCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        eventTypeCollectionView.delegate = self 
        eventTypeCollectionView.dataSource = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
