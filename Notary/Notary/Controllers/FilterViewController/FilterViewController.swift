//
//  FilterViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 04/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

enum cellType: Int {
    case gigTime = 0
    case eventType = 1
    case priceRange = 2
    case OperationRadius = 3
}

class FilterViewController: UIViewController {
    
    // MARK: - Outlets -
    // tableView
    @IBOutlet weak var mainTableView: UITableView!
    
    // Button

    @IBOutlet weak var applyButton: UIButton!
    
    // MARK: - Outlets -
    // Variable Decleration
    let catransitionAnimationClass = CatransitionAnimationClass()
    var selectedDirection: UITableView.Direction!
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        applyButton.transform = CGAffineTransform(translationX: 0, y: applyButton.frame.origin.y + 80 )
        selectedDirection = UITableView.Direction.top(useCellsFrame: true)
        self.mainTableView.reloadData(with: .simple(duration: 1.5, direction: self.selectedDirection, constantDelay: 0), reversed: false)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.8, delay: 0.3, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: [], animations: {
            self.applyButton.transform = .identity
        }) { (success) in
            
        }
    }
   
    // MARK: - Action Methods -
    @IBAction func applyAction(_ sender: UIButton) {
    }

    @IBAction func backAction(_ sender: Any) {
        backButton()
    }
    
    @IBAction func resetAction(_ sender: Any) {
    }
    
}
