//
//  MusicGenreVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension MusicGenreViewController {
    
    func makeApiCall() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        apiTag = RequestType.musicGenres.rawValue
        
        musicGenreViewModel.getMusicGenreAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.musicGenres)
        }

    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .musicGenres:
                            
                            progressMessage = PROGRESS_MESSAGE.Loading
                        
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                    
                }
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }

                
                break
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType
                {
                    case RequestType.musicGenres:
                        
                        genresData = []
                        
                        if let dataRes = dataResponse as? [Any] {
                            
                            for eachGenresData in dataRes {
                                
                                let genresDataModel = MusicGenreModel.init(musicGenreDetails: eachGenresData)
                                
                                genresData.append(genresDataModel)
                                
                            }
                            
                        }
                        
                        for eachPrevGenresData in prevSelectedMusicGenres {
                            
                            if let index = genresData.index(where: {$0.musicGenreId == eachPrevGenresData.musicGenreId}) {
                                
                                DDLogDebug("\(index)")
                                selectedMusicGenres.append(genresData[index])
                                
                            }
                            
                        }
                        
                        self.tableView.reloadData()
                        
                        break
                        
                    default:
                        
                        break
                }
                
            default:
                
                if errorMessage != nil{
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                    
                }
                
                break
        }
        
    }
    
}
