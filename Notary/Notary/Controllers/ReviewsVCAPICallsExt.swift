//
//  ReviewsVCAPICallsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import ESPullToRefresh

extension ReviewsViewController {
    
    //MARK - WebService Call -
    func getReviewsAndRating() {
        
        if pageNo == 0 {
            
            self.tableView.isHidden = true
        }
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            if pageNo > 0 {
                
                self.tableView.es.stopLoadingMore()
            }
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        reviewsViewModel.methodName = API.METHOD.REVIEWANDRATING + "/\(pageNo)"
        reviewsViewModel.pageNo = pageNo
        
        reviewsViewModel.getReviewsListAPICall { (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.GetReviews)
        }
        
    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                
                    self.apiTag = requestType.rawValue
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .GetReviews:
                        
                            progressMessage = PROGRESS_MESSAGE.Loading
                        
                        default:
                        
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                    
                }
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                break
                
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType
                {
                    case RequestType.GetReviews:
                        
                        if let dataRes = dataResponse as? [String : Any] {
                            
                            
                            for eachReview in (dataRes[SERVICE_RESPONSE.reviews] as! [Any]) {
                                
                                let reviewModel = ReviewsModel.init(reviewDetails: eachReview)
                                reviewsArray.append(reviewModel)
                            }
                            
                            if pageNo == 0 {
                                
                                if let reviewCount = dataRes[SERVICE_RESPONSE.reviewCount] as? Int {
                                    
                                    if reviewCount > 1 {
                                        
                                        reviewsLabel.text = "\(reviewCount) Reviews"
                                    } else {
                                        
                                        reviewsLabel.text = "\(reviewCount) Review"
                                    }
                                }
                                
                                averageStarLabel.text = "\(GenericUtility.strForObj(object: dataRes[SERVICE_RESPONSE.averageRating]))"
                                
                                ratingView.rating = Float(averageStarLabel.text!)!
                                
                                self.tableView.isHidden = false
                                
                            } else {
                                
                                self.tableView.es.stopLoadingMore()
                                
                            }
                            
                            self.tableView.reloadData()
                            
                        }
                        
                        
                    default:
                        break
                }
                
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                    
                }
                
                if pageNo > 0 {
                    
                    self.tableView.es.stopLoadingMore()
                }
                
                break
        }
        
    }
    
    
}

