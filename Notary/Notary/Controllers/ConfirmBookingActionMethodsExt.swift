//
//  ConfirmBookingActionMethodsExt.swift
//  Notary
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension ConfirmBookingScreen {

    //MARK: - UIButton Actions -
    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {

        self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func bookButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        bookNowValidation()
    }
    
    @IBAction func changeAddressButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let addressVC:YourAddressViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.yourAddressVC) as! YourAddressViewController
        
        addressVC.isFromConfirmBookingVC = true
        
        self.navigationController!.pushViewController(addressVC, animated: true)
        
    }
    
    @IBAction func addPaymentButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let paymentVC:PaymentViewController = self.storyboard!.instantiateViewController(withIdentifier: VCIdentifier.paymentVC) as! PaymentViewController
        
        paymentVC.isFromConfirmBookingVC = true
        paymentVC.CBModel = CBModel
        
        self.navigationController!.pushViewController(paymentVC, animated: true)
        
    }
    
    @IBAction func applyPromocodeButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if (promocodeCell.applyButton.titleLabel?.text)! == "Clear" {
            
            self.clearPromoCodeDetails()
            self.tableView.reloadData()
            
        } else {
            
            if (promocodeCell.promocodeTextField.text?.length)! > 0 && isPromoCodeValid == false {
                
                self.validatePromocodeAPI()
            }
        }
        
    }
    
    @IBAction func paymentSelectedButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let button = sender as? UIButton
        
        switch (button?.tag)! {
            
        case 1:
            
            if CBModel.paymentmethodTag != 0 {
                
                self.clearPromoCodeDetails()
            }
            
            CBModel.paymentmethodTag = 0
            //                tableView.reloadSections(IndexSet(integer: 5), with: .none)
            self.tableView.reloadData()
            
            break
            
            
        case 2:
            
            if CBModel.paymentmethodTag != 1 {
                
                self.clearPromoCodeDetails()
            }
            CBModel.paymentmethodTag = 1
            //                tableView.reloadSections(IndexSet(integer: 5), with: .none)
            self.tableView.reloadData()
            
            break
            
        default:
            break
        }
        
        
    }
    
    @objc func bookNowButtonAction(bookNowButton:UIButton) {
        
        selectedDate = scheduleDate
        CBModel.appointmentLocationModel.bookingType = BookingType.Default
        CBModel.isScheduleBookingAvailable = false
        
        self.showBookButton()

        self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
    }
    
    @objc func bookLaterButtonAction(bookLaterButton:UIButton) {
        
        scheduleDate = TimeFormats.roundDate(Date.dateWithDifferentTimeInterval().addingTimeInterval(3600 * 2))
        datePickerView.minimumDate = scheduleDate
        CBModel.appointmentLocationModel.scheduleDate = scheduleDate
        selectedDate = CBModel.appointmentLocationModel.scheduleDate
        datePickerView.date = selectedDate
        CBModel.appointmentLocationModel.bookingType = BookingType.Schedule
        
        scheduleBookingAPI()

        self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
    }
    
    @objc func calendarButtonAction(calendarButton:UIButton) {
        
        datePickerView.date = selectedDate
        self.dataPickerBackView.isHidden = false
        
        self.datePickerViewBottomConstraint.constant = 0
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
            
        })
        
        
    }
    
    @IBAction func datePickerCancelButtonAction(_ sender: Any) {
        
        self.datePickerViewBottomConstraint.constant = -600
        self.dataPickerBackView.isHidden = true
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
            
        })
    }
    
    
    @IBAction func datePickerSaveButtonAction(_ sender: Any) {
        
        if selectedDate != datePickerView.date   {
            
            selectedDate = datePickerView.date
            
            CBModel.appointmentLocationModel.bookingType = BookingType.Schedule
            CBModel.appointmentLocationModel.scheduleDate = selectedDate
            
            self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
            
            //Call schedule booking service API
            scheduleBookingAPI()
        }
        
        
        self.datePickerViewBottomConstraint.constant = -600
        self.dataPickerBackView.isHidden = true
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
            
        })
    }
    
    
    @IBAction func lastDueConfirmButtonAction(_ sender: Any) {
        
        hideLastDueView()
        liveBookingAPI()
    }
    
    @IBAction func lastDueCancelButtonAction(_ sender: Any) {
        
        hideLastDueView()
    }
    
    func showLastDueView() {
        
        self.lastDueBackViewBottomConstraint.constant = 0
        self.dataPickerBackView.isHidden = false
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
            
        })
    }
    
    func hideLastDueView() {
        
        self.lastDueBackViewBottomConstraint.constant = -500
        self.dataPickerBackView.isHidden = true
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
            
        })
    }
    
}
