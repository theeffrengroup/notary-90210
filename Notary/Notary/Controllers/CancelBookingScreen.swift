//
//  CancelBookingScreen.swift
//  LiveM
//
//  Created by Rahul Sharma on 04/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


class CancelBookingScreen:UIView {
    
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var closeButton: UIButton!
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var submitButton: UIButtonCustom!
    
    
    var bookingID:Int64 = 0
    var selectedReasonRowTag = -1
    var arrayOfCancelReasons:[CancelReasonModel] = []
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    let cancelBookingViewModel = CancelBookingViewModel()
    var newCancelFeeAlertWindow:UIWindow!
    var alertController:UIAlertController!
    
    //MARK: - Initial Methods -
    
    static var share: CancelBookingScreen? = nil
    
    static var sharedInstance: CancelBookingScreen {
        
        if share == nil {
            
            share = Bundle(for: self).loadNibNamed("CancellationBookingScreen",
                                                   owner: nil,
                                                   options: nil)?.first as? CancelBookingScreen
            
            share?.frame = (WINDOW_DELEGATE??.frame)!
           
            share?.tableView.register(UINib(nibName: "CancelBookingTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cancelBookingCell")
            
        }
        
        share?.acessClass.acessDelegate = share!
        
        return share!
    }
    
    func showCancelBookingPopUp() {
        
//        WINDOW_DELEGATE??.addSubview(self)
        
        self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        
        self.isHidden = false

        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                        
        }) { (finished) in
            
        }
    }

     
    
    
    //MARK: - UIButton Actions -
    
    @IBAction func closeButtonAction(_ sender: Any) {
        
        topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        
        UIView.animate(withDuration: 0.6,
                       delay: 0.2,
                       options: UIView.AnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                        
        }) { (finished) in
            
            CancelBookingScreen.share?.acessClass.acessDelegate = nil
            
            self.removeFromSuperview()
            CancelBookingScreen.share = nil
            
        }
        
    }
    
    
    /// Button Action to cancel Booking
    ///
    /// - Parameter sender: submit button object
    @IBAction func submitButtonAction(_ sender: Any) {
        
        if selectedReasonRowTag >= 0 {
            
            cancelBookingAPI()
            
        } else {
            
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.CANCEL_BOOKING.CancelReasonMissing)
        }
        
    }
    
    func showCancellationFeeAlert(message:String) {
        
        // create the alert
        if alertController == nil {
        
            alertController = UIAlertController(title: ALERTS.Message, message: message, preferredStyle: UIAlertController.Style.alert)
        }
        
        if newCancelFeeAlertWindow != nil {
            
            newCancelFeeAlertWindow?.resignKey()
            newCancelFeeAlertWindow?.removeFromSuperview()
            newCancelFeeAlertWindow = nil
        }
        
        newCancelFeeAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newCancelFeeAlertWindow?.rootViewController = UIViewController()
        newCancelFeeAlertWindow?.windowLevel = UIWindowLevelAlert + 1
        newCancelFeeAlertWindow?.makeKeyAndVisible()
        
        let DestructiveAction = UIAlertAction(title: ALERTS.NO, style: UIAlertAction.Style.destructive) {
            (result : UIAlertAction) -> Void in
            
            self.newCancelFeeAlertWindow.resignKey()
            self.newCancelFeeAlertWindow.removeFromSuperview()
            self.newCancelFeeAlertWindow = nil
            
            DDLogDebug("Destructive")
            
            CancelBookingScreen.share?.acessClass.acessDelegate = nil
            
            self.alertController = nil
            self.removeFromSuperview()
            CancelBookingScreen.share = nil
            
        }
        
        // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
        
        let okAction = UIAlertAction(title: ALERTS.YES, style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            
            self.newCancelFeeAlertWindow.resignKey()
            self.newCancelFeeAlertWindow.removeFromSuperview()
            self.newCancelFeeAlertWindow = nil

            self.alertController = nil

            self.showCancelBookingPopUp()
           
        }
        
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
        
        newCancelFeeAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
}


extension CancelBookingScreen:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.getCancelReasons.rawValue:
            
            getCancelReasonsAPI()
            
            break
            
        case RequestType.cancelBooking.rawValue:
            
            cancelBookingAPI()
            break
            
            
        default:
            break
        }
    }
    
}

