//
//  HomeCustomMethods.swift
//  DayRunner
//
//  Created by Rahul Sharma on 02/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

extension HomeScreenViewController{
    
    /// initiall Setup for BottomCollection View
    ///
    /// - Parameter view: view over which the bottom appears
    func initiallSetup(_ view: UIView){
        let min = Date()
        let max = Date().addingTimeInterval(60 * 60 * 24 * 12)
        let picker = CollectionButtomView.show(minimumDate: min, maximumDate: max, view: view)
        picker.highlightColor = UIColor.orange
        picker.isDatePickerOnly = true
    }
    
    func datepickerSetUp(){
        let min = Date()
        let max = Date().addingTimeInterval(60 * 60 * 24 * 30)
        let picker = DateTimePicker.show(minimumDate: min, maximumDate: max)
        picker.highlightColor = #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1)
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "DONE"
        picker.todayButtonTitle = "Today"
        picker.is12HourFormat = true
        picker.dateFormat = "hh:mm aa dd/MM/YYYY"
        //        picker.isDatePickerOnly = true
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm aa dd/MM/YYYY"
        }
    }
    
    /// Catransition Animation To View Controller
    ///kCATransitionFromTop
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier)
        
        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
                                                                  subType: kCATransitionFromTop,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.35)
        
        self.navigationController?.pushViewController(dstVC, animated: false)
    }
    
    /// Flip Animation For View Controller
    ///
    /// - Parameter idntifier: Identifier
    func flipAnimation(_ idntifier: String){
        let search = self.storyboard!.instantiateViewController(withIdentifier: idntifier)
        UIView.beginAnimations("animation", context: nil)
        UIView.setAnimationDuration(1.0)
        self.navigationController!.pushViewController(search, animated: false)
        UIView.setAnimationTransition(UIViewAnimationTransition.flipFromLeft, for: self.navigationController!.view, cache: false)
        UIView.commitAnimations()
    }
    
    
    
}
