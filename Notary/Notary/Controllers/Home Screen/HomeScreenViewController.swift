//
//  HomeScreenViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 31/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class HomeScreenViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var profileImageButton: UIButton!
    
    @IBOutlet var profileImageView: UIImageView!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var topAddressBackView: UIViewCustom!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet var showListScreenButtonBackView: UIViewCustom!
    
//    @IBOutlet var collectionView: iCarousel!
    
    @IBOutlet weak var locationButtonOutlet: UIButton!
    @IBOutlet weak var listButtonOutlet: UIButton!
    
    @IBOutlet weak var mainButtonHeight: NSLayoutConstraint!
    
//    @IBOutlet var collectionBackViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var topAddressBackViewTopConstraint: NSLayoutConstraint!
    
//    @IBOutlet var providerListCollectionViewBottomConstraint: NSLayoutConstraint!
    
    
    //Schedule DatePicker
    
    @IBOutlet var datePickerBackgroundView: UIView!
    
    @IBOutlet var eventStartBackView: UIView!
    
    @IBOutlet var datepickerCancelButton: UIButtonCustom!
    
    @IBOutlet var datepickerSaveButton: UIButtonCustom!
    
    @IBOutlet var datePickerBackBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var eventStartCollectionView: UICollectionView!
    
    @IBOutlet var datePickerView: UIDatePicker!
    
    @IBOutlet weak var datePickerFullBackView: UIView!
    
    
    //Later booking View
    
    @IBOutlet var selectedBookLaterBackView: UIView!
    
    @IBOutlet var selectedBookLaterDateLabel: UILabel!
    
    @IBOutlet var selectedBookLaterButton: UIButton!
    
    @IBOutlet var selectedBookLaterCloseButton: UIButton!
    
    
    @IBOutlet weak var noMusiciansMessageBackView: UIView!
    
    @IBOutlet weak var noMusiciansMessageLabel: UILabel!
    
    @IBOutlet weak var bookButtonBackView: UIView!
    
    @IBOutlet weak var bookButton: UIButtonCustom!
    
    
    @IBOutlet weak var etaBackView: UIView!
    
    @IBOutlet weak var etaLabel: UILabel!
    
    @IBOutlet weak var etaActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var etaBackViewAlignCenterYContraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var bookButtonHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var showListBackViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var currentLocationButtonBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var mapViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var calendarButtonWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topAddressDivider: UIView!
    
    
    
    
    let catransitionAnimationClass = CatransitionAnimationClass()
    let locationObj = LocationManager.sharedInstance()
    
    var delegate: LeftMenuProtocol?
    
    
    var isAddressManuallyPicked = false
    
    var isFocusedMarkerPicked = false
    
    var dictOfMarkers:[String:Any] = [:]
    
    var arrayOfProvidersModel:[MusicianDetailsModel] = []
    var arrayOfMusicians:[Any] = []
    var selectedProviderTag = 0

    
    let mqttModel = MQTT.sharedInstance()
    let musiciansListManager = MusiciansListManager.sharedInstance()
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var apiTag:Int!
    
    var selectedView:ProviderCollectionView!
    
    let appointmentLocationModel = AppoimtmentLocationModel.sharedInstance
    
    var currentLat = 0.0
    var currentLong = 0.0
    
    var selectedEventStartTag = 0
    
    var scheduleDate:Date!
    var selectedDate:Date!
//    var noArtistMessageToShow = true
    var noAeristAlertController:UIAlertController!
    var needToShowProgress = false
    
    var isVCisDispappeared = false
    
    var currentShortestDistance = 0.0
    let distanceAndETAManager = DistanceAndETAManager.sharedInstance

    
    // Shared instance object for gettting the singleton object
    static var obj:HomeScreenViewController? = nil
    
    class func sharedInstance() -> HomeScreenViewController {
        
        return obj!
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.showStatusBarBackView()
        
        HomeScreenViewController.obj = self
        
//        if Helper.SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(version: "11.0") {

            self.mapViewTopConstraint.constant = -NavigationBarHeight
            mapView.layoutIfNeeded()
//        }
        
        if SCREEN_HEIGHT == 812 { //iPhoneX
            
            self.etaBackViewAlignCenterYContraint.constant = 24
        }
        
        setInitialMapViewProperties()
        
        Helper.editNavigationBar(navigationController!)
        
        initializeMQTTMethods(initial: true)
        
        showPrevProviderDetails()
        
        self.selectedBookLaterBackView.isHidden = true
        
        if scheduleDate == nil {
            
            scheduleDate = TimeFormats.roundDate(Date.dateWithDifferentTimeInterval().addingTimeInterval(3600 * 2))
            datePickerView.minimumDate = scheduleDate
        }
        
        selectedDate = scheduleDate
        datePickerView.date = selectedDate
                
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            
            MusiciansListManager.sharedInstance().getCategoriesServiceAPI(currentLat: LocationManager.sharedInstance().latitute, currentLong: LocationManager.sharedInstance().longitude)
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
            
            PendingReviewsManager.sharedInstance.getPendingReviews()
        })

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        showBookLaterDetails()
        checkLocationIsChanged()

        acessClass.acessDelegate = self
        locationObj.delegate = self
        
        distanceAndETAManager.delegate = self
        
        self.setTimeZonesToCalendar()
        
        hideNavigation()
        initializeMQTTMethods(initial: false)
        
        isVCisDispappeared = false
        
        getUpdatedMusiciansListFromMQTTManager()
        
        topAddressBackViewTopConstraint.constant = 20 + NavigationBarHeight
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        showProfileImageView()
        hideNavigation()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.frame = SCREEN_RECT
    }

    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    
    func hideNavigation() {
        
        Helper.statusBarView.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view?.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        uninitializeMQTTMethods()
        
        locationObj.delegate = nil
        acessClass.acessDelegate = nil
        distanceAndETAManager.delegate = nil

        
        isVCisDispappeared = true
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.view?.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        
        self.datePickerCancelButtonAction(self.datepickerCancelButton)
    }
    
}

extension HomeScreenViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
            case RequestType.getAllMusicians.rawValue:
                
                initializeMQTTMethods(initial: false)
                
                break
            
            default:
                break
        }
    }
    
}

extension HomeScreenViewController {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let gigTimeCell:MDGigTimeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "gigTimeCell", for: indexPath) as! MDGigTimeCollectionViewCell
        
        gigTimeCell.timeLabel.text = "\(30*(indexPath.row+1)) MIN."
        
        
        if selectedEventStartTag == indexPath.row {
            
            gigTimeCell.timeLabel.textColor = UIColor.red
            gigTimeCell.selectedDivider.isHidden = false
            
            gigTimeCell.timeLabel.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            
            UIView.animate(withDuration: 0.8,
                           delay: 0.2,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 5,
                           options: [],
                           animations: {
                            
                            gigTimeCell.timeLabel.transform = .identity
                            
            }) { (success) in
                
                
            }
            
            
        } else {
            
            gigTimeCell.timeLabel.textColor = UIColor.black
            gigTimeCell.selectedDivider.isHidden = true
            
        }
        
        return gigTimeCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        let sizeOfEachCell:CGSize = CGSize.init(width: (collectionView.bounds.size.width - 30)/3.5, height: 35)
        
        let sizeOfEachCell:CGSize = CGSize.init(width: collectionView.bounds.size.width/4, height: 35)
        
        return sizeOfEachCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedEventStartTag = indexPath.row
        appointmentLocationModel.selectedEventStartTag = selectedEventStartTag
        self.eventStartCollectionView.reloadData()
        
    }

    
}


