//
//  CBBookNowOrLaterTableViewCell.swift
//  Notary
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class CBBookNowOrLaterTableViewCell:UITableViewCell {
    
    @IBOutlet var bookNowView: UIView!
    
    @IBOutlet var bookLaterView: UIView!
    
    @IBOutlet var bookLaterDateView: UIView!
    
    @IBOutlet weak var bookNowSelectedImageView: UIImageView!
    
    @IBOutlet weak var bookNowTitleLabel: UILabel!
    
    @IBOutlet weak var bookNowDescriptionLabel: UILabel!
    
    @IBOutlet weak var bookNowButton: UIButton!
    
    
    @IBOutlet weak var bookLaterSelectedImageView: UIImageView!

    @IBOutlet weak var bookLaterTitleLabel: UILabel!
    
    @IBOutlet weak var bookLaterDescriptionLabel: UILabel!
    
    @IBOutlet weak var bookLaterButton: UIButton!
    
    @IBOutlet weak var selectedDateLabel: UILabel!
    
    @IBOutlet weak var calendarButton: UIButton!
    

    func showBookNowOrLaterDetails(CBModel:ConfirmBookingModel) {
        
        if CBModel.appointmentLocationModel.bookingType == .Schedule {
            
            bookNowTitleLabel.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
            bookNowDescriptionLabel.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)

            bookLaterTitleLabel.textColor = #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)
            bookLaterDescriptionLabel.textColor = #colorLiteral(red: 0.3137254902, green: 0.3137254902, blue: 0.3137254902, alpha: 1)
            
            self.bookLaterDateView.isHidden = false
            
            self.bookNowButton.isUserInteractionEnabled = true
            self.bookLaterButton.isUserInteractionEnabled = false

            self.bookNowSelectedImageView.image = #imageLiteral(resourceName: "check_off")
            self.bookLaterSelectedImageView.image = #imageLiteral(resourceName: "Ellipse3")

            
            if CBModel.appointmentLocationModel.scheduleDate != nil {
                
                let dateformat = DateFormatter.initTimeZoneDateFormat()
                dateformat.dateFormat = "EEEE dd MMM yyyy hh:mm a"
                
                dateformat.timeZone = Helper.getTimeZoneFromLoaction(latitude: CBModel.appointmentLocationModel.pickupLatitude,
                                                                              longitude: CBModel.appointmentLocationModel.pickupLongitude)
                
                self.selectedDateLabel.text = dateformat.string(from:CBModel.appointmentLocationModel.scheduleDate)
            }
            
        } else {
            
            bookNowTitleLabel.textColor = #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)
            bookNowDescriptionLabel.textColor = #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)
            
            bookLaterTitleLabel.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
            bookLaterDescriptionLabel.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
            
            self.bookLaterDateView.isHidden = true
            
            self.bookNowButton.isUserInteractionEnabled = false
            self.bookLaterButton.isUserInteractionEnabled = true
            
            self.bookNowSelectedImageView.image = #imageLiteral(resourceName: "Ellipse3")
            self.bookLaterSelectedImageView.image = #imageLiteral(resourceName: "check_off")

        }
    }
}
