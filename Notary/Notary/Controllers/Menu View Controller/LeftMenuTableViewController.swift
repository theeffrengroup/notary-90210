//
//  LeftMenuTableViewController.swift
//  Iserve
//
//  Created by Rahul Sharma on 12/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

enum LeftMenu: Int {
    case searchArtist = 0
    case myEvent
    case payment
//    case wallet
//    case reviews
    case address
    case support
    case share
    case helpCenter
//    case liveChat
    case notaryAbout
}

protocol LeftMenuProtocol: class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftMenuTableViewController: UITableViewController {
    
    @IBOutlet weak var profileImage: UIImageView!

    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var actiVityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ProfileButtonOutlet: UIButton!
    
    var profilViewController: ProfileViewController!
    var homeScreenViewController: UIViewController!
    var paymentViewController: PaymentViewController!
    var reviewsViewController: ReviewsViewController!
    var yourAddressViewController: YourAddressViewController!
    var supportListViewController: SupportListViewController!
    var shareViewController: ShareViewController!
    var helpCenterViewController: TicketsViewController!
    var liveMViewController: LiveMViewController!
    var myEventViewController: MyEventViewController!
    var liveChatViewController: LiveChatViewController!
    var walletViewController: WalletViewController!
    
    let menuModal = LeftMenuViewModel()
    
    var storybrd:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    
    static var obj:LeftMenuTableViewController? = nil
    
    class func sharedInstance() -> LeftMenuTableViewController {
        
        return obj!
    }

    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        LeftMenuTableViewController.obj = self
        
        
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        self.setUpViewControllers()
        self.tableView.registerCellClass(BaseTableViewCell.self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        showUserDetails()
    }
    
    
    
    /// Method to show current user details
    func showUserDetails () {
        
        userNameLabel.text = "\(Utility.firstName) \(Utility.lastName)"
        
        self.profileImage.layer.borderWidth = 2
        self.profileImage.layer.borderColor = APP_COLOR.cgColor
        
        if !Utility.profilePic.isEmpty {
            
            actiVityIndicator.startAnimating()
            
            profileImage.kf.setImage(with: URL(string: Utility.profilePic),
                                     placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                     options: [.transition(ImageTransition.fade(1))],
                                     progressBlock: { receivedSize, totalSize in
            },
                                     completionHandler: { image, error, cacheType, imageURL in
                                        
                                        self.actiVityIndicator.stopAnimating()
            })
            
        } else {
            
            profileImage.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }

    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .searchArtist, .myEvent, .payment, .address, .support, .share, .helpCenter, .notaryAbout: //.liveChat
                return BaseTableViewCell.height()
            }
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuModal.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
                case .searchArtist, .myEvent, .payment, .address, .support, .share, .helpCenter, .notaryAbout://, .liveChat
                    let cell:LeftMenuTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "LeftMenu") as! LeftMenuTableViewCell
                    cell.setData(menuModal.menus[indexPath.row], image: menuModal.menusImages[indexPath.row])
                    return cell
            }
        }
        return UITableViewCell()
    }
    
    
    /// Custom Method to load all Left menu controllers
    func setUpViewControllers() -> Void {
        
        self.profilViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.profileVC) as? ProfileViewController
        self.profilViewController.delegate = self
        
        self.shareViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.shareVC) as? ShareViewController
        self.shareViewController.delegate = self
        
        self.paymentViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.paymentVC) as? PaymentViewController
        self.paymentViewController.delegate = self
        
        self.yourAddressViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.yourAddressVC) as? YourAddressViewController
        self.yourAddressViewController.delegate = self
        
        self.reviewsViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.reviewsVC) as? ReviewsViewController
        self.reviewsViewController.delegate = self
        
        self.supportListViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.supportVC) as? SupportListViewController
        self.supportListViewController.delegate = self
        
        self.helpCenterViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.zenDeskVC) as? TicketsViewController
        self.helpCenterViewController.delegate = self
        
        self.liveMViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.liveMVC) as? LiveMViewController
        self.liveMViewController.delegate = self
        
        
        self.myEventViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.myEventVC) as? MyEventViewController
        self.myEventViewController.delegate = self
        
        self.liveChatViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.liveChatVC) as? LiveChatViewController
        self.liveChatViewController.delegate = self
        
        self.walletViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.walletVC) as? WalletViewController
        self.walletViewController.delegate = self
    
    }
    
    
    /// Show User Profile details button action
    ///
    /// - Parameter sender: Show User Profile details button object
    @IBAction func ProfileButtonAction(_ sender: Any) {
        
        self.profilViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.profileVC) as? ProfileViewController
        self.profilViewController.delegate = self
        
        Helper.getCurrentVC().navigationController?.pushViewController(self.profilViewController, animated: false)
        self.slideMenuController()?.closeLeft()
        self.slideMenuController()?.closeRight()

    }
}

// MARK: - LeftMenuProtocol
extension LeftMenuTableViewController : LeftMenuProtocol
{
    
    /// Method to maintain each left menu button action
    ///
    /// - Parameter menu: left menu class object
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {
            
            case .searchArtist:
                
                break
                
            case .myEvent:
                
//                self.myEventViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.myEventVC) as! MyEventViewController
//                self.myEventViewController.delegate = self
                
                Helper.getCurrentVC().navigationController?.pushViewController(self.myEventViewController, animated: false)
                
            case .payment:
                
                Helper.getCurrentVC().navigationController?.pushViewController(self.paymentViewController, animated: false)
                
//            case .reviews:
//
//                self.reviewsViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.reviewsVC) as! ReviewsViewController
//                self.reviewsViewController.delegate = self
//
//                Helper.getCurrentVC().navigationController?.pushViewController(self.reviewsViewController, animated: false)
            
            
            /*case .wallet:
                
                self.walletViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.walletVC) as! WalletViewController
                self.walletViewController.delegate = self
                
                Helper.getCurrentVC().navigationController?.pushViewController(self.walletViewController, animated: false)*/
            
            case .address:
           
                Helper.getCurrentVC().navigationController?.pushViewController(self.yourAddressViewController, animated: false)
            
            case .support:

                self.supportListViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.supportVC) as? SupportListViewController
                self.supportListViewController.delegate = self
                
                Helper.getCurrentVC().navigationController?.pushViewController(self.supportListViewController, animated: false)
                
            case .share:
                
                self.shareViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.shareVC) as? ShareViewController
                self.shareViewController.delegate = self
                Helper.getCurrentVC().navigationController?.pushViewController(self.shareViewController, animated: false)
                
            case .helpCenter:
                
                Helper.getCurrentVC().navigationController?.pushViewController(self.helpCenterViewController, animated: false)
            
           /* case .liveChat:
                
                self.liveChatViewController = storybrd.instantiateViewController(withIdentifier: VCIdentifier.liveChatVC) as! LiveChatViewController
                self.liveChatViewController.delegate = self
                
                Helper.getCurrentVC().navigationController?.pushViewController(self.liveChatViewController, animated: false)
                break*/
                
            case .notaryAbout:
                
                Helper.getCurrentVC().navigationController?.pushViewController(self.liveMViewController, animated: false)
                break
        }
        
        self.slideMenuController()?.closeLeft()
        self.slideMenuController()?.closeRight()
    }
}
