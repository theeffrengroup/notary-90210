//
//  ChatVCMQTTDelegateExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 12/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension ChatViewController:MQTTSimpleSingleChatManagerDelegate {
    
    func chatMessageRecievedFromMQTT(chatMessageDetails: ChatMessageRecievedModel) {
        
        let msg = chatMessageDetails.messageContent
        let timeStamp = chatMessageDetails.messageTimestamp
        
        switch chatMessageDetails.messageType {
            
        case .photo:
            
            let message = Message.init(type: .photo , content: msg, owner: .receiver, timestamp: Int64(timeStamp) + 1, isRead: false)
            items.append(message)
            
        case .location:
            
            break
            
        default:
            
            let message = Message.init(type: .text , content: msg, owner: .receiver, timestamp: Int64(timeStamp) + 1, isRead: false)
            
            items.append(message)
            
            break
        }
        
        DispatchQueue.main.async {
            
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
            //            self.tableView.scrollRectToVisible(CGRect.init(x: self.tableView.contentSize.width - 1, y: self.tableView.contentSize.height - 1, width: 1, height: 1), animated: false)
        }
        
    }
    
}
