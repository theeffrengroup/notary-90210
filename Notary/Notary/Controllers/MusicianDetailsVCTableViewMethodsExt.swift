//
//  MusicianDetailsVCTableViewMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation


extension MusicianDetailsViewController:UITableViewDelegate,UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if musicianFullDetailsModel != nil {
            
            return musicianFullDetailsModel.arrayOfMetaData.count + 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
            case (musicianFullDetailsModel.arrayOfMetaData.count + 1) - 1://Review Section
                
                return musicianFullDetailsModel.reviewsArray.count
            
            default:
                
                return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        
        let label = UILabel.init(frame: CGRect.init(x: 15, y: 0, width: tableView.bounds.size.width-30, height: 30))
        label.font = UIFont.init(name: FONTS.TitilliumWebSemiBold, size: 15)
        
        if section == ((musicianFullDetailsModel.arrayOfMetaData.count + 1) - 1) {
            
            if musicianFullDetailsModel.reviewsArray.count == 0 {
                
                let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                return view
                
            } else {
                
                label.text = "Reviews (\(musicianFullDetailsModel.noOfReviews))"
            }
            
            
        } else {
            
            label.text = musicianFullDetailsModel.arrayOfMetaData[section].fieldTitle
        }
        
        label.textColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
        
        view.addSubview(label)
        
        return view
        
    }
    
    /*func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        var view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
        
        switch section {
            
            case ((musicianFullDetailsModel.arrayOfMetaData.count + 1) - 1):
                
                if musicianFullDetailsModel.reviewsArray.count == 0 {
                    
                    view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                    return view
                }
                return view
            
            default:
                return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        switch section {
            
        case 1:
            
            if musicianFullDetailsModel.reviewsArray.count == 0 {
                
                return 0.01
                
            } else {
                
                return 0.01
            }
            
        default:
            
            return 0.0
        }
        
    }*/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == ((musicianFullDetailsModel.arrayOfMetaData.count + 1) - 1) && musicianFullDetailsModel.reviewsArray.count == 0 {
            
            return 0.01
        }
        return 30.0

    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        
            case ((musicianFullDetailsModel.arrayOfMetaData.count + 1) - 1)://Reviews
                
                let reviewCell:MDReviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as! MDReviewTableViewCell
            
                
                reviewCell.noReviewsBackView.isHidden = true
                reviewCell.reviewView.isHidden = false
                
                reviewCell.showReviewDetails(reviewModel: ReviewsModel.init(musicianReviewDetails: musicianFullDetailsModel.reviewsArray[indexPath.row]))
                
                return reviewCell
            
            default://Meta Data Fields
                
                let normalCell:MDNormalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "normalCell", for: indexPath) as! MDNormalTableViewCell
                
                let metaDataModel = musicianFullDetailsModel.arrayOfMetaData[indexPath.section]
                
                normalCell.readMoreLabel.text = metaDataModel.fieldDescription
                
                normalCell.readMoreButton.tag = indexPath.section
                normalCell.readMoreLabel.numberOfLines = 0
                
                if metaDataModel.fieldType == 2 {
                    
                    normalCell.readMoreButton.isHidden = true
                    normalCell.readMoreButtonHeightConstraint.constant = 0
                    normalCell.readMoreButton.setTitle("", for: .normal)
                    
                } else {
                    
                    if labelNumberOflines(label: normalCell.readMoreLabel) > 3 {
                        
                        normalCell.readMoreButton.isHidden = false
                        normalCell.readMoreButtonHeightConstraint.constant = 20
                        
                        if states[indexPath.section] {
                            
                            normalCell.readMoreLabel.numberOfLines = 3
                            normalCell.readMoreButton.setTitle("Read more", for: .normal)
                            
                        } else {
                            
                            normalCell.readMoreLabel.numberOfLines = 0
                            normalCell.readMoreButton.setTitle("Read less", for: .normal)
                        }
                        
                        normalCell.readMoreButton.addTarget(self, action: #selector(readMoreButtonAction(button:)), for: .touchUpInside)
                        
                    } else {
                        
                        normalCell.readMoreButton.isHidden = true
                        normalCell.readMoreButtonHeightConstraint.constant = 0
                        normalCell.readMoreButton.setTitle("", for: .normal)
                    }
                    
                }
                
                return normalCell
            
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height:CGFloat
        switch indexPath.section {
            
            case ((musicianFullDetailsModel.arrayOfMetaData.count + 1) - 1):
                
                let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.size.width - 83, height: 0))
                
                label.font = UIFont.init(name: FONTS.TitilliumWebRegular, size: 13)
                
                let reviewModel = ReviewsModel.init(musicianReviewDetails: musicianFullDetailsModel.reviewsArray[indexPath.row])
                
                label.text = reviewModel.comment
                
                height = Helper.measureHeightLabel(label, width: SCREEN_WIDTH! - 83)
                
                return height + 75
            
            default:
                return UITableViewAutomaticDimension
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
//        cell.contentView.transform = CGAffineTransform(scaleX: 1.4, y:1.4)
//
//        UIView.animate(withDuration: 0.8,
//                       delay: 0,
//                       usingSpringWithDamping: 0.5,
//                       initialSpringVelocity: 3,
//                       options: [],
//                       animations: {
//
//                        cell.contentView.transform = .identity
//
//        }, completion: { (completed) in
//
//
//        })
//
        
        
    }
    
    func labelNumberOflines(label: UILabel) -> Int {
        let textSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: CGFloat(Float.infinity))
        let rHeight = lroundf(Float(label.sizeThatFits(textSize).height))
        let charSize = lroundf(Float(label.font.lineHeight))
        let lineCount = rHeight/charSize
        return lineCount
    }
    
    @objc func readMoreButtonAction(button:UIButton) {
        
        if states[button.tag] {
            
            states[button.tag] = false
            
        } else {
            
            states[button.tag] = true
        }
        
//        self.tableView.reloadSections(IndexSet(integer: button.tag), with: .none)
        
        self.tableView.reloadData()
        
        self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: button.tag), at: UITableView.ScrollPosition.middle, animated: true)
        
    }
    
}


