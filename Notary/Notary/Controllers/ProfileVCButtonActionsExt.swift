//
//  ProfileVCButtonActionsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ProfileViewController {
    
    // MARK: - Action methods -
    @IBAction func backAction(_ sender: Any) {
        
        //        delegate?.changeViewController(LeftMenu.searchArtist)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func imageAction(_ sender: Any) {
        musicGenre = true
        libraryEnabled = !libraryEnabled
        croppingEnabled = !croppingEnabled
        alertView()
    }
    
    @IBAction func musicGenreAction(_ sender: Any) {
        musicGenre = true
        catransitionAnimation(idntifier: VCIdentifier.musicGenre)
    }
    
    @IBAction func countryPickerAction(_ sender: Any) {
        catransitionAnimation(idntifier: VCIdentifier.countryNameVC)
    }
    
    
    
    @IBAction func changeEmailAction(_ sender: Any) {
        isEmail = true
        performSegue(withIdentifier: SEgueIdetifiers.profileChnageEM, sender: nil)
    }
    
    
    @IBAction func changeMobileAction(_ sender: Any) {
        isEmail = false
        performSegue(withIdentifier: SEgueIdetifiers.profileChnageEM, sender: nil)
    }
    
    
    
    @IBAction func editAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if firstNameBottomView.transform != .identity {
            
            editButtonAnimation()
            UIView.transition(with: editButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.editButton.setTitle(ALERTS.save, for: .normal)
            })
            
        }else {
            
            if nameTextField.text?.length == 0 {
                
                Helper.alertVC(title: ALERTS.Message, message: ALERTS.FirstNameMissing)
                
            } else if lastNameTextField.text?.length == 0 {
                
                Helper.alertVC(title: ALERTS.Message, message: ALERTS.LastNameMissing)
                
            } else {
                
                if  profileAlternatePhoto == 1 ||  profileAlternatePhoto == 2 {
                    
                    uploadProfileImageToAmazon()
                    
                }else {
                    
                    self.updateUserDefault()
                    self.serviceRequestToUpdateProfile()
                }
                
                
                UIView.transition(with: editButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.editButton.setTitle(ALERTS.edit, for: .normal)
                })
                
                initiallSetupForAnimation()
                animations()
                
//                if (phoneNumBottomView.frame.size.height + phoneNumBottomView.frame.origin.y) + 25 > scrollView.frame.size.height {
//
//                    scrollContentViewHeightConstraint.constant = (phoneNumBottomView.frame.size.height + phoneNumBottomView.frame.origin.y) + 25 - scrollView.frame.size.height;
//                }
//                else
//                {
//                    self.scrollContentViewHeightConstraint.constant = 0
//                }
//
//                scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
//                scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
//
//                self.view.layoutIfNeeded()
                
            }
            
        }
    }
    
    @IBAction func changePasswordAction(_ sender: Any) {
        self.performSegue(withIdentifier: SEgueIdetifiers.profileToChangePass, sender: nil)
    }
    
    @IBAction func logOutAction(_ sender: Any) {
        // create the alert
        
        let alertController = UIAlertController(title: ALERTS.Message, message: ALERTS.LogOut, preferredStyle: UIAlertController.Style.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
        
        let newAlertWindow = UIWindow(frame: UIScreen.main.bounds)
        newAlertWindow.rootViewController = UIViewController()
        newAlertWindow.windowLevel = UIWindowLevelAlert + 1
        newAlertWindow.makeKeyAndVisible()
        
        let DestructiveAction = UIAlertAction(title: ALERTS.NO, style: UIAlertAction.Style.destructive) {
            (result : UIAlertAction) -> Void in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            
            DDLogDebug("Destructive")
        }
        
        // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
        
        let okAction = UIAlertAction(title: ALERTS.YES, style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            
            newAlertWindow.resignKey()
            newAlertWindow.removeFromSuperview()
            
            self.logOut()
        }
        
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
   
        newAlertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func dobAction(_ sender: Any) {
        
        self.birthdayPicker.setDate(selectedDOB, animated: true)
        
        let dateFormat = DateFormatter.initTimeZoneDateFormat()
        dateFormat.dateFormat = "dd-MM-yyyy"
        pickerDateLabel.text = dateFormat.string(from: selectedDOB)
        
        dobPickerBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }

    
    
    @IBAction func cancelPickerAction(_ sender: Any) {
        
        dobPickerBottomConstraint.constant = -500
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func donePickerAction(_ sender: Any) {
        
        selectedDOB = birthdayPicker.date
        
        dobLabel.text = Helper.getDobStringDate(date: selectedDOB)
        
        dobPickerBottomConstraint.constant = -500
        
        UIView.animate(withDuration: 0.75, animations: {() -> Void in
            
            self.view.layoutIfNeeded()
        })
    }
    
    
}
