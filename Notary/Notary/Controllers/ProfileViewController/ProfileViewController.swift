//
//  ProfileViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 07/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class ProfileViewController: UIViewController, AccessTokeDelegate {
    
    // MARK: - Outlet -
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var scrollContentView: UIView!
    
    @IBOutlet weak var profilePhoto: UIImageView!
    
    @IBOutlet weak var countryImage: UIImageView!
    
    @IBOutlet weak var firstNameBottomView: UIView!
    
    @IBOutlet weak var lastNameBottomView: UIView!
    
    @IBOutlet weak var aboutMeBottomView: UIView!
    
    @IBOutlet weak var emailBottomView: UIView!
    
    @IBOutlet weak var phoneNumBottomView: UIView!
    
    @IBOutlet weak var dobBottomView: UIView!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var logOutButton: UIButton!
    
    @IBOutlet weak var changeButton: UIButtonCustom!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var imageButton: UIButton!
    
    @IBOutlet weak var dobButton: UIButton!
    
    @IBOutlet weak var imageButtonOutlet: UIButton!
    
    @IBOutlet weak var countryPickerButton: UIButton!
    
    
    @IBOutlet weak var photoActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var changePasswordBGViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var dobImageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var aboutMeCountHeight: NSLayoutConstraint!
    
    @IBOutlet weak var countryPickerViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imageButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var dobPickerBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
        
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var mobileNumberTextField: UITextField!
    
    @IBOutlet weak var aboutMeTextView: UITextView!
    
    
    @IBOutlet weak var aboutMeCountLabel: UILabel!
    
    @IBOutlet weak var dobLabel: UILabel!
    
    @IBOutlet weak var pickerDateLabel: UILabel!
    
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    
    @IBOutlet weak var birthdayPicker: UIDatePicker!
    
    
    @IBOutlet var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var topView: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var datePickerBackView: UIView!
    @IBOutlet weak var changeEmailButton: UIButton!
    @IBOutlet weak var changePhoneNumButton: UIButton!
    
    
    @IBOutlet var navigationTopView: UIView!
    
    @IBOutlet var navigationTitleView: UIView!
    
    @IBOutlet var navigationTitleLabel: UILabel!
    
    
    @IBOutlet weak var topViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var navigationTopViewHeightConstraint: NSLayoutConstraint!
    
    
    
    // MARK: - Variable Decleration -
    
    var rotationAngle: CGFloat!
    var refreshMonthCount : Int!
    var monthI: Int!
    var croppingEnabled: Bool = false
    var libraryEnabled: Bool = true
    let catransitionAnimationClass = CatransitionAnimationClass()
    var selectedCountry: Country!
    var activeField: UITextField?
    
    var countriesFiltered = [Country]()
    weak var delegate: LeftMenuProtocol?
    var profilePicURL: String!
    var musicGenreApi: String = ""
    var landingViewController: UIViewController!
    let acessClass = AccessTokenRefresh()
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var apiTag: Int!
    var profileAlternatePhoto = 0
    var profileResponseModel: ProfileResponseModel? = nil
    var isEmail: Bool = false
    var musicGenre: Bool = false
    var genresArray = [MusicGenreModel]()
    var selectedDOB:Date!
    
    let profileViewModel = ProfileViewModel()
    let disposeBag = DisposeBag()

    let imagePicker = UIImagePickerController()
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    
    
    // MARK: - Default Class Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profilePhoto.image = PROFILE_DEFAULT_IMAGE
        
        acessClass.acessDelegate = self
        
        birthdayPicker.timeZone = Helper.getTimeZoneFromCurrentLoaction()
        
        Helper.editNavigationBar(navigationController!)
        self.initialNavigationSetUp()
        initiallSetupForAnimation()
        animations()
        photoActivityIndicator.isHidden = true
        profilePhoto.clipsToBounds = true
        
        self.title = ""
        
        if !musicGenre {
            
            sendServiceToGetProfile()
            musicGenre = false
            
        }
        
        birthdayPicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        
        selectedDOB = birthdayPicker.date
        
        dateChanged(birthdayPicker)
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
                
        appDelegate?.keyboardDelegate = self
        
        appDelegate?.accessTokenDelegate = self
        
        /*self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view?.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear*/
        
        hideNavigation()
        
        USER_DEFAULTS.API_CALL.profileScreen = false
        
        addObserveToVariables()
        
        imagePicker.delegate = self
        
        setupGestureRecognizer()
        
        isEmail = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (phoneNumBottomView.frame.size.height + phoneNumBottomView.frame.origin.y) + 25 > scrollView.frame.size.height {
            
            scrollContentViewHeightConstraint.constant = (phoneNumBottomView.frame.size.height + phoneNumBottomView.frame.origin.y) + 25 - scrollView.frame.size.height;
        }
        else
        {
            self.scrollContentViewHeightConstraint.constant = 0
        }
        
        self.scrollView.contentSize = CGSize.init(width: self.scrollView.frame.size.width, height: (phoneNumBottomView.frame.size.height + phoneNumBottomView.frame.origin.y) + 25)
        
        self.scrollView.layoutIfNeeded()
    
        scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: false)
        
        self.scrollView.layoutIfNeeded()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        /*self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.view?.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()*/

        showNavigation()
        
        self.view.endEditing(true)
        
        appDelegate?.keyboardDelegate = nil
        appDelegate?.accessTokenDelegate = nil
        acessClass.acessDelegate = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { (context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            guard let vc = (self.slideMenuController()?.mainViewController as? UINavigationController)?.topViewController else {
                return
            }
            if vc.isKind(of: ProfileViewController.self)  {
                self.slideMenuController()?.removeLeftGestures()
                self.slideMenuController()?.removeRightGestures()
            }
        })
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        
        let dateFormat = DateFormatter.initTimeZoneDateFormat()
        dateFormat.dateFormat = "dd-MM-yyyy"
        pickerDateLabel.text = dateFormat.string(from: sender.date)
        
    }
    
        
    
    /// to recall the api
    @objc func recallApi() {
        switch apiTag {
        case RequestType.signOut.rawValue:
            logOut()
        case RequestType.getProfileData.rawValue:
            sendServiceToGetProfile()
        case RequestType.updateProfileData.rawValue:
            serviceRequestToUpdateProfile()
        default:
            break
        }
    }
    
    func addObserveToVariables() {
        
        nameTextField.rx.text
            .orEmpty
            .bind(to: profileViewModel.firstNameText)
            .disposed(by: disposeBag)
        
        lastNameTextField.rx.text
            .orEmpty
            .bind(to: profileViewModel.lastNameText)
            .disposed(by: disposeBag)
        
        emailTextField.rx.text
            .orEmpty
            .bind(to: profileViewModel.emailText)
            .disposed(by: disposeBag)
        
//        musicGenreTextField.rx.text
//            .orEmpty
//            .bind(to: profileViewModel.musicGenereText)
//            .disposed(by: disposeBag)
        
        mobileNumberTextField.rx.text
            .orEmpty
            .bind(to: profileViewModel.phoneNumberText)
            .disposed(by: disposeBag)
        
        aboutMeTextView.rx.text
            .orEmpty
            .bind(to: profileViewModel.aboutMeText)
            .disposed(by: disposeBag)
    
    }

    
}



extension ProfileViewController:KeyboardDelegate {
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        let inputViewFrame: CGRect? = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect
        let inputViewFrameInView: CGRect = view.convert(inputViewFrame!, from: nil)
        let intersection: CGRect = scrollView.frame.intersection(inputViewFrameInView)
        let ei: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: intersection.size.height, right: 0.0)
        scrollView.scrollIndicatorInsets = ei
        scrollView.contentInset = ei
        
    }
    
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
        }
        
    }
    
}

// MARK: - CountrySelectedDelegate -
extension ProfileViewController: CountrySelectedDelegate {
    
    /// getting data from CountryNameViewController
    ///
    /// - Parameter country: country description
    func countryNameSelected(countrySelected country: Country) {
        self.selectedCountry = country
        DDLogVerbose("country selected  code \(selectedCountry.country_code), country name \(self.selectedCountry.country_name), dial code \(self.selectedCountry.dial_code)")
        
        let imagestring = selectedCountry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImage.image = UIImage(named: imagePath)
        
        countryCodeLabel.text =  self.selectedCountry.dial_code
    }
    
    /// Filter Country Name
    ///
    /// - Parameter searchText: To search text in country name
    func filtercountry(_ searchText: String) {
        let picker = CountryNameViewController()
        picker.initialSetUp()
        let contry: Country
        contry = picker.localCountryName(searchText)
        let imagestring = contry.country_code
        let imagePath = "CountryPicker.bundle/\(imagestring).png"
        countryImage.image = UIImage(named: imagePath)
    }
    
}

extension ProfileViewController: MusicGenreViewDelegate {
    
    func setGenresData(data: [MusicGenreModel]) {
        
        genresArray = []
        
        var displayGenresName: String = ""
        var displayGenresId: String = ""
        
        for eachGenresDataModel in data {
            
            genresArray.append(eachGenresDataModel)
            
            if displayGenresName.length == 0 {
                
                displayGenresName = "\(eachGenresDataModel.musicGenreName)"
                displayGenresId = "\(eachGenresDataModel.musicGenreId)"
                
            }else {
                
                displayGenresName = "\(displayGenresName), \(eachGenresDataModel.musicGenreName)"
                displayGenresId = "\(displayGenresId),\(eachGenresDataModel.musicGenreId)"
            }
        }
        musicGenreApi = displayGenresId
//        musicGenreTextField.text = displayGenresName
    }
    
}

extension ProfileViewController: changeEmailMobileDelegate {
    func recallApi(sucess: Bool) {
        if sucess {
            sendServiceToGetProfile()
        }
    }
}

extension ProfileViewController: setPasswordDelegate {
    func passwordUpdated(sucess: Bool) {
        if sucess {
            sendServiceToGetProfile()
        }
    }
}

extension ProfileViewController: UITextViewDelegate {
    
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        aboutMeTextView.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
        
        if aboutMeTextView.text! == ALERTS.aboutMe {
            
            aboutMeTextView.text = ""
        }
        
        return true

    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.text.length > 250 {
            
            if text == "" {
                adjustUITextViewHeight(textView: textView)
                return true
            }
            
            return false
            
        } else if text == "\n" {
            
            textView.resignFirstResponder()
            return false
        }
        
        adjustUITextViewHeight(textView: textView)
        return true
    }

    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ALERTS.aboutMe || textView.text.length == 0 {
            
            textView.text = ALERTS.aboutMe
            aboutMeTextView.textColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8039215686, alpha: 1)
            
        } else {
            
            aboutMeTextView.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.231372549, alpha: 1)
        }
        
    }
    
}


extension ProfileViewController {
    func uploadProfileImageToAmazon() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }

        
        Helper.showPI(_message: PROGRESS_MESSAGE.Saving)
        if profileAlternatePhoto == 0 ||  profileAlternatePhoto == 2 {
            profilePhoto.image  = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
        AmazonManager.sharedInstance().upload(withImage: profilePhoto.image!,
                                              imgPath: "ProfileImages/") { (result, imageUrl) in
//                                                Helper.hidePI()
                                                self.profilePicURL = imageUrl
                                                self.updateUserDefault()
                                                self.serviceRequestToUpdateProfile()
        }
    }
}

extension ProfileViewController: UIScrollViewDelegate {
    
    /*func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if scrollView.contentOffset.y >= NavigationBarHeight {
            
            navigationController?.navigationBar.isTranslucent = false
            navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
            navigationController?.view?.backgroundColor = UIColor.white
            navigationController?.navigationBar.backgroundColor = UIColor.white
            
            self.title = nameTextField.text
            
        } else {
            
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            navigationController?.view?.backgroundColor = UIColor.clear
            navigationController?.navigationBar.backgroundColor = UIColor.clear
            
            self.title = ""
        }
        
    }*/
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let yOffset = scrollView.contentOffset.y
        
        if yOffset > 0 {
            
            if yOffset <= NavigationBarHeight { //tableHeaderView.frame.size.height
                
//                //Scroll to Top
//                if yOffset == NavigationBarHeight {
//
//                    //show Navigation
//                    self.navigationTopView.isHidden = false
//                    self.navigationTitleView.isHidden = false
//
//                    DDLogDebug("\nNo - 1, y = \(yOffset)")
//
//                } else {
                
                    hideNavigation()
                    DDLogDebug("\nNo - 2, y = \(yOffset)")
//                }
                
            } else {
                
                self.navigationTopView.isHidden = false
                self.navigationTitleView.isHidden = false
                
                DDLogDebug("\nNo - 3, y = \(yOffset)")
            }
            
        } else {
            
            hideNavigation()
            DDLogDebug("\nNo - 4, y = \(yOffset)")
        }
        
    }
    
    func showNavigation() {
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.view?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    
    func hideNavigation() {
        
        self.navigationTopView.isHidden = true
        self.navigationTitleView.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view?.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        Helper.statusBarView.backgroundColor = UIColor.clear
        
    }
    
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
         var  chosenImage = UIImage()
         chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage //2
        
         chosenImage = Helper.changeImageFrame(with: chosenImage, scaledTo: CGSize(width: 100, height: 100))
        
         profilePhoto.image = chosenImage
         profilePhoto.layer.borderWidth = 1
         profilePhoto.layer.borderColor = APP_COLOR.cgColor
         profileAlternatePhoto = 1
        
        dismiss(animated:true, completion: nil) //5

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
}

extension ProfileViewController {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
//        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension ProfileViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}


