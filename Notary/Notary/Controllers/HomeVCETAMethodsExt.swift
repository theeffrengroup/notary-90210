//
//  HomeVCETAMethodsExt.swift
//  Notary
//
//  Created by 3Embed on 20/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension HomeScreenViewController:DistanceAndETAManagerDelegate {
    
    func showETAMessage(message:String) {
        
        DispatchQueue.main.async(execute: {() -> Void in

            self.etaLabel.text = message
        })
    }
    
    func showETAIndicator() {
        
        DispatchQueue.main.async(execute: {() -> Void in
            
            self.etaActivityIndicator.startAnimating()
        })
    }
    
    func hideETAIndicator() {
        
        DispatchQueue.main.async(execute: {() -> Void in
            
            self.etaActivityIndicator.stopAnimating()
        })
    }
    
    func calculateETAAPICall() {
        
        self.showETAIndicator()
        
        distanceAndETAManager.mileageMatric = arrayOfProvidersModel[0].mileageMatric
        
        if distanceAndETAManager.currentGoogleServerKey.length > 0 {
            
            distanceAndETAManager.srcLat = appointmentLocationModel.pickupLatitude
            distanceAndETAManager.srcLong = appointmentLocationModel.pickupLongitude
            distanceAndETAManager.destLat = arrayOfProvidersModel[0].latitude
            distanceAndETAManager.destLong = arrayOfProvidersModel[0].longitude

            distanceAndETAManager.getDistanceAndETA()
            
        } else {
            
            currentShortestDistance = 0.0
            self.showETAMessage(message:"1\nMIN")
            self.hideETAIndicator()
        }
        
    }

    func distanceAndETARsponse(timeInMinute: String, distance: String) {
        
        if self.etaLabel != nil {
            
            self.showETAMessage(message:timeInMinute.replacingOccurrences(of: " ", with: "\n"))
            self.hideETAIndicator()
        }
    }
}
