//
//  ZendeskViewModel.swift
//  Zendesk
//
//  Created by Vengababu Maparthi on 26/12/17.
//  Copyright © 2017 Vengababu Maparthi. All rights reserved.
//


import UIKit
import RxAlamofire
import RxCocoa
import RxSwift


class NewTicketRequest {
    let subject :String!
    let body:String!
    let status:String!
    let type:String!
    let priority:String!
    let requester_id:String!
    init(sub:String,comments:String,status:String,ticketType:String,priority:String,req_id:String) {
        self.subject = sub
        self.body = comments
        self.status = status
        self.type = ticketType
        self.priority = priority
        self.requester_id = req_id
    }
}

class ZendeskModel:NSObject {
    
    let disposebag = DisposeBag()
    var modelData = ModelClass()
    var ticketsHist = historyModel()
    
    // creating new ticket
    func postTheNewTicket(newTicketData:NewTicketRequest,completionHandler:@escaping (Bool) -> ()) {
        
        let ticketDict:[String:Any] = ["subject"  :newTicketData.subject,
                                         "body"        :newTicketData.body,
                                         "status"      :"open",
                                         "priority"    :newTicketData.priority,
                                         "type"         :"problem",
                                         "requester_id": Utility.zendeskRequesterId]
        
        let rxApiCall = ZendeskAPI()
        rxApiCall.postTheNewTicket(paramDict:ticketDict)
        
        rxApiCall.Zendesk_Response
            .subscribe(onNext: {response in
                
                if response.httpStatusCode == HTTPSResponseCodes.SuccessResponse.rawValue {
                    
                    Helper.alertVC(title: ALERTS.Message, message: "you request for new ticket is successfull and ticket will be created soon")
                    
                    completionHandler(true)
                }else{
                   completionHandler(false)
                }
                
            }, onError: {error in
                
            }).disposed(by: disposebag)
    }
    
    // getting all tickets data
    func getTheTicketData(userID:String,completionHandler:@escaping (Bool,ModelClass) -> ()) {
        
        let rxApiCall = ZendeskAPI()
        rxApiCall.getTheTicketData(id:userID)
        rxApiCall.Zendesk_Response
            .subscribe(onNext: {response in
                
                if response.httpStatusCode == HTTPSResponseCodes.SuccessResponse.rawValue {
                    
                completionHandler(true,self.modelData.getTheParsedData(dict: response.data["data"] as! [String : Any]))
                }
                
            }, onError: {error in
            }).disposed(by: disposebag)
    }
    
    /// getting particular ticket id data
    func getTheTicketsHistory(userID:String,completionHandler:@escaping (Bool,historyModel) -> ()) {
        let rxApiCall = ZendeskAPI()
        rxApiCall.getTheTicketHistory(id:userID)
        rxApiCall.Zendesk_Response
            .subscribe(onNext: {response in
                if response.httpStatusCode == HTTPSResponseCodes.SuccessResponse.rawValue {
                    completionHandler(true,self.ticketsHist.getTheParsedTicketsData(dict: response.data["data"] as! [String : Any]))
                }
                
            }, onError: {error in
            }).disposed(by: disposebag)
    }
    
    // post the comment to already existing ticket using ticket id
    func postTheNewTicketComment(params:[String:Any],completionHandler:@escaping (Bool) -> ()) {
  
        let rxApiCall = ZendeskAPI()
        rxApiCall.postTheNewTicketComment(paramDict:params)
        rxApiCall.Zendesk_Response
            .subscribe(onNext: {response in
                if response.httpStatusCode == HTTPSResponseCodes.SuccessResponse.rawValue {
                    completionHandler(true)
                }else{
                    completionHandler(false)
                }
                
            }, onError: {error in
            }).disposed(by: disposebag)
    }
}


