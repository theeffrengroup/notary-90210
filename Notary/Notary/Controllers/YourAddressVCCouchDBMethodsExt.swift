//
//  YourAddressVCCouchDBMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension YourAddressViewController {
    
    func loadAddressFromCouchDB() {
        
        if Utility.manageAddressDocId.isEmpty {
            
            CouchDBManager.sharedInstance.createCouchDB()
            
        } else {
            
            addressArrayFromCouchDB = getDocumentDetailsFromCouchDB()
        }
        
        
        arrayOfAddress = []
        
        for addressDetail in addressArrayFromCouchDB {
            
            let addressDetailModel = AddressModel.init(manageAddressDetails: addressDetail)
            arrayOfAddress.append(addressDetailModel)
        }
        
        tableView.reloadData()
    }
    
    func getDocumentDetailsFromCouchDB() -> [Any] {
    
        return AddressCouchDBManager.sharedInstance.getManageAddressDetailsFromCouchDB()
    }
    
    func updateDocumentDetailsToCouchDB() {
        
        AddressCouchDBManager.sharedInstance.updateManageAddressDetailsToCouchDBDocument(manageAddressArray: addressArrayFromCouchDB)
    }
    
    
}
