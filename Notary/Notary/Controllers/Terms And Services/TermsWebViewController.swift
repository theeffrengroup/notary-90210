//
//  TermsWebViewController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 12/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
enum isTerms : Int {
    
    case terms = 0
    case conditions = 1
    case Default = 123
}

class TermsWebViewController: UIViewController {
    
    // MARK: - Outlets -
    @IBOutlet weak var webView: UIWebView!
    
    // MARK: - Variable Decleration -
    var webURL : String  = ""
    
    var isForTermsAndConditions : Bool  = false
    var navTitle: String = ""
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!

    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallSetup()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if !isForTermsAndConditions {
            
            self.title = navTitle
            
        } else if navTitle == "Legal" {
            
            webURL = Links.Legal
        }
        setupGestureRecognizer()
        initiallSetup()
    }
    
    // MARK: - Action Methods -
    @IBAction func backButtonAction(_ sender: AnyObject) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Custom Methods -
    func initiallSetup(){
        
        let url = NSURL (string: webURL);
        let requestObj = NSURLRequest(url: url! as URL);
        self.webView.loadRequest(requestObj as URLRequest);
        
        if isForTermsAndConditions && navTitle.length <= 0{
            
            self.title = "terms and conditions"
            
        } else if navTitle.length <= 0 {
            
            self.title = "privacy policy"
        }
    }
    
}

// MARK: - Web View Delegate Method -
extension TermsWebViewController : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        Helper.hidePI()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        Helper.hidePI()
    }
}

extension TermsWebViewController: UINavigationControllerDelegate {
    
    private func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension TermsWebViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}
