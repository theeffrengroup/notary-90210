//
//  SignUpModel.swift
//  DayRunner
//
//  Created by Rahul Sharma on 04/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

enum SignUpLoginType : Int {
    
    case normal = 1
    case facebook = 2
    case google = 3
}

enum SignUpAccountType : Int {
    
    case individual = 1
    case bussiness = 2
}

enum SignUpValidationType : Int {
    
    case email = 1
    case phone = 2
}


struct SignupDetails {
    
    var customerName = ""
    var profilePic = ""
    var companyName = ""
    var companyAddress = ""
    var phoneNumber = ""
    var email = ""
    var password = ""
    var addressLine1 = ""
    var addressLine2 = ""
    var CountryCode = ""
    var zipCode = ""
    var loginType: SignUpLoginType? = nil
    var accountType: SignUpAccountType? = nil
    var socialMediaId = ""
    var refferalCode = ""
}
