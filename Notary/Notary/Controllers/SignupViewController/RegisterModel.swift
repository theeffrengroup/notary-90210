//
//  RegisterModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 22/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

struct RegisterModel {
    
    var firstNameText = ""
    var lastNameText = ""
    var dobText = ""
    var emailText = ""
    var passwordText = ""
    var phoneNumberText = ""
    var referralCodeText = ""
    var registerType = RegisterType.Default
    var facebookId = ""
    var countryCode = ""
    var profilePicURL = ""
}
