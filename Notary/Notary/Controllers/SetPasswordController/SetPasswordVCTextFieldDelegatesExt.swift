//
//  SetPasswordTextFieldDelegatesExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension SetPasswordController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == oldPasswordTF {
            
            oldPassView.backgroundColor = #colorLiteral(red: 0.3882352941, green: 0.4235294118, blue: 0.7725490196, alpha: 1)
            newPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
            reenterPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
            
        }else if textField == newPassTF {
            
            newPassView.backgroundColor = #colorLiteral(red: 0.3882352941, green: 0.4235294118, blue: 0.7725490196, alpha: 1)
            oldPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
            reenterPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
            
        }else if textField == reEnterPassTF {
            
            reenterPassView.backgroundColor = #colorLiteral(red: 0.3882352941, green: 0.4235294118, blue: 0.7725490196, alpha: 1)
            oldPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
            newPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
        }
        
        return true
    }
    
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//        if forgetPass {
//
//            if textField == newPassTF{
//
//                animateButtonLoading(1)
//                reEnterPassTF.becomeFirstResponder()
//                oldPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
//            }

//                newPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1) //UIColor.init(hex: "#BABABA")
//
//            }else if textField == reEnterPassTF{
//                if reEnterPassTF.text?.length != 0 && oldPasswordTF.text?.length != 0 && newPassTF.text?.length != 0 {
//                    animateButtonLoading(2)
//                }else if reEnterPassTF.text?.length != 0 && newPassTF.text?.length != 0 {
//                    animateButtonLoading(2)
//                }
//                reenterPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
//
//            }
//
//            if textField == oldPasswordTF{
//
//                oldPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
//            }
//
//            if !Helper.isValidPassword(password: textField.text!)
//            {
//                Helper.alertVC(title: ALERTS.Message, message: ALERTS.PasswordInvalid)
//                textField.becomeFirstResponder()
//            }
//
//        } else {
//
//            if textField == newPassTF{
//
//                if textField == oldPasswordTF{
//                    animateButtonLoading(1)
//                    reEnterPassTF.becomeFirstResponder()
//                    oldPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
//                }
//
//                newPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1) //UIColor.init(hex: "#BABABA")
//
//            }else if textField == reEnterPassTF{
//                if reEnterPassTF.text?.length != 0 && oldPasswordTF.text?.length != 0 && newPassTF.text?.length != 0 {
//                    animateButtonLoading(2)
//                }else if reEnterPassTF.text?.length != 0 && newPassTF.text?.length != 0 {
//                    animateButtonLoading(2)
//                }
//                reenterPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
//
//            }
//
//            if textField == oldPasswordTF{
//
//                oldPassView.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
//            }
//
//            if !Helper.isValidPassword(password: textField.text!)
//            {
//                Helper.alertVC(title: ALERTS.Message, message: ALERTS.PasswordInvalid)
//                textField.becomeFirstResponder()
//            }
//        }
//
//
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var textFieldText = String()
        
        if range.length == 0 {//Entered New Character
            
            textFieldText = textField.text! + string
            
            animateButtonLoading(checkFieldsAreValid(textField: textField, textFieldText: textFieldText))

        }
        else {//Entered BackSpace
            
            textFieldText = String(textField.text!.dropLast(range.length))
            
            textField.text = textFieldText
            
            animateButtonLoading(checkFieldsAreValid(textField: textField, textFieldText: textFieldText))

            return false
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        animateButtonLoading(checkFieldsAreValid(textField: textField, textFieldText: ""))
        return true
    }
    
    func checkFieldsAreValid(textField:UITextField, textFieldText:String) -> Int {
        
        var oldPasswordText = oldPasswordTF.text!
        var newPasswordText = newPassTF.text!
        var confirmPasswordText = reEnterPassTF.text!
        var validFieldCount:Int = 0
        
        switch textField {
            
            case oldPasswordTF:
                oldPasswordText = textFieldText
            
            case newPassTF:
                newPasswordText = textFieldText
            
            case reEnterPassTF:
                confirmPasswordText = textFieldText
            
            default:
                break
        }
        
        if oldPasswordText.length > 0 {
            
            validFieldCount = validFieldCount + 1
        }
        
        if newPasswordText.length > 0 {
            
            validFieldCount = validFieldCount + 1
        }
        
        if confirmPasswordText.length > 0 {
            
            validFieldCount = validFieldCount + 1
        }
        
        return validFieldCount
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == oldPasswordTF {
            
            newPassTF.becomeFirstResponder()
            
        } else if textField == newPassTF {
            
            reEnterPassTF.becomeFirstResponder()
            
        }else if textField == reEnterPassTF {
            
            textField.resignFirstResponder()
        }
    
        return false
    }
    
    
}
