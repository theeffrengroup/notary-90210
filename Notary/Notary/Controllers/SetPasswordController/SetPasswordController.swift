//
//  SetPasswordController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 14/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


protocol setPasswordDelegate {
    func passwordUpdated(sucess: Bool)
}

class SetPasswordController: UIViewController, AccessTokeDelegate {
    // MARK: - Outlets -
    //Views
    @IBOutlet weak var oldPassYConstrain: NSLayoutConstraint!
    @IBOutlet weak var newPassView: UIView!
    @IBOutlet weak var reenterPassView: UIView!
    @IBOutlet weak var continueBackView: UIViewCustom!
    @IBOutlet weak var oldPassView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    // Label
    @IBOutlet weak var passwordText: UILabel!
    
    // Text Field
    @IBOutlet weak var newPassTF: UITextField!
    @IBOutlet weak var reEnterPassTF: UITextField!
    @IBOutlet weak var oldPasswordTF: FloatLabelTextField!
    
    // Button
    @IBOutlet weak var continueButtonOutlet: UIButtonCustom!
    
    // Layout Contstrain
    @IBOutlet weak var loadingViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabelTopConstraint: NSLayoutConstraint!
    
    
    
    // MARK: - Variable Decleration -
    var forgetPass: Bool = false
    var phoneNumber: String = ""
    let catransitionAnimationClass = CatransitionAnimationClass()
    var countryCode: String = ""
    let acessClass = AccessTokenRefresh()
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    var apiName: Int!
    var userID: [String:Any] = [:]
    var delegate: setPasswordDelegate?
    
    var setPasswordViewModel = SetPasswordViewModel()
    let disposeBag = DisposeBag()
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallSetup()
        
        if !forgetPass {
            
            self.titleLabel.text = ""
            self.titleLabelTopConstraint.constant = 0
            
        } else {
            
            self.titleLabelTopConstraint.constant = 20
        }
        
        self.passwordText.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        animateBottomView()
        
        if forgetPass {
            
            self.newPassTF.becomeFirstResponder()
            
        } else {
            
            self.oldPasswordTF.becomeFirstResponder()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDelegate?.accessTokenDelegate = self
        addObserveToVariables()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        appDelegate?.accessTokenDelegate = nil
    }
    
    // MARK: - Action Methods -
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func continueButtonAction(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if !forgetPass {
            
            if (oldPasswordTF.text)! != Utility.customerPassword {
                
                Helper.showAlert(head: ALERTS.Message, message: ALERTS.SET_NEW_PASSWORD.WrongOldPassword)
                return
            }
            
        }
        
        if !Helper.isValidPassword(password: (newPassTF.text)!) {
            
            Helper.showAlert(head: ALERTS.Message, message: ALERTS.SET_NEW_PASSWORD.NewPasswordInvalid)

        } else if (newPassTF.text)! != (reEnterPassTF.text)! {
            
            Helper.showAlert(head: ALERTS.Message, message: ALERTS.SET_NEW_PASSWORD.NewAndConfirmPasswordMisMatch)
            
        } else {
            
             if forgetPass {
                
                changePassword()

             } else {
                
                updatePassword()
            }
            
        }
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapGestureAction(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    @objc func recallApi() {
        updatePassword()
    }
    
    func addObserveToVariables() {
        
        newPassTF.rx.text
            .orEmpty
            .bind(to: setPasswordViewModel.newPasswordText)
            .disposed(by: disposeBag)
        
        reEnterPassTF.rx.text
            .orEmpty
            .bind(to: setPasswordViewModel.reEnterPasswordText)
            .disposed(by: disposeBag)
        
        oldPasswordTF.rx.text
            .orEmpty
            .bind(to: setPasswordViewModel.oldPasswordText)
            .disposed(by: disposeBag)

        
    }

    
}


// MARK: - UINavigationControllerDelegate
extension SetPasswordController: UINavigationControllerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

