//
//  SetPasswordViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class SetPasswordViewModel {
    
    var newPasswordText = Variable<String>("")
    var oldPasswordText = Variable<String>("")
    var reEnterPasswordText = Variable<String>("")
    var userId = ""
    
    let disposebag = DisposeBag()
    
    let rxSetPasswordAPICall = SetPasswordAPI()
    
    func changePasswordBeforeLoginAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxSetPasswordAPICall.changePasswordBeforeLoginServiceAPICall(reEnterPasswordText: reEnterPasswordText.value,
                                                                     userId: userId)
        
        if !rxSetPasswordAPICall.changePasswordBeforeLogin_Response.hasObservers {
            
            rxSetPasswordAPICall.changePasswordBeforeLogin_Response
            .subscribe(onNext: {response in
                
                if (response.data[SERVICE_RESPONSE.Error] != nil) {
                    
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    return
                }
                
                completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                
                
            }, onError: {error in
                
            }).disposed(by: disposebag)

        }
        
    }
    

    func changePasswordAfterLoginAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxSetPasswordAPICall.changePasswordAfterLoginServiceAPICall(reEnterPasswordText: reEnterPasswordText.value,
                                                                    oldPasswordText: oldPasswordText.value)
        
        if !rxSetPasswordAPICall.changePasswordAfterLogin_Response.hasObservers {
            
            rxSetPasswordAPICall.changePasswordAfterLogin_Response
            .subscribe(onNext: {response in
                
                if (response.data[SERVICE_RESPONSE.Error] != nil) {
                    
                    Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                    return
                }
                
                completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                
                
            }, onError: {error in
                
            }).disposed(by: disposebag)

        }
        
    }
    
}

