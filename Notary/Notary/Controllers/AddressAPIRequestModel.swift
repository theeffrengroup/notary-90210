//
//  File.swift
//  LiveM
//
//  Created by Rahul Sharma on 25/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

struct AddressAPIRequestModel {
    
    var addressLine1 = ""
    var addressLine2 = ""
    var city = ""
    var state = ""
    var country = ""
    var pincode = ""
    var latitude = 0.0
    var longitude = 0.0
    var addressType = ""
    var addressId = ""
}
