//
//  HomeListVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension HomeListViewController {
    
    func initializeMQTTMethods(initial:Bool) {
        
        musiciansListManager.delegate = self
        musiciansListManager.navigation = navigationController
        musiciansListManager.viewController = self
        musiciansListManager.timeInterval = Double(ConfigManager.sharedInstance.customerPublishLocationInterval)
        
        if isAddressChanged {

            isAddressChanged = false
            Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
            if appointmentLocationModel.bookingType == BookingType.Schedule {
                
                musiciansListManager.publish(isIinitial: false)
                
            } else {
                
                musiciansListManager.publish(isIinitial: true)
            }
            
        } else {
            
            if initial {
                
                musiciansListManager.publish(isIinitial: true)
                
            } else {
                
                musiciansListManager.publish(isIinitial: false)
            }
        }
        
        

    }
    
    func uninitializeMQTTMethods() {
        
        musiciansListManager.stopPublish()
        musiciansListManager.navigation = nil
        musiciansListManager.viewController = nil
        musiciansListManager.delegate = nil
    }
    
    /// initiall Setup For Controller
    func initiallSetup() {
        
        selectedDirection = UITableView.Direction.top(useCellsFrame: true)
        buttonBottomView.transform = CGAffineTransform(translationX: 0, y: buttonBottomView.frame.origin.y + 80 )
        self.tableView.reloadData(with: .simple(duration: 1.5, direction: self.selectedDirection, constantDelay: 0), reversed: false)
        
    }
    
    func showProfileImageView() {
        
        profileImageView.layer.borderColor = APP_COLOR.cgColor
        self.profileImageView.layer.borderWidth = 1
        
        if !Utility.profilePic.isEmpty {
            
            profileImageView.kf.setImage(with: URL(string: Utility.profilePic),
                                         placeholder:#imageLiteral(resourceName: "myevent_profile_default_image"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock: { receivedSize, totalSize in
            },
                                         completionHandler: { image, error, cacheType, imageURL in
                                            
            })
            
        } else {
            
            profileImageView.image = #imageLiteral(resourceName: "myevent_profile_default_image")
        }
    }
    
    
    /// Flip Animation For View Controller
    ///
    /// - Parameter idntifier: Identifier
    func flipAnimation(_ idntifier: String){
        let search = self.storyboard!.instantiateViewController(withIdentifier: idntifier)
        UIView.beginAnimations("animation", context: nil)
        UIView.setAnimationDuration(1.0)
        self.navigationController!.pushViewController(search, animated: false)
        UIView.setAnimationTransition(UIView.AnimationTransition.flipFromLeft, for: self.navigationController!.view, cache: false)
        UIView.commitAnimations()
    }
    
    /// Catransition Animation To View Controller
    ///
    /// - Parameter idntifier: Identifier
    func catransitionAnimation(idntifier: String){
        let dstVC = self.storyboard!.instantiateViewController(withIdentifier: idntifier)
        
        
        TransitionAnimationWrapperClass.caTransitionAnimationType(kCATransitionMoveIn,
                                                                  subType: kCATransitionFromTop,
                                                                  for: (self.navigationController?.view)!,
                                                                  timeDuration: 0.3)
        
        self.navigationController?.pushViewController(dstVC, animated: false)
    }

    func showDateSelectOption() {
        
        self.topAddressDivider.isHidden = false
        self.calendarButtonWidthConstraint.constant = 40
    }
    
    func hideDateSelectOption() {
        
        self.topAddressDivider.isHidden = true
        self.calendarButtonWidthConstraint.constant = 0
    }
    
    func setTimeZonesToCalendar() {
        
        self.datePickerView.timeZone = Helper.getTimeZoneFromLoaction(latitude: appointmentLocationModel.pickupLatitude,
                                                                      longitude: appointmentLocationModel.pickupLongitude)
        
        if appointmentLocationModel.bookingType == BookingType.Schedule {
            
            self.updateSelectedBookLaterDetails()
        }
        
    }
}
