//
//  MDGigTimeTableVieCell.swift
//  LiveM
//
//  Created by Apple on 08/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class MDGigTimeTableViewCell:UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var noGigTimeBackView: UIView!
    @IBOutlet var noGigTimeLabel: UILabel!
    
    
    var CBModel:ConfirmBookingModel!
    var isConfirmBooking:Bool = false
    
}

extension MDGigTimeTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if CBModel.providerFullDetalsModel != nil {
            
            if CBModel.providerFullDetalsModel.gigTimeArray.count > 0 {
                
                self.noGigTimeBackView.isHidden = true
                self.collectionView.isHidden = false
                
                return 1
                
            } else {
                
                self.noGigTimeBackView.isHidden = false
                self.collectionView.isHidden = true
                
                return 0
            }
            
        }
        
        self.noGigTimeBackView.isHidden = false
        self.collectionView.isHidden = true
        
        return 0

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return CBModel.providerFullDetalsModel.gigTimeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let gigTimeCell:MDGigTimeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "gigTimeCell", for: indexPath) as! MDGigTimeCollectionViewCell
        
        let gigTimeDict = CBModel.providerFullDetalsModel.gigTimeArray[indexPath.row] as! [String:Any]
        
        
//        gigTimeCell.timeLabel.text = String(format:"%@ %@.\n%@ %@", GenericUtility.strForObj(object:gigTimeDict["name"]), GenericUtility.strForObj(object:gigTimeDict["unit"]), CBModel.providerFullDetalsModel.currencySymbol, GenericUtility.strForObj(object:gigTimeDict["price"]))
        
//        gigTimeCell.timeLabel.text = String(format:"%@ %@", GenericUtility.strForObj(object:gigTimeDict["name"]), GenericUtility.strForObj(object:gigTimeDict["unit"]))
        
        gigTimeCell.timeLabel.text = String(format:"%@ %@\n%@ %@", GenericUtility.strForObj(object:gigTimeDict["name"]), GenericUtility.strForObj(object:gigTimeDict["unit"]), CBModel.providerFullDetalsModel.currencySymbol, GenericUtility.strForObj(object:gigTimeDict["price"]))


        
        
        
        gigTimeCell.timeLabel.textColor = UIColor.black
        gigTimeCell.selectedDivider.isHidden = true
        
        if isConfirmBooking {
            
            if CBModel.selectedGigTimeTag == indexPath.row {
                
                gigTimeCell.timeLabel.textColor = UIColor.red
                gigTimeCell.selectedDivider.isHidden = false
                
                CBModel.selectedGigTimeAmount = Double(GenericUtility.strForObj(object:gigTimeDict["price"]))!
                
                CBModel.selectedGigTimeId = GenericUtility.strForObj(object:gigTimeDict["id"])
                
                gigTimeCell.timeLabel.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                
                UIView.animate(withDuration: 0.8, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 3, options: [], animations: {
                    
                    gigTimeCell.timeLabel.transform = .identity
                    
                }) { (success) in
                    
                }

                
            }
        }
        
        return gigTimeCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sizeOfEachCell:CGSize = CGSize.init(width: (collectionView.bounds.size.width - 30)/3.5, height: 65)
        return sizeOfEachCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isConfirmBooking {
            
//            if let cell = self.collectionView.cellForItem(at: indexPath) as? MDGigTimeCollectionViewCell {
//                
//                cell.timeLabel.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
//                
//                UIView.animate(withDuration: 0.8, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 3, options: [], animations: {
//                    
//                    cell.timeLabel.transform = .identity
//                    
//                }) { (success) in
//                    
//                }
//                
//            }

            
            if CBModel.selectedGigTimeTag != indexPath.row {
                
                CBModel.selectedGigTimeTag = indexPath.row
                
                self.collectionView.reloadData()
            }
            
            
           
        }
    }


    
}
