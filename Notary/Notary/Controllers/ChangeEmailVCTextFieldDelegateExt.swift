//
//  ChangeEmailTextFieldDelegateExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension ChangeEmailMobileViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        validateFields(textFieldText: textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        var textFieldText = ""
        
        if range.length == 0 {//Entered New Character
            
            textFieldText = textField.text! + string
        }
        else {//Entered BackSpace
            
            textFieldText = String(textField.text!.dropLast())
        }
        
        validateFields(textFieldText: textFieldText)
        
        return true
    }
    
    /// Animate Button Loading for user Id password
    ///
    /// - Parameter value: value to decide its percent of loading
    func animateButtonLoading(_ value: Int) {
        self.view.layoutIfNeeded()
        if value == 1 {
            self.loadingWidth.constant = self.continueButton.frame.size.width
        }else {
            self.loadingWidth.constant = 0
        }
        
        UIView.animate(withDuration: 0.6, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func validateFields(textFieldText:String) {
        
        if isEmail {
            
            if Helper.isValidEmail(testStr: textFieldText) {
                
                animateButtonLoading(1)
                
            } else {
                
                animateButtonLoading(0)
            }
            
        } else {
            
            if textFieldText.length > 5 {
                
                animateButtonLoading(1)
                
            }else {
                
                animateButtonLoading(0)
            }
            
        }

    }
    
}
