//
//  ConfirmBookingVCCustomMethods.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import Kingfisher

extension ConfirmBookingScreen {
    
    func initialSetup() {
        
        arrayOfHeaderTitles = [
            "WHEN DO YOU WANT TO BOOK FOR?",
            "JOB LOCATION",
            "PAYMENT METHOD",
            "PROMOCODE",
            "YOUR PAYMENT BREAKDOWN"
        ]
    
        
//        if Helper.SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(version: "11.0") {
//
//            self.tableViewTopConstraint.constant = -NavigationBarHeight
//            tableView.layoutIfNeeded()
//        }
        
        bookButton.setTitle(ALERTS.CONFIRM_BOOKING.BookButton, for: UIControl.State.normal)
        self.tableView.reloadData()
        
    }
    
    func showBookLaterDetails() {
        
        if CBModel.appointmentLocationModel.bookingType == BookingType.Default {
            
            scheduleDate = TimeFormats.roundDate(Date.dateWithDifferentTimeInterval().addingTimeInterval(3600 * 2))
            datePickerView.minimumDate = scheduleDate
            CBModel.appointmentLocationModel.scheduleDate = scheduleDate
            selectedDate = CBModel.appointmentLocationModel.scheduleDate
            datePickerView.date = selectedDate
            
            self.showBookButton()

        } else {
            
            selectedDate = CBModel.appointmentLocationModel.scheduleDate
            datePickerView.date = selectedDate
            
            self.disableBookButton()

            scheduleBookingAPI()
        }
        
        self.tableView.reloadData()

    }
    
    
    func fetchDefaultCardDetails() {
        
        var defaultCardArray:[Any] = []
        
        if !Utility.defaultCardDocId.isEmpty {
            
            defaultCardArray = PaymentCardManager.sharedInstance.getDefaultCardDocumentDetailsFromCouchDB(documentId: Utility.defaultCardDocId)
        }
        
        if defaultCardArray.count > 0 {
            
            CBModel.selectedCardModel = CardDetailsModel.init(selectedCardDetails: defaultCardArray[0])
        }
        
    }
    
    func showBookButton() {
        
        self.bookButton.isUserInteractionEnabled = true
        self.bookButton.backgroundColor = APP_COLOR
    }
    
    func disableBookButton() {
        
        self.bookButton.isUserInteractionEnabled = false
        self.bookButton.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }
    
    func bookNowValidation() {
        
        if CBModel.appointmentLocationModel.pickupAddress.length == 0 {
            
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.CONFIRM_BOOKING.MissingAddress)
            
        } else if CBModel.paymentmethodTag == 0 && CBModel.selectedCardModel == nil {
            
            Helper.alertVC(title: ALERTS.Message, message: ALERTS.CONFIRM_BOOKING.MissingPayment)
            
        } else {
            
            customerLastDueAPI()
//            liveBookingAPI()
        }
        
    }
    
    func clearPromoCodeDetails() {
        
        isPromoCodeValid = false
        
        CBModel.promoCodeText = ""
        CBModel.discountType = ""
        CBModel.discountValue = 0.0
        
    }
    
    func showValidatedPromoCodeDetails(discountAmount:Double) {
        
        isPromoCodeValid = true
        
        CBModel.promoCodeText = (promocodeCell.promocodeTextField?.text)!
        CBModel.discountValue = discountAmount
        CBModel.discountType = "Fixed"
        
    }
    
    func setTimeZonesToCalendar() {
        
        self.datePickerView.timeZone = Helper.getTimeZoneFromLoaction(latitude: CBModel.appointmentLocationModel.pickupLatitude,
                                                                      longitude: CBModel.appointmentLocationModel.pickupLongitude)
        
    }
    
}
