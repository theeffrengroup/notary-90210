//
//  ConfirmBookingScreen.swift
//  LiveM
//
//  Created by Rahul Sharma on 16/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Foundation

class ConfirmBookingScreen:UIViewController,KeyboardDelegate {
    
    @IBOutlet weak var navigationBackButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
  
    @IBOutlet weak var bookButtonBackView: UIView!
    
    @IBOutlet weak var bookButton: UIButtonCustom!
    
    @IBOutlet weak var datePickerBackView: UIView!
    
    @IBOutlet weak var datePickerView: UIDatePicker!
    
    @IBOutlet weak var datePickerCancelButton: UIButtonCustom!
    
    @IBOutlet weak var datePickerSaveButton: UIButtonCustom!
    
    @IBOutlet weak var dataPickerBackView: UIView!
    
    
    @IBOutlet weak var lastDueBackView: UIView!
    
    @IBOutlet weak var lastDueTitleLabel: UILabel!
    
    @IBOutlet weak var lastDueAddressLabel: UILabel!
    
    @IBOutlet weak var lastDueDateLabel: UILabel!
    
    @IBOutlet var lastDueCancelButton: UIButton!
    
    @IBOutlet weak var lastDueConfirmButton: UIButton!
    
    @IBOutlet weak var lastDueBackViewBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var bookButtonBackViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var datePickerViewBottomConstraint: NSLayoutConstraint!
    
    
    
    var arrayOfHeaderTitles:[String] = []
    
    var apiTag:Int!
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    var bookNowOrLaterCell: CBBookNowOrLaterTableViewCell!
    var addressCell:CBAddressTableViewCell!
    var paymentCell:CBPaymentMethodCell!
    var promocodeCell:CBPromocodeTableViewCell!
    
    var CBModel:ConfirmBookingModel = ConfirmBookingModel.sharedInstance()
    
    let confirmBookingViewModel = ConfirmBookingViewModel()
    
    var appDelegate: AppDelegate? = (UIApplication.shared.delegate as? AppDelegate)
    
    var currencySymbol = Utility.currencySymbol
    
    var selectedServices = [ServicesModel]()
    
    var scheduleDate:Date!
    var selectedDate:Date!
    
    var isPromoCodeValid = false
    
    
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 250
        self.tableView.estimatedSectionHeaderHeight = 100
        
        
        
        if scheduleDate == nil {
            
            scheduleDate = TimeFormats.roundDate(Date.dateWithDifferentTimeInterval().addingTimeInterval(3600 * 2))
            datePickerView.minimumDate = scheduleDate
            
        }
        
        self.disableBookButton()
        
        initialSetup()
        fetchDefaultCardDetails()
        showBookLaterDetails()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setTimeZonesToCalendar()
        
        acessClass.acessDelegate = self
        appDelegate?.keyboardDelegate = self
        
        self.tableView.reloadData()
        
        self.tableView.scrollRectToVisible(CGRect.init(x: 0, y: 0, width: 1, height: 1), animated: false)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
       
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
        appDelegate?.keyboardDelegate = nil
        
        self.datePickerCancelButtonAction(self.datePickerCancelButton)
        self.lastDueCancelButtonAction(self.lastDueCancelButton)
    }
    
    
    // MARK: - Keyboard Methods -
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillShow(notification: NSNotification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.tableView.contentInset = contentInsets
        self.tableView.scrollIndicatorInsets = contentInsets
        
    }
    
    
    /**
     *  Keyboard Shown Delegate
     *
     *  @param notification Keyboard notification details
     */
    @objc func keyboardWillHide(notification: NSNotification) {
        
        //Once keyboard disappears, restore original positions
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0,bottom: 0.0,right: 0.0)
        
        UIView.animate(withDuration: 0.2) {
            
            self.tableView.contentInset = contentInsets
            self.tableView.scrollIndicatorInsets = contentInsets
        }
        
    }
   
}

extension ConfirmBookingScreen:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
            case RequestType.liveBooking.rawValue:
                
                liveBookingAPI()
                
                break
            
            case RequestType.ScheduleBookingDetails.rawValue:
                
                liveBookingAPI()
                
                break
            
            case RequestType.CustomerLastDues.rawValue:
                
                customerLastDueAPI()
                
                break
            
            case RequestType.ValidatePromoCode.rawValue:
                
                validatePromocodeAPI()
                
                break
                
            default:
                break
        }
    }
    
}
