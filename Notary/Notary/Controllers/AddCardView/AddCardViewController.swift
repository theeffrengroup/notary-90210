//
//  AddCardViewController.swift
//  LiveM
//
//  Created by Rahul Sharma on 09/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class AddCardViewController: UIViewController {

    // MARK: - Outlets -
    @IBOutlet weak var paymentBackgroundView: UIView!
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var scanButton: UIButtonCustom!

    // MARK: - Variable Declearation -
    
    let screenSize = UIScreen.main.bounds
    var paymentTextField:StripePaymentTextField!
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    
    let stripeManager = StripeManager.sharedInstance()
    
    var apiTag:Int!
    
    var cardToken:String!
    
    var addPaymentViewModel = AddPaymentViewModel()
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!

    
    
    static var obj:AddCardViewController? = nil
    
    class func sharedInstance() -> AddCardViewController {
    
        return obj!
    }
    
    
    // MARK: - Default Class Method -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        AddCardViewController.obj = self
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        stripeManager.setStripeDefaultPublishableKey(publishableKey: Utility.paymentGatewayAPIKey)
        
        setPaymentTextFieldProperties()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        acessClass.acessDelegate = self
        setupGestureRecognizer()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
    }


    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        paymentTextField.becomeFirstResponder()
    }
    
    
    // MARK: - Action methods -
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func doneAction(_ sender: Any) {
        
        if !paymentTextField.isValid {
            
            Helper.showAlert(head: ALERTS.Message, message:ALERTS.PAYMENT.MissingCardDetails)
        }
            
        else if stripeManager.defaultPublishableKey().length == 0 {
            
            Helper.showAlert(head: ALERTS.Message, message:ALERTS.PAYMENT.MissingStripeKey)
        }
        else {
            
            Helper.showPI(_message: PROGRESS_MESSAGE.Adding)
            
            stripeManager.createStripeCardToken(cardParams: paymentTextField.cardParams, completion: { (tokenId, error) in
                
                if (error != nil) {
                    
                    Helper.hidePI()
                    Helper.showAlert(head: ALERTS.Error, message:(error!.localizedDescription))
                }
                else {
                    
                    DDLogDebug("Card Token: \(String(describing: tokenId))")
                    self.cardToken = tokenId
                    self.addCardAPI(tokenId: tokenId!)
                    
                }

            })
            
        }

    }
    
    
    
    @IBAction func scanCardAction(_ sender: Any) {
        
        let scanCardViewController = CardIOPaymentViewController.init(paymentDelegate: self as CardIOPaymentViewControllerDelegate)
        scanCardViewController?.hideCardIOLogo = true
        scanCardViewController?.disableManualEntryButtons = true
        scanCardViewController?.modalPresentationStyle = UIModalPresentationStyle.formSheet
        scanCardViewController?.title = "Scan Card"
        scanCardViewController?.navigationController?.isNavigationBarHidden = false
        
        self.present(scanCardViewController!, animated: true, completion: nil)
    }
    
    
}

extension AddCardViewController:CardIOPaymentViewControllerDelegate {
    //MARK: - Card IO Deleagte -
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        
        self.dismiss(animated: true, completion: nil)
        
        stripeManager.validateCardDetails(cardNumber: cardInfo.cardNumber,
                                        expiryMonth: cardInfo.expiryMonth,
                                        expiryYear: cardInfo.expiryYear,
                                        cvv: cardInfo.cvv,
                                        completion: { (isValid) in
                        
                                            if isValid {
                                                
                                                self.doneAction(self.doneButton)
                                                
                                            } else {
                                                
                                                Helper.showAlert(head: ALERTS.Message, message:ALERTS.PAYMENT.MissingCardDetails)
                                            }
        })
        
        
    }
    
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension AddCardViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
        case RequestType.addCard.rawValue:
            
            if cardToken != nil {
                
                 addCardAPI(tokenId:cardToken)
            }
            
            break
            
        default:
            break
        }
    }
    
}


extension AddCardViewController {
    
    func setPaymentTextFieldProperties() {
        
        paymentTextField = StripeManager.sharedInstance().getPaymentTextField(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenSize.size.width - 100), height: CGFloat(40)))
        
        paymentBackgroundView.addSubview(paymentTextField)
    }
    
}

extension AddCardViewController: UINavigationControllerDelegate {
    
    private func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension AddCardViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    
}

