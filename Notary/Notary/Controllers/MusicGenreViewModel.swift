//
//  MusicGenreViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class MusicGenreViewModel {
    
    let disposebag = DisposeBag()
    
    let rxMusicGenreAPI = MusicGenreAPI()
    
    func getMusicGenreAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        rxMusicGenreAPI.getMusicGenreServiceAPICall()
        
        if !rxMusicGenreAPI.getMusicGenre_Response.hasObservers {
            
            rxMusicGenreAPI.getMusicGenre_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
    }
}
