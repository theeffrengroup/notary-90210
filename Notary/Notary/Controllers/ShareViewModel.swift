//
//  ShareViewModel.swift
//  Notary
//
//  Created by 3Embed on 23/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class ShareViewModel {
    
    let disposebag = DisposeBag()
    
    func getReferralCodeAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxReferralCodeAPICall = ReferralCodeAPI()
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        if !rxReferralCodeAPICall.referralCode_Response.hasObservers {
            
            rxReferralCodeAPICall.referralCode_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                }).disposed(by: disposebag)
            
        }
        
        rxReferralCodeAPICall.getReferralCodeServiceAPICall()
    }
}
