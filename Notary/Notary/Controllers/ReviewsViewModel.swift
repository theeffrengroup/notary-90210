//
//  ReviewsViewModel.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


class ReviewsViewModel {
    
    let disposebag = DisposeBag()
    var methodName = ""
    var pageNo = 0
    
    func getReviewsListAPICall(completion:@escaping (Int,String?,Any?) -> ()) {
        
        let rxReviewsAPICall = ReviewsAPI()
        
        if !rxReviewsAPICall.getReviewsList_Response.hasObservers {
            
            rxReviewsAPICall.getReviewsList_Response
                .subscribe(onNext: {response in
                    
                    if (response.data[SERVICE_RESPONSE.Error] != nil) {
                        
                        Helper.showAlert(head: ALERTS.Error, message: response.data[SERVICE_RESPONSE.Error] as! String)
                        return
                    }
                    
                    completion(response.httpStatusCode, response.data[SERVICE_RESPONSE.ErrorMessage] as? String , response.data[SERVICE_RESPONSE.DataResponse])
                    
                }, onError: {error in
                    
                    completion(0, error.localizedDescription, nil)
                    
                }).disposed(by: disposebag)
            
        }
        
        rxReviewsAPICall.getReviewsListServiceAPICall(methodName: methodName,pageNo:pageNo)
    }
}
