//
//  HelpCollectionViewCell.swift
//  Iserve
//
//  Created by Rahul Sharma on 25/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class HelpCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var discriptionLabel: UILabel!
    
}
