//
//  BDTwoPaymentDetailsTableViewCell.swift
//  Notary
//
//  Created by 3Embed on 27/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class BDTwoPaymentDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet var cardAndCashBackView: UIView!
    
    @IBOutlet weak var paymentTypeTitleLabel: UILabel!
    
    @IBOutlet var cardDetailsbackView: UIView!
    
    @IBOutlet var cardNumberLabel: UITextField!
    
    @IBOutlet weak var cardImageButton: UIButton!
    
    @IBOutlet var walletTitleLabel: UITextField!
    
    @IBOutlet weak var walletImageButton: UIButton!
    
    @IBOutlet var cardAmountLabel: UILabel!
    
    @IBOutlet var walletAmountLabel: UILabel!
    
    func showPaymentMethodDetails(bookingDetails:BookingDetailsModel) {
        
        walletTitleLabel.text = "Wallet"
        
        cardAmountLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",bookingDetails.amountPaidByCardOrCash), currencySymbol: bookingDetails.currencySymbol)
        
        walletAmountLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",bookingDetails.amountPaidByWallet), currencySymbol: bookingDetails.currencySymbol)
        
        walletImageButton.setImage(#imageLiteral(resourceName: "cash"), for: UIControl.State.normal)
        
        if bookingDetails.paymentTypeValue == 2 {
            
            paymentTypeTitleLabel.text = bookingDetails.paymentTypeText.capitalized
            
            cardDetailsbackView.isHidden = false
            
            if bookingDetails.cardNumber.length > 0 {
                
                cardNumberLabel.text = "**** **** **** " + bookingDetails.cardNumber
            }
            else {
                
                cardNumberLabel.text = ""
            }
            
            cardImageButton.setImage(Helper.cardImage(with:bookingDetails.cardBrand), for: UIControl.State.normal)
            
        } else {
            
            paymentTypeTitleLabel.text = bookingDetails.paymentTypeText.capitalized
            
            cardDetailsbackView.isHidden = false
            
            cardNumberLabel.text = "Cash"
            cardImageButton.setImage(#imageLiteral(resourceName: "cash"), for: UIControl.State.normal)
            
        }
        
    }
}
