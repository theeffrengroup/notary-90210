//
//  BDBookingStatusTableViewCell.swift
//  Notary
//
//  Created by Rahul Sharma on 22/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class BDBookingStatusTableViewCell: UITableViewCell {
    
    @IBOutlet weak var statusLabel: UILabel!
    
}
