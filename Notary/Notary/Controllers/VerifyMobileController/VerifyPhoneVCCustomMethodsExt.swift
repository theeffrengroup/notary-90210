//
//  VerifyPhoneCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension VerifyMobileViewController{
    
    func initiallSetup() {
        otpImage1.transform = CGAffineTransform(scaleX: 0, y: 0)
        otpImage2.transform = CGAffineTransform(scaleX: 0, y: 0)
        otpImage3.transform = CGAffineTransform(scaleX: 0, y: 0)
        otpImage4.transform = CGAffineTransform(scaleX: 0, y: 0)
        verifyBackView.transform = CGAffineTransform(translationX: -1000, y: 0)
    }
    
    func initiallAnimation() {
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
            self.otpImage1.transform = .identity
        }) { (true) in
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                self.otpImage2.transform = .identity
            }) { (true) in
                UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                    self.otpImage3.transform = .identity
                }) { (true) in
                    UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                        self.otpImage4.transform = .identity
                    }) { (true) in
                        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                            self.verifyBackView.transform = .identity
                            self.code1.becomeFirstResponder()
                        })
                    }
                }
            }
        }
    }
    
    func animatingLoadingView(_ value: Int) {
        let buttonheight = verifyButtonOutlet.frame.size.width / 4
        self.view.layoutIfNeeded()
        loadingViewWidth.constant = buttonheight * CGFloat(value)
        UIView.animate(withDuration: 0.6, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func calculateTheLoadingHeight() -> Int {
        var length = 0
        if code1.text?.length != 0 {
            length = length + 1
        }
        if code2.text?.length != 0 {
            length = length + 1
        }
        if code3.text?.length != 0 {
            length = length + 1
        }
        if code4.text?.length != 0 {
            length = length + 1
        }
        
        return length
    }
    
    /// add Timer
    func addTimer() {
        countTimer = 120
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(VerifyMobileViewController.update), userInfo: nil, repeats: true)
    }
    
    /// update Timer
    @objc func update() {
        if(countTimer > 0) {
            countTimer -= 1
            let min:Int = countTimer / 60
            let sec:Int = countTimer % 60
            timerLabel.text = "\(min) : \(sec)"
            timerLabel.isEnabled = true
            timerLabel.isHidden = false
            resendCodeButton.isEnabled = false
            resendCodeButton.isUserInteractionEnabled = false
            resendCodeButton.setTitleColor(UIColor.lightGray, for: .normal)
        }else {
            resendCodeButton.setTitleColor(#colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1) , for: .normal)
            resendCodeButton.isEnabled = true
            resendCodeButton.isUserInteractionEnabled = true
        }
    }
    
    
    // MARK: - Segue Method -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEgueIdetifiers.verMobileToSetPass {
            if let dstVC = segue.destination as? SetPasswordController {
                dstVC.forgetPass = true
                dstVC.countryCode = countryCode
                dstVC.userID = sender as! [String : Any]
                dstVC.phoneNumber = phoneNumber
            }
        }
    }
    
}
