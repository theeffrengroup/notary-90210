//
//  BDTableViewMethodsExt.swift
//  Notary
//
//  Created by Rahul Sharma on 22/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension BookingDetailsViewController:UITableViewDataSource,UITableViewDelegate {
 
    enum BookingDetailsSections: Int {
        
        case BookingStatus = 0
        case TotalBillAmount = 1
        case NotaryDetails = 2
        case JobLocation = 3
        case FeeDetails = 4
        case Total = 5
        case CancellationFeeDetails = 6
        case CancellationTotalBill = 7
        case PaymentMethod = 8
        case CancellationReason = 9
        
        static var count: Int { return BookingDetailsSections.CancellationReason.rawValue + 1}
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return BookingDetailsSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : BookingDetailsSections = BookingDetailsSections(rawValue: section)!
        
        switch sectionType {
            
            case .BookingStatus, .TotalBillAmount, .JobLocation:
                
                return 1
            
            case .PaymentMethod:
                
                return 1
            
            case .NotaryDetails:
                
                if bookingDetailsModel.bookingModel == .OnDemand {
                    
                    if bookingDetailsModel.bookingStatus == Booking_Status.Pending.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.IgnoreOrExpire.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.Requested.rawValue  {
                        
                        return 0
                        
                    } else if bookingDetailsModel.providerName.length == 0 {
                        
                        return 0
                        
                    } else {
                       
                        return 1
                    }
                    
                } else {
                    
                    return 1
                }
            
            case .FeeDetails:
                
                return bookingDetailsModel.arrayOfFees.count
            
            case .Total:
                
                if bookingDetailsModel.bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                    
                    return 1
                    
                }
                return 0
            
            case .CancellationFeeDetails, .CancellationTotalBill:
            
                if bookingDetailsModel.bookingStatus == Booking_Status.CancelByCustomer.rawValue {
                    
                    return 1
                    
                }
                return 0
            
            case .CancellationReason:
                
                if bookingDetailsModel.bookingStatus == Booking_Status.CancelByCustomer.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.CancelByProvider.rawValue {
                    
                    return 1
                    
                }
                return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionType : BookingDetailsSections = BookingDetailsSections(rawValue: section)!
        
        let headerCell:BDHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! BDHeaderTableViewCell
        
        switch sectionType {
            
            case .BookingStatus, .CancellationFeeDetails, .CancellationTotalBill, .Total, .CancellationReason:
                
                let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                return view
            
            case .NotaryDetails:
                
                if bookingDetailsModel.bookingModel == .OnDemand {
                    
                    if bookingDetailsModel.bookingStatus == Booking_Status.Pending.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.IgnoreOrExpire.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.Requested.rawValue  {
                        
                        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                        return view
                        
                    } else if bookingDetailsModel.providerName.length == 0 {
                        
                        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                        return view
                        
                    } else {
                        
                        headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.NotaryDetails
                        return headerCell.contentView
                    }
                    
                } else {
                    
                    headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.NotaryDetails
                    return headerCell.contentView
                }
            
            case .PaymentMethod:
                
                headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.PaymentMethod
                return headerCell.contentView
            
            case .FeeDetails:
                
                if bookingDetailsModel.bookingStatus == Booking_Status.Raiseinvoice.rawValue {
                
                    headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.YourPaymentBreakDown
                    
                } else {
                    
                    headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.RequestedServices
                }
                return headerCell.contentView
            
            case .TotalBillAmount:
                
                headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.TotalBillAmount
                return headerCell.contentView
            
            case .JobLocation:
                headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.JobLocation
                return headerCell.contentView
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let sectionType : BookingDetailsSections = BookingDetailsSections(rawValue: section)!
        
        let footerCell:BDFooterTableViewCell = tableView.dequeueReusableCell(withIdentifier: "footerCell") as! BDFooterTableViewCell
        
        switch sectionType {
            
            case .NotaryDetails:
                
                if bookingDetailsModel.bookingModel == .OnDemand {
                    
                    if bookingDetailsModel.bookingStatus == Booking_Status.Pending.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.IgnoreOrExpire.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.Requested.rawValue  {
                        
                        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                        return view
                        
                    } else if bookingDetailsModel.providerName.length == 0 {
                        
                        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                        return view
                        
                    } else {
                        
                        return footerCell.contentView
                    }
                    
                } else {
                    
                    return footerCell.contentView
                }
            
            case .BookingStatus, .TotalBillAmount, .JobLocation:
                
                return footerCell.contentView
            
            
            case .PaymentMethod, .Total, .CancellationTotalBill, .CancellationReason:
            
                let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                return view
            
            case .FeeDetails:
                
                if bookingDetailsModel.bookingStatus == Booking_Status.CancelByCustomer.rawValue {
                    
                    return footerCell.contentView
                }
                
                let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                return view
            
            
            case .CancellationFeeDetails:
                
                if bookingDetailsModel.bookingStatus == Booking_Status.CancelByCustomer.rawValue {
                    
                    return footerCell.contentView
                }
                
                let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                return view
   
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let sectionType : BookingDetailsSections = BookingDetailsSections(rawValue: section)!
        
        switch sectionType {
            
            case .BookingStatus, .CancellationFeeDetails, .CancellationTotalBill, .Total, .CancellationReason:
            
                return 0.001
            
            case .NotaryDetails:
                
                if bookingDetailsModel.bookingModel == .OnDemand {
                    
                    if bookingDetailsModel.bookingStatus == Booking_Status.Pending.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.IgnoreOrExpire.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.Requested.rawValue  {
                        
                        return 0.001
                        
                    } else if bookingDetailsModel.providerName.length == 0 {
                        
                        return 0.001
                        
                    } else {
                        
                        return 30.0
                    }
                    
                } else {
                    
                    return 30.0
                }
            
            
            case .TotalBillAmount, .JobLocation,.PaymentMethod, .FeeDetails:
                
                return 30.0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        let sectionType : BookingDetailsSections = BookingDetailsSections(rawValue: section)!
        
        switch sectionType {
            
            case .NotaryDetails:
            
                if bookingDetailsModel.bookingModel == .OnDemand {
                    
                    if bookingDetailsModel.bookingStatus == Booking_Status.Pending.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.IgnoreOrExpire.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.Requested.rawValue  {
                        
                        return 0.001
                        
                    } else {
                        
                        return 20.0
                    }
                    
                } else if bookingDetailsModel.providerName.length == 0 {
                    
                    return 0.001
                    
                } else {
                    
                    return 20.0
                }
            
            case .BookingStatus, .TotalBillAmount, .JobLocation:
                
                    return 20.0
            
            case .PaymentMethod, .Total, .CancellationTotalBill, .CancellationReason:
                
                    return 0.001
            
            case .FeeDetails:
                
                if bookingDetailsModel.bookingStatus == Booking_Status.CancelByCustomer.rawValue {
                    
                    return 20.0
                }
                
                return 0.001
            
            case .CancellationFeeDetails:
                
                if bookingDetailsModel.bookingStatus == Booking_Status.CancelByCustomer.rawValue {
                    
                    return 20.0
                }
                
                return 0.001
  
        }
  
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType : BookingDetailsSections = BookingDetailsSections(rawValue: indexPath.section)!
        
        switch sectionType {
                
            case .BookingStatus:
                
                if bookingStatusCell == nil {
                    
                    bookingStatusCell = tableView.dequeueReusableCell(withIdentifier: "bookingStatusCell", for: indexPath) as? BDBookingStatusTableViewCell
                    
                    if bookingDetailsModel.bookingStatus == Booking_Status.Pending.rawValue {
                        
                        bookingStatusCell.statusLabel.text = ALERTS.BOOKING_FLOW.PendingStatusMessage
                        
                    } else {
                        
                        bookingStatusCell.statusLabel.text = bookingDetailsModel.bookingStatusMessage.uppercased()
                    }
                }
                
                return bookingStatusCell
                
            case .TotalBillAmount:
                
                if amountDetailsCell == nil {
                    
                    amountDetailsCell = tableView.dequeueReusableCell(withIdentifier: "amountDetailsCell", for: indexPath) as? BDAmountDetailsTableViewCell
                    
                    let totalValue =  bookingDetailsModel.bookingTotalAmount
                    
                    
                    amountDetailsCell.amountLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",totalValue), currencySymbol: bookingDetailsModel.currencySymbol)
                    
                    
                    let startDate = Date(timeIntervalSince1970: TimeInterval(bookingDetailsModel.bookingStartTime))
                    
                    let dateFormat = DateFormatter.initTimeZoneDateFormat()
                    
                    dateFormat.dateFormat = "d MMM yyyy, hh:mm a"
                    amountDetailsCell.dateLabel.text = dateFormat.string(from: startDate)
                    
                    if bookingDetailsModel.bookingStatus == Booking_Status.Pending.rawValue {
                        
                        amountDetailsCell.remainingTimerBackView.isHidden = false
                        amountDetailsCell.remainingTimeView.isHidden = false
                        amountDetailsCell.showBookingTimerDetails(bookingDetail: bookingDetailsModel)
                        
                    } else {
                        
                        amountDetailsCell.remainingTimerBackView.isHidden = true
                        amountDetailsCell.remainingTimeView.isHidden = true
                        amountDetailsCell.timer.invalidate()

                    }
                    
                }
                
                return amountDetailsCell
            
            case .NotaryDetails:
                
                if (bookingDetailsModel.bookingStatus == Booking_Status.Raiseinvoice.rawValue) {
                        
                    if notaryDetailsCell == nil {
                        
                        notaryDetailsCell = tableView.dequeueReusableCell(withIdentifier: "notaryDetailsCell", for: indexPath) as? BDNotaryDetailsTableViewCell
                        
                        notaryDetailsCell.showNotaryDetails(bookingDetails: bookingDetailsModel)
                    }
                    
                    return notaryDetailsCell
                        
                } else {
                    
                    if notaryRequiredDetailsCell == nil {
                        
                        notaryRequiredDetailsCell = tableView.dequeueReusableCell(withIdentifier: "notaryRequiredDetailsCell", for: indexPath) as? BDNotaryRequiredDetailsTableViewCell
                        
                        notaryRequiredDetailsCell.showNotaryDetails(bookingDetails: bookingDetailsModel)
                    }
                    
                    return notaryRequiredDetailsCell
                        
                    
                }
            
            case .JobLocation:
                
                if addressDetailsCell == nil {
                    
                    addressDetailsCell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as? BDAddressTableViewCell
                    
                    addressDetailsCell.addressLabel.text = bookingDetailsModel.address1
                }
                
                return addressDetailsCell
            
            case .PaymentMethod:
                
                if bookingDetailsModel.isPaidByWallet {
                    
                    if twoPaymentDetailsCell == nil {
                        
                        twoPaymentDetailsCell = tableView.dequeueReusableCell(withIdentifier: "twoPaymentMethodCell", for: indexPath) as? BDTwoPaymentDetailsTableViewCell
                        
                        twoPaymentDetailsCell.showPaymentMethodDetails(bookingDetails: bookingDetailsModel)
                    }
                    
                    return twoPaymentDetailsCell
                    
                } else {
                    
                    if paymentDetailsCell == nil {
                        
                        paymentDetailsCell = tableView.dequeueReusableCell(withIdentifier: "paymentMethodCell", for: indexPath) as? BDPaymentDetailsTableViewCell
                        
                        paymentDetailsCell.showPaymentMethodDetails(bookingDetails: bookingDetailsModel)
                        
                    }
                    
                    return paymentDetailsCell
                }
            
            case .CancellationReason:
                
                if cancellationReasonCell == nil {
                    
                    cancellationReasonCell = tableView.dequeueReusableCell(withIdentifier: "cancellationReasonCell", for: indexPath) as? BDCancellationReasonTableViewCell
                    
                    cancellationReasonCell.cancellationReasonLabel.text = bookingDetailsModel.cancellationReason
                }
                
                return cancellationReasonCell
            
            case .FeeDetails:
                
                let paymentBreakdownCell:CBPaymentBreakDownCell = tableView.dequeueReusableCell(withIdentifier: "paymentBreakdownCell", for: indexPath) as! CBPaymentBreakDownCell
                
                paymentBreakdownCell.titleLabel.text = (bookingDetailsModel.arrayOfFees[indexPath.row].keys.first)!
                
                paymentBreakdownCell.amountLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f", (bookingDetailsModel.arrayOfFees[indexPath.row].values.first)! as! CVarArg), currencySymbol: bookingDetailsModel.currencySymbol)
                
                return paymentBreakdownCell
            
            case .Total:
            
                let paymentTotalCell:CBTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "paymentTotalCell", for: indexPath) as! CBTotalTableViewCell
                
                var totalValue =  bookingDetailsModel.bookingTotalAmount
                
                if bookingDetailsModel.bookingStatus == Booking_Status.CancelByCustomer.rawValue {
                    
                    totalValue = 0.0
                    paymentTotalCell.totalTitleLabel.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                    paymentTotalCell.totalValueLabel.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                    
                } else {
                    
                    paymentTotalCell.totalTitleLabel.textColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
                    paymentTotalCell.totalValueLabel.textColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
                }
                
                paymentTotalCell.totalTitleLabel.text = ALERTS.BOOKING_FLOW.FEES_TITLES.Total
                    
                paymentTotalCell.totalValueLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",totalValue), currencySymbol: bookingDetailsModel.currencySymbol)
                
                return paymentTotalCell
            
            case .CancellationFeeDetails:
            
                let paymentBreakdownCell:CBPaymentBreakDownCell = tableView.dequeueReusableCell(withIdentifier: "paymentBreakdownCell", for: indexPath) as! CBPaymentBreakDownCell
                
                paymentBreakdownCell.titleLabel.text = ALERTS.BOOKING_FLOW.FEES_TITLES.CancellationFee
                
                
                paymentBreakdownCell.amountLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",bookingDetailsModel.cancellationFee), currencySymbol: bookingDetailsModel.currencySymbol)
                
                return paymentBreakdownCell
            
            case .CancellationTotalBill:
            
                let paymentTotalCell:CBTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "paymentTotalCell", for: indexPath) as! CBTotalTableViewCell
                
                
                paymentTotalCell.totalTitleLabel.text = ALERTS.BOOKING_FLOW.FEES_TITLES.BillTotal
                
                paymentTotalCell.totalValueLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f",bookingDetailsModel.bookingTotalAmount), currencySymbol: bookingDetailsModel.currencySymbol)
                
                return paymentTotalCell
            
        }
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let sectionType : BookingDetailsSections = BookingDetailsSections(rawValue: indexPath.section)!
        
        switch sectionType {
                
            case .BookingStatus:
                
                return 40
                
            case .TotalBillAmount:
                
                return 90
                
            case .NotaryDetails:
                
                if (bookingDetailsModel.bookingStatus == Booking_Status.Raiseinvoice.rawValue) {
                    
                    return 100
                }
                return 70
                
            case .JobLocation, .CancellationReason:
                return UITableViewAutomaticDimension
                
            case .PaymentMethod:
                
                if bookingDetailsModel.isPaidByWallet {
                    
                    return 120
                    
                } else {
                    
                    if bookingDetailsModel.paymentTypeValue == 2 {
                        
                        return 80
                        
                    } else {
                        
                        return 40
                    }
                    
                }
                
            case .FeeDetails, .Total, .CancellationFeeDetails, .CancellationTotalBill:
                
                return 30
            
        }
        
    }
    
}
