//
//  BookingDetailsViewController.swift
//  Notary
//
//  Created by Rahul Sharma on 22/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class BookingDetailsViewController:UIViewController {
    
    @IBOutlet weak var navigationLeftButton: UIButton!
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    
    
    var bookingDetailsModel:BookingDetailsModel!
    
    var cancelBookingScreen:CancelBookingScreen!
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    var bookingStatusCell:BDBookingStatusTableViewCell!
    var amountDetailsCell:BDAmountDetailsTableViewCell!
    var notaryDetailsCell:BDNotaryDetailsTableViewCell!
    var addressDetailsCell:BDAddressTableViewCell!
    var paymentDetailsCell:BDPaymentDetailsTableViewCell!
    var twoPaymentDetailsCell:BDTwoPaymentDetailsTableViewCell!
    var notaryRequiredDetailsCell:BDNotaryRequiredDetailsTableViewCell!
    var cancellationReasonCell:BDCancellationReasonTableViewCell!

    
    var isFromPendingBooking = false
    
    
    // Shared instance object for gettting the singleton object
    static var obj:BookingDetailsViewController? = nil
    
    class func sharedInstance() -> BookingDetailsViewController {
        
        return obj!
    }
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 250
//        self.tableView.estimatedSectionHeaderHeight = 40
//        self.tableView.estimatedSectionFooterHeight = 20

        BookingDetailsViewController.obj = self
        showBookingDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupGestureRecognizer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if amountDetailsCell != nil {
            
            amountDetailsCell.timer.invalidate()
        }
    }
    
    func showBookingDetails() {
        
        if isFromPendingBooking {
            
            if bookingDetailsModel.bookingStatus == Booking_Status.Pending.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.UnassignedDispatch.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.Requested.rawValue {
                
                self.cancelButton.isHidden = false
                
            } else {
                
                self.cancelButton.isHidden = true
            }
            
        } else {
            
            self.cancelButton.isHidden = true
        }
        
        
        
        self.title = ALERTS.BOOKING_FLOW.EventID + " : \(bookingDetailsModel.bookingId)"
        self.tableView.reloadData()
    }
    
    
    @IBAction func navigationLeftButtonAction(_ sender: Any) {
        
        if CancelBookingScreen.share != nil {
            
            CancelBookingScreen.sharedInstance.closeButtonAction(CancelBookingScreen.sharedInstance.closeButton)
            
            if CancelBookingScreen.sharedInstance.newCancelFeeAlertWindow != nil && CancelBookingScreen.sharedInstance.alertController != nil  {
                
                CancelBookingScreen.sharedInstance.alertController.dismiss(animated: false, completion: nil)
            }
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        cancelBookingScreen = CancelBookingScreen.sharedInstance
        
        cancelBookingScreen.bookingID = bookingDetailsModel.bookingId
        
        WINDOW_DELEGATE??.addSubview(cancelBookingScreen)
        self.cancelBookingScreen.isHidden = true
        
        self.cancelBookingScreen.getCancelReasonsAPI()
        
        /*WINDOW_DELEGATE??.addSubview(cancelBookingScreen)
        
        cancelBookingScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       options: UIViewAnimationOptions.beginFromCurrentState,
                       animations: {
                        
                        self.cancelBookingScreen.topView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
                        
        }) { (finished) in
            
            self.cancelBookingScreen.getCancelReasonsAPI()
            
        }*/
    }
    
}

extension BookingDetailsViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

extension BookingDetailsViewController {
    
    func setupGestureRecognizer() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            
            self.navigationController?.delegate = self
            
            if MyEventViewController.obj != nil && (bookingDetailsModel.bookingStatus == Booking_Status.Pending.rawValue || bookingDetailsModel.bookingStatus == Booking_Status.Requested.rawValue) {
                
                MyEventViewController.sharedInstance().gotoPastBooking = false
                MyEventViewController.sharedInstance().gotoPendingBooking = true
                MyEventViewController.sharedInstance().gotoUpcomingBooking = false
            }
            
            self.navigationController?.popViewController(animated: true)
            //            self.navigationLeftButtonAction(self.navigationLeftButton)
            
        case .changed:
            
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
}

