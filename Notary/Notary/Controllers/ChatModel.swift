//
//  ChatModel.swift
//  LiveMPro
//
//  Created by Vengababu Maparthi on 09/12/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa

class Message: NSObject {
    
    var owner: MessageOwner
    var type: MessageType
    var content: Any
    var timestamp: Int64 = 0
    var isRead: Bool
    
    private var toID: String?
    private var fromID: String?
    
    //MARK: Inits
    init(type: MessageType, content: Any, owner: MessageOwner, timestamp: Int64, isRead: Bool) {
        self.type = type
        self.content = content
        self.owner = owner
        self.timestamp = timestamp
        self.isRead = isRead
    }
}

class Chat: NSObject {
    
    let disposebag = DisposeBag()
    var messageDict = [Message]()
    
    func getTheChatHistory(bookingID:String,indexVal:String,completionHandler:@escaping (Bool,[Message]) -> ()) {

        let rxApiCall = ChatAPI()
        rxApiCall.getTheChatMessages(bookingID: bookingID + "/" + indexVal)
        rxApiCall.getchat_Response
            .subscribe(onNext: {response in
                switch response.httpStatusCode{
                case 200:
                    if let dict = response.data["data"] as? [[String:Any]]  {
                        if indexVal == "0"{
                        ChatCouchDBManager.sharedInstance.updateChatDetailsToCouchDBDocument(arrayOfMessages: dict, bookingId: bookingID)
                        }
                        completionHandler(true,self.updateTheChatData(chatDict: dict ))
                    }else{
                        completionHandler(false, self.messageDict)
                    }
                    break
                default:
                    break
                }
            }, onError: { error in
                completionHandler(false, self.messageDict)
                Helper.alertVC(title: ALERTS.Error, message: error.localizedDescription)
            }).disposed(by: disposebag)
    }
    
    func updateTheChatData(chatDict:[[String:Any]]) -> [Message] {
        
        messageDict = [Message]()
        
        for chat in chatDict {
            
            switch chat["type"] as!  NSNumber{
                
                case 1://Text message
                    
                    if String(describing:chat["fromID"]!) == Utility.customerId {
                        messageDict.append(Message.init(type: .text, content: chat["content"] as Any, owner: .sender, timestamp: chat["timestamp"] as! Int64, isRead: true))
                    }else{
                        messageDict.append(Message.init(type: .text, content: chat["content"] as Any, owner: .receiver, timestamp: chat["timestamp"] as! Int64, isRead: true))
                    }
                    
                case 2://Photo message
                    
                    if String(describing:chat["fromID"]!) == Utility.customerId {
                        messageDict.append(Message.init(type: .photo, content: chat["content"] as Any, owner: .sender, timestamp: chat["timestamp"] as! Int64, isRead: true))
                    }else{
                        messageDict.append(Message.init(type: .photo, content: chat["content"] as Any, owner: .receiver, timestamp: chat["timestamp"] as! Int64, isRead: true))
                    }
                    break
                    
                default:
                    break
                
            }
        }
        
        return messageDict
    }
    
    func updateToServerchat(type:MessageType,
                            timeStamp:Int64,
                            content:String,
                            bookingId:String,
                            musicianId:String,
                            completionHandler:@escaping (Bool) ->()){
        
        
        
        let rxApiCall = ChatAPI()
        
        let chatSendMessageModel = ChatMessageSendModel()
        
        chatSendMessageModel.bookingId = bookingId
        chatSendMessageModel.messageType = type
        chatSendMessageModel.messageTimestamp = timeStamp
        chatSendMessageModel.musicianId = musicianId
        chatSendMessageModel.messageContent = content
        
        var typeID = 0
        
        switch chatSendMessageModel.messageType {
            
        case .photo:
            
            typeID = 2
            break
            
        case .text:
            
            typeID = 1
            break
            
        default:
            
            typeID = 3
            break
        }
        
        let messageDict : [String : Any] = [
            
            "type": typeID,
            "timestamp":chatSendMessageModel.messageTimestamp,
            "content" :chatSendMessageModel.messageContent,
            "fromID":Utility.customerId,
            "bid":chatSendMessageModel.bookingId,
            "targetId":chatSendMessageModel.musicianId
        ]
        
        
        rxApiCall.chatSend_Response
            .subscribe(onNext: {response in
             
                switch response.httpStatusCode{
                    
                    case 200:
                        ChatCouchDBManager.sharedInstance.updateEachChatDetailsToCouchDBDocument(message: messageDict, bookingId: bookingId)

                        completionHandler(true)
                        break
                    
                    default:
                           completionHandler(false)
                        break
                    
                }
                
            }, onError: {  error in
                
                
            }).disposed(by: disposebag)
        
        rxApiCall.sendTheChatMessage(chatSendMessageModel: chatSendMessageModel)
    }
}



