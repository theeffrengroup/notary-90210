//
//  ShareViewControlerAPICallsExt.swift
//  Notary
//
//  Created by 3Embed on 23/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension ShareViewController {
    
    func getReferralCode() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        shareViewModel.getReferralCodeAPICall{ (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.GetReferralCode)
        }
        
    }
    
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode
        {
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    
                    var progressMessage = PROGRESS_MESSAGE.Loading
                    
                    switch requestType {
                        
                        case .GetReferralCode:
                            
                            progressMessage = PROGRESS_MESSAGE.Loading
                        
                        default:
                            
                            break
                    }
                    
                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                    
                }
                
                break
            
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                
                break
            
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType
                {
                    case RequestType.GetReferralCode:
                        
                        if let dataRes = dataResponse as? [String:Any] {
                            
                            if let referralCode = dataRes["referralCode"] as? String {
                                
                                codeLabel.text = referralCode
                                
                                AppDelegate().defaults.set(referralCode, forKey: USER_DEFAULTS.USER.REFERRAL_CODE)

                                AppDelegate().defaults.synchronize()
                            }
                            
                        }
                        
                        break
                    
                    default:
                        break
                }
            
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                }
                
                break
        }
        
    }
 
}
