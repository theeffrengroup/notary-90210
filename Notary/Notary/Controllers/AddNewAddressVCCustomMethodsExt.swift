//
//  AddNewAddressVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension AddNewAddressViewController {
    
    func setInitialAddressviewProperties() {
        
        if isForEditing {
            
            isAddressForEditing = true
            
            switch addressDetails.tagAddress {
            case "home":
                
                changeButtonState(homeButton)
                selectedAddresstype = homeButton.tag
                showNormalAddressTagFrame()
                
            case "office":
                
                changeButtonState(workButton)
                selectedAddresstype = workButton.tag
                showNormalAddressTagFrame()
                
                
            default:
                
                changeButtonState(otherButton)
                selectedAddresstype = otherButton.tag
                otherTextField.text = addressDetails.tagAddress
                showOtherAddressTagFrame()
                
            }
            
            
        } else {
            
            changeButtonState(homeButton)
            selectedAddresstype = homeButton.tag
            showNormalAddressTagFrame()
        }
        
        
    }
    
    
    func updatedAddressFromMap(){
        
        switch selectedAddresstype {
            
        case 1,2:
            
            showNormalAddressTagFrame()
            break
            
        case 3:
            
            showOtherAddressTagFrame()
            break
            
        default:
            break
        }
    }
    
    func showOtherAddressTagFrame() {
        
        DispatchQueue.main.async(execute: {() -> Void in
            
            let addressLabelHeight:CGFloat = Helper.measureHeightLabel(self.addressLabel, width: SCREEN_WIDTH! - 80)
            self.topAddressViewHeightConstraint.constant = addressLabelHeight + 75 + 25
            
            self.view.layoutIfNeeded()
            
            self.otherTextfieldBackViewHeightConstraint.constant = 25
            self.otherTextFieldBackView.isHidden = false
            
            self.view.layoutIfNeeded()
        })
    }
    
    func showNormalAddressTagFrame() {
        
        DispatchQueue.main.async(execute: {() -> Void in
            
            let addressLabelHeight:CGFloat = Helper.measureHeightLabel(self.addressLabel, width: SCREEN_WIDTH! - 80)
            self.topAddressViewHeightConstraint.constant = addressLabelHeight + 75
            
            self.view.layoutIfNeeded()
            
            self.otherTextfieldBackViewHeightConstraint.constant = 0
            self.otherTextFieldBackView.isHidden = true
            
            self.view.layoutIfNeeded()
        })
    }
    
    func changeButtonState(_ sender:Any) {
        
        let mBtn: UIButton? = (sender as? UIButton)
        for button: UIView in (mBtn?.superview?.subviews)! {
            
            if (button is UIButton) {
                
                (button as? UIButton)?.isSelected = false
            }
        }
        
        mBtn?.isSelected = true
        
    }
    
    func showInitialSpringAnimation() {
        
        self.topAddressView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 3,
                       options: [],
                       animations: {
                        
                        self.topAddressView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        
        }, completion: { (completed) in
            
            self.saveButton.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            
            UIView.animate(withDuration: 0.8,
                           delay: 0.2,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 3, options: [], animations: {
                            
                            self.saveButton.transform = .identity
                            
            })
            
            
        })
        
    }
    
}
