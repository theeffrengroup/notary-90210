//
//  SearchLocationVCCustomMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension SearchLocationViewController {
    
    func loadsManageAndPreviouslySelectedAddress() {
        
        previouslySelectedAddress = []
        managedAddress = []
        
        arrayOfCurrentSearchResult = []
        
        if !Utility.manageAddressDocId.isEmpty {
            
            managedAddress = AddressCouchDBManager.sharedInstance.getManageAddressDetailsFromCouchDB()
            previouslySelectedAddress = AddressCouchDBManager.sharedInstance.getSearchAddressDetailsFromCouchDB()
        }
        
        tableView.reloadData()
    }
    
    
}
