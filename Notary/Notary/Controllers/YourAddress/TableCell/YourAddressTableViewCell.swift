//
//  YourAddressTableViewCell.swift
//  LiveM
//
//  Created by Rahul Sharma on 10/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class YourAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var addressImage: UIImageView!
    
    @IBOutlet weak var addressTypeLabel: UILabel!
    @IBOutlet weak var fullAddressLabel: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
