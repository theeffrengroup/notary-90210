//
//  HomeListTableViewCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 05/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

class HomeListTableViewCell: UITableViewCell {

    // MARK: - Outlets -
    // Image View

    @IBOutlet weak var topView: UIViewCustom!
    
    @IBOutlet var defaultImageView: UIImageView!
    
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    // Array Of star Images
   
    @IBOutlet weak var collectionBackView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // label
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var pageController: UIPageControl!
    
    
    
    var musicianDetailsModel:MusicianDetailsModel!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension HomeListTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if musicianDetailsModel.workImagesArray.count > 0 {
            
            self.collectionView.isHidden = false
            self.defaultImageView.isHidden = true
            self.pageController.numberOfPages = musicianDetailsModel.workImagesArray.count
            self.pageController.currentPage = 0
            
            return 1
            
        } else {
            
            self.collectionView.isHidden = true
            self.defaultImageView.isHidden = false
            self.pageController.numberOfPages = 0
            
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return musicianDetailsModel.workImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let workImageCell:HomeListWorkImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "workImageCell", for: indexPath) as! HomeListWorkImageCollectionViewCell
        
        if musicianDetailsModel.workImagesArray[indexPath.row].length > 0 {
            
            workImageCell.activityIndicator.startAnimating()
            
            workImageCell.workImageView.kf.setImage(with: URL(string: musicianDetailsModel.workImagesArray[indexPath.row]),
                                          placeholder:#imageLiteral(resourceName: "defaultworkimage"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
            },
                                          completionHandler: { image, error, cacheType, imageURL in
                                            
                                            workImageCell.activityIndicator.stopAnimating()
            })
                
            
        } else {
    
            workImageCell.workImageView.image = #imageLiteral(resourceName: "defaultworkimage")
        }
        
        return workImageCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sizeOfEachCell:CGSize = CGSize.init(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
        
        return sizeOfEachCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == self.collectionView {
            
            if !decelerate {
                
                self.changePageController()
            }
            
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.collectionView {
            
            self.changePageController()
        }
        
    }

    func changePageController() {
        
        var visibleRect = CGRect()
        
        visibleRect.origin = self.collectionView.contentOffset
        visibleRect.size = self.collectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        if let visibleIndexPath: IndexPath = self.collectionView.indexPathForItem(at: visiblePoint) {
            
            self.pageController.currentPage = visibleIndexPath.row
        }
        
    }
    
}
