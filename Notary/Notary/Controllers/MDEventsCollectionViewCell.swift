//
//  MDEventsCollectionViewCell.swift
//  LiveM
//
//  Created by Apple on 08/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class MDEventsCollectionViewCell:UICollectionViewCell {
    
    @IBOutlet var eventImageView: UIButton!
    
    @IBOutlet var eventNameLabel: UILabel!
    
    @IBOutlet var selectedDivider: UIView!
    
}
