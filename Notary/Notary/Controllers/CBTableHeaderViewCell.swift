//
//  CBTableHeaderViewCell.swift
//  Notary
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class CBTableHeaderViewCell:UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
}
