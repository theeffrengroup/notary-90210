//
//  HomeVCLocationMethodsExt.swift
//  LiveM
//
//  Created by Rahul Sharma on 20/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

extension HomeScreenViewController:LocationManagerDelegate {
    
    func didUpdateLocation(location: LocationManager) {
        
        currentLat = location.latitute
        currentLong = location.longitude
    
        appointmentLocationModel.pickupLatitude = locationObj.latitute
        appointmentLocationModel.pickupLongitude = locationObj.longitude
        
        self.setTimeZonesToCalendar()
        
        addressLabel.text = locationObj.address
        
        musiciansListManager.checkZoneisChanged(currentLat: appointmentLocationModel.pickupLatitude, currentLong: appointmentLocationModel.pickupLongitude)
        
        appointmentLocationModel.pickupAddress = addressLabel.text!
    
        if appointmentLocationModel.bookingType == BookingType.Schedule {
            
            musiciansListManager.publish(isIinitial: false)

        } else {
            
            musiciansListManager.publish(isIinitial: true)
        }
        
    
    
//        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(location.latitute),
//                                              longitude: CLLocationDegrees(location.longitude),
//                                              zoom: MAP_ZOOM_LEVEL)
//        self.mapView.animate(to: camera)
        
        let camera = GMSCameraUpdate.setTarget(CLLocationCoordinate2D.init(latitude: CLLocationDegrees(location.latitute), longitude: CLLocationDegrees(location.longitude)), zoom: MAP_ZOOM_LEVEL)
        
        
        self.mapView.animate(with: camera)
        
        self.perform(#selector(self.showCurrentLocation), with: nil, afterDelay: 0.6)
        
    }
    
    func didFailToUpdateLocation() {
        
    }
    
    func didChangeAuthorization(authorized: Bool) {
        
    }
    
    
    /// Method to show current location in MapView
    @objc func showCurrentLocation() {
        
        let location = self.mapView.myLocation
        
        if (location != nil) {
            
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees((location?.coordinate.latitude)!),
                                                  longitude: CLLocationDegrees((location?.coordinate.longitude)!),
                                                  zoom: MAP_ZOOM_LEVEL)
            self.mapView.animate(to: camera)
            
        }
        
    }
    
    
    /// Get Location Details From Lat & Long
    ///
    /// - Parameter coordinate: current map position lat & Long
    func getLocationDetails(_ coordinate: CLLocationCoordinate2D) {
        
        var positionChanged = false
        
        if appointmentLocationModel.pickupLatitude != coordinate.latitude || appointmentLocationModel.pickupLongitude != coordinate.longitude {
            
            positionChanged = true
        }
        
        appointmentLocationModel.pickupLatitude = coordinate.latitude
        appointmentLocationModel.pickupLongitude =  coordinate.longitude
        
        self.setTimeZonesToCalendar()
        
        if positionChanged {
            
            if appointmentLocationModel.bookingType == BookingType.Schedule {
                
                musiciansListManager.publish(isIinitial: false)
                
            } else {
                
                musiciansListManager.publish(isIinitial: true)
            }
        }
       
        self.musiciansListManager.checkZoneisChanged(currentLat: appointmentLocationModel.pickupLatitude, currentLong: appointmentLocationModel.pickupLongitude)

        GMSGeocoder().reverseGeocodeCoordinate(coordinate, completionHandler: { (gmsAddressResponse, error) in
            
            if let addressDetails = gmsAddressResponse?.firstResult() {
                
                // Address Details
                DDLogVerbose("Add New Address Address Details:\(addressDetails)")
                
                
                // Current Address
                if let currentAddress = addressDetails.lines {
                    
        
                    DDLogVerbose("Current Address: \(currentAddress)")
                    
                    var address = (currentAddress.joined(separator: ", "))
                    
                    if address.hasPrefix(", ") {
                        
                        address = address.substring(2)
                    }
                    
                    if address.hasSuffix(", ") {
                        
                        let endIndex = address.index(address.endIndex, offsetBy: -2)
                        
                        address = String(address[..<endIndex])
                    }
                    
                    self.addressLabel.text = address
                    self.appointmentLocationModel.pickupAddress = address
                    
                }
                
            } else {
                
                DDLogError("Home Class Address Fetch Error: \(String(describing: error?.localizedDescription))")
            }
            
        })

    }
    
}



extension HomeScreenViewController:SearchAddressDelegate {
    
    func searchAddressDelegateMethod(_ addressModel:AddressModel) {
        
        addressLabel.text = GenericUtility.strForObj(object:addressModel.fullAddress)
        appointmentLocationModel.pickupAddress = addressLabel.text!
        
        if appointmentLocationModel.pickupLatitude != Double(addressModel.latitude)! {
            
            needToShowProgress = true
            self.musiciansListManager.checkZoneisChanged(currentLat: Double(addressModel.latitude)!, currentLong: Double(addressModel.longitude)!)
//            noArtistMessageToShow = true
            
            self.setTimeZonesToCalendar()
        }
        
        appointmentLocationModel.pickupLatitude = Double(addressModel.latitude)!
        appointmentLocationModel.pickupLongitude = Double(addressModel.longitude)!
        
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(appointmentLocationModel.pickupLatitude),
                                              longitude: CLLocationDegrees(appointmentLocationModel.pickupLongitude),
                                              zoom: MAP_ZOOM_LEVEL)
        self.mapView.animate(to: camera)
        
        CATransaction.commit()
        
        isAddressManuallyPicked = true
    }
    
    func searchAddressCurrentLocationButtonClicked() {
        
        if appointmentLocationModel.pickupLatitude != self.mapView.myLocation?.coordinate.latitude {
            
            self.setTimeZonesToCalendar()
            
            needToShowProgress = true
            self.musiciansListManager.checkZoneisChanged(currentLat:(self.mapView.myLocation?.coordinate.latitude)!,
                                                         currentLong: (self.mapView.myLocation?.coordinate.longitude)!)

//            noArtistMessageToShow = true
        }
        
        currentLocationAction(locationButtonOutlet)
    }
    
}
