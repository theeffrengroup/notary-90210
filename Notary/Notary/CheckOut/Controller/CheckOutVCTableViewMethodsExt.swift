//
//  CheckOutVCTableViewMethods.swift
//  Notary
//
//  Created by Rahul Sharma on 13/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension CheckOutViewController:UITableViewDataSource,UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arrayOfSubCategories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfSubCategories[section].arrayOfServices.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if arrayOfSubCategories[section].subCategoryName.length == 0 {
            
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
            return view
            
        } else {
            
            let headerCell:CheckOutVCTableSectionHeaderCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! CheckOutVCTableSectionHeaderCell //tableView.dequeueReusableCell(withIdentifier: "headerCell", for:IndexPath.init(row: section, section: 0)) as! CheckOutVCTableSectionHeaderCell
            
            if section == 0 {
                
                headerCell.topDivider.isHidden = true
                
            } else {
                
                headerCell.topDivider.isHidden = false
            }
            
            headerCell.headerTitleLabel.text = arrayOfSubCategories[section].subCategoryName
            headerCell.headerDescriptionLabel.text = arrayOfSubCategories[section].subCategoryDescription
            
            return headerCell.contentView

        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if arrayOfSubCategories[section].subCategoryName.length == 0 {
            
            return 0.01
            
        } else {
            
            return UITableViewAutomaticDimension
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let serviceCell:CheckOutVCServicesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for:indexPath) as! CheckOutVCServicesTableViewCell
        
        serviceCell.viewMoreButton.addTarget(self, action: #selector(viewMoreButtonAction(viewMoreButton:)), for: .touchUpInside)
        
        serviceCell.addServiceButton.addTarget(self, action: #selector(addOrRemoveServiceButtonAction(addOrRemoveServiceButton:)), for: .touchUpInside)
        
        serviceCell.plusQuantityButton.addTarget(self, action: #selector(quantityPlusButtonAction(quantityPlusButton:)), for: .touchUpInside)
        
        serviceCell.minusQuantityButton.addTarget(self, action: #selector(quantityMinusButtonAction(quantityMinusButton:)), for: .touchUpInside)
        
        var serviceModel = arrayOfSubCategories[indexPath.section].arrayOfServices[indexPath.row]
        
        for eachSelectedService in arrayOfSelectedServices {
            
            if eachSelectedService.serviceId == serviceModel.serviceId {
                
                serviceModel = eachSelectedService
                break
            }
        }
        
        serviceCell.showServiceDetails(serviceModel:serviceModel)
        
        if indexPath.section == arrayOfSubCategories.count - 1 && (indexPath.row == arrayOfSubCategories[arrayOfSubCategories.count - 1].arrayOfServices.count - 1) {
            
            serviceCell.divider.isHidden = false
            
        } else {
            
            serviceCell.divider.isHidden = true
        }

        return serviceCell
    }
    
}
