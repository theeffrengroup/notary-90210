//
//  CheckOutVCCustomMethodsExt.swift
//  Notary
//
//  Created by Rahul Sharma on 14/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension CheckOutViewController {
    
    func parseCartResponse() {
        
        
    }
    
    func showCartDetails() {
        
        self.tableView.isHidden = false
        self.checkoutBottomView.isHidden = false
        
        if arrayOfSelectedServices.count > 0 {
            
            if arrayOfSelectedServices.count == 1 {
                
                servicesCountLabel.text = "\(arrayOfSelectedServices.count) SERVICE"
                
            } else {
                
                servicesCountLabel.text = "\(arrayOfSelectedServices.count) SERVICES"
            }
            self.checkOutButton.isUserInteractionEnabled = true
            self.checkoutBottomView.backgroundColor = APP_COLOR
            
        } else {
            
            servicesCountLabel.text = "NO SERVICES"
            self.checkOutButton.isUserInteractionEnabled = false
            self.checkoutBottomView.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.totalCartAmount = 0.0

        }
        
        
        servicesTotalAmountLabel.text = Helper.getValueWithCurrencySymbol(data:String(format:"%.2f",self.totalCartAmount), currencySymbol: self.currencySymbol)

    }
    
    func getServiceDetailsFromServiceId(serviceId:String) -> ServicesModel {
        
        var serviceModel = ServicesModel()
        
        if let index = arrayOfServicesResponse.index(where: {$0["_id"] as? String == serviceId}) {
            
            serviceModel = ServicesModel.init(serviceDetails: arrayOfServicesResponse[index])
            return serviceModel
            
        }
        
        return serviceModel
    }
    
    func removeServiceDetailsUsingServiceId(serviceId:String) {
        
        if let index = arrayOfSelectedServices.index(where: {$0.serviceId == serviceId}) {
            
            arrayOfSelectedServices.remove(at: index)
        }
    }
}
