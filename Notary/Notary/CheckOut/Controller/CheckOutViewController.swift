//
//  CheckOutViewController.swift
//  Notary
//
//  Created by Rahul Sharma on 13/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class CheckOutViewController:UIViewController {
    
    
    @IBOutlet weak var navigationLeftButton: UIButton!
  
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var checkoutBottomView: UIView!
    
    @IBOutlet weak var servicesCountLabel: UILabel!
    
    @IBOutlet weak var servicesTotalAmountLabel: UILabel!
    
    @IBOutlet weak var checkOutButton: UIButton!
    
    
    var CBModel:ConfirmBookingModel = ConfirmBookingModel.sharedInstance()
    var checkOutViewModel = CheckOutViewModel()
    
    let acessClass = AccessTokenRefresh.sharedInstance()
    var apiTag:Int!
    
    var serviceIdToAddOrRemove = ""
    var quantityValue = 1
    
    var arrayOfSubCategories = [SubCategoriesModel]()
    var arrayOfSubCategoriesResponse = [[String:Any]]()
    var arrayOfServicesResponse = [[String:Any]]()

    var arrayOfSelectedServices = [ServicesModel]()

    var totalCartAmount = 0.0
    
    var serviceDescriptionView:ServiceDescriptionView!
    
    var currencySymbol = Utility.currencySymbol
    
    var cartId = ""
    
    // MARK: - Default Class Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getServicesAPI()
        self.tableView.estimatedRowHeight = 125
        self.tableView.estimatedSectionHeaderHeight = 100

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        acessClass.acessDelegate = self
        
        self.tableView.scrollRectToVisible(CGRect.init(x: 0, y: 0, width: 1, height: 1), animated: false)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        acessClass.acessDelegate = nil
        
    }
    
}

extension CheckOutViewController:AccessTokeDelegate {
    
    func recallApi() {
        
        switch apiTag {
            
            case RequestType.GetServices.rawValue:
                
                getServicesAPI()
                
                break
            
            case RequestType.GetCurrentCartDetails.rawValue:
                
                getCurrentCartDetails()
                
                break
            
            case RequestType.AddServiceToCart.rawValue:
                
                addServicesToCart()
                
                break
            
            case RequestType.RemoveServiceFromCart.rawValue:
                
                removeServicesFromCart()
                
                break
            
            default:
                break
        }
    }
    
}
