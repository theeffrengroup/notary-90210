//
//  WalletVCAPICallsExt.swift
//  Notary
//
//  Created by 3Embed on 19/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

extension WalletViewController {
    
    @objc func getTheUpdatedWalletDataAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        
        walletViewModel.getTheUpdatedWalletDataAPICall{ (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.GetWalletDetails)
        }
        
    }
    
    func rechargeWalletAPI() {
        
        if !NetworkHelper.sharedInstance.networkReachable() {
            
            Helper.alertVC(title: ALERTS.Oops, message: ALERTS.NoNetwork)
            return
        }
        
        walletViewModel.rechargeWalletAPICall(cardId: selectedCardModel.id,
                                              amount: self.enterAmt){ (statCode, errMsg, dataResp) in
            
            self.webServiceResponse(statusCode: statCode, errorMessage: errMsg, dataResponse: dataResp, requestType: RequestType.RechargeWallet)
        }
        
    }
    
    //MARK - Web Service Response -
    func webServiceResponse(statusCode:Int,errorMessage:String?,dataResponse:Any?, requestType:RequestType)
    {
        switch statusCode {
                
            case HTTPSResponseCodes.TokenExpired.rawValue:
                
                if let dataRes = dataResponse as? String {
                    
                    AppDelegate().defaults.set(dataRes, forKey: USER_DEFAULTS.TOKEN.ACCESS)
                    
                    self.apiTag = requestType.rawValue
                    
                    let progressMessage = PROGRESS_MESSAGE.Loading

                    self.acessClass.getAcessToken(progressMessage: progressMessage)
                    
                }
                
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Message, message: errorMessage!)
                }
                break
                
           
            case HTTPSResponseCodes.SuccessResponse.rawValue:
                
                switch requestType {
                    
                case RequestType.GetWalletDetails:
                    
                    if isFromAPI {
                        
                        Helper.alertVC(title: ALERTS.Message, message: "Wohooo...You have successfully recharged your wallet with " + Utility.currencySymbol + self.enterAmt + " balance.!")
                        
                        let indexPath = IndexPath(row: WalletRowType.Default.rawValue, section: WalletSectionType.cardDetails.rawValue)
                        if let cell = self.tableView.cellForRow(at: indexPath) as? WalletCardCell{
                            cell.amountTF.text = ""
                        }
                    }

                    let ud = UserDefaults.standard
                    
                    if let dict = dataResponse as? [String:Any] {

                        if let hardLimit  = dict["hardLimit"] as? Double {
                            
                            ud.set(String(format:"%.2f", hardLimit) , forKey: USER_DEFAULTS.WALLET.WalletHardLimit)
                            
                        } else {
                            
                             ud.set(GenericUtility.strForObj(object: dict["hardLimit"]) , forKey: USER_DEFAULTS.WALLET.WalletHardLimit)
                        }

                        if let softLimit  = dict["softLimit"] as? Double {
                            
                            ud.set(String(format:"%.2f", softLimit) , forKey: USER_DEFAULTS.WALLET.WalletSoftLimit)
                            
                        } else {
                            
                            ud.set(GenericUtility.strForObj(object: dict["softLimit"]) , forKey: USER_DEFAULTS.WALLET.WalletSoftLimit)
                        }

                        if let walletAmount  = dict["walletAmount"] as? String  {

                            ud.set(walletAmount , forKey: USER_DEFAULTS.WALLET.WalletTotalAmount)
                        }
                        
                        self.showWalletBalanceMessage()

                    }

                    ud.synchronize()
                    self.tableView.reloadData()
                
                    break
                    
                case .RechargeWallet:
                    
                    self.isFromAPI = true
                    self.perform(#selector(self.getTheUpdatedWalletDataAPI), with: nil, afterDelay: 0.4)
                    
                    break
                    
                
                    
                default:
                    
                    break
                }
                
                break
                
            default:
                
                if errorMessage != nil {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage!)
                }
                break
        }
        
    }
    
}
