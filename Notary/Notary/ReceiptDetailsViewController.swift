//
//  ReceiptDetailsViewController.swift
//  Notary
//
//  Created by Rahul Sharma on 30/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation

class ReceiptDetailsViewController: UIViewController {
    
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var bookingDetails:BookingDetailsModel!
    var arrayOfTableHeaderTitle:[String] = []
    
    var paymentDetailsCell:BDPaymentDetailsTableViewCell!
    var twoPaymentDetailsCell:BDTwoPaymentDetailsTableViewCell!
    var signatureCell:ReceiptVCSignatureTableCell!
    
    
    enum ReceiptDetailsSections: Int {
        
        case AddressDetails = 0
        case FeesDetails = 1
        case Total = 2
        case PaymentMethod = 3
        case Signature = 4
        
        static var count: Int { return ReceiptDetailsSections.Signature.rawValue + 1}
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 10
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedSectionHeaderHeight = 30
        
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    
    
    @IBAction func closeButtonAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension ReceiptDetailsViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return ReceiptDetailsSections.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : ReceiptDetailsSections = ReceiptDetailsSections(rawValue: section)!
        
        switch sectionType {
            
            case .AddressDetails:
                return 1
            
            case .FeesDetails:
                return bookingDetails.arrayOfFees.count
            
            case .Total:
                return 1
            
            case .PaymentMethod:
                
//                if bookingDetails.isPaidByWallet {
//
//                    return 2
//                }
                
                return 1
            
            case .Signature:
                return 1
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType : ReceiptDetailsSections = ReceiptDetailsSections(rawValue: indexPath.section)!
        
        switch sectionType {
            
            case .AddressDetails:
            
                let addressCell:ReceiptVCAddressDetailsCell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as! ReceiptVCAddressDetailsCell
                
                addressCell.addressLabel.text = bookingDetails.address1
                
                return addressCell
            
            case .FeesDetails:
                
                let paymentBreakdownCell:CBPaymentBreakDownCell = tableView.dequeueReusableCell(withIdentifier: "paymentBreakdownCell", for: indexPath) as! CBPaymentBreakDownCell
                
                paymentBreakdownCell.titleLabel.text = (bookingDetails.arrayOfFees[indexPath.row].keys.first)!
                
                paymentBreakdownCell.amountLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:"%.2f", (bookingDetails.arrayOfFees[indexPath.row].values.first)! as! CVarArg), currencySymbol: bookingDetails.currencySymbol)
                
                return paymentBreakdownCell

            case .Total:
            
                let paymentTotalCell:CBTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "paymentTotalCell", for: indexPath) as! CBTotalTableViewCell
                
                let totalValue =  bookingDetails.bookingTotalAmount
                
                paymentTotalCell.totalValueLabel.text = Helper.getValueWithCurrencySymbol(data: String(format:" %.2f",totalValue), currencySymbol: bookingDetails.currencySymbol)
                
                return paymentTotalCell
                
            case .PaymentMethod:
                
                if bookingDetails.isPaidByWallet {
                    
                    if twoPaymentDetailsCell == nil {
                        
                        twoPaymentDetailsCell = tableView.dequeueReusableCell(withIdentifier: "twoPaymentMethodCell", for: indexPath) as! BDTwoPaymentDetailsTableViewCell
                        
                        twoPaymentDetailsCell.showPaymentMethodDetails(bookingDetails: bookingDetails)
                    }
                    
                    return twoPaymentDetailsCell
                    
                } else {
                    
                    if paymentDetailsCell == nil {
                        
                        paymentDetailsCell = tableView.dequeueReusableCell(withIdentifier: "paymentMethodCell", for: indexPath) as! BDPaymentDetailsTableViewCell
                        
                        paymentDetailsCell.showPaymentMethodDetails(bookingDetails: bookingDetails)
                        
                    }
                    
                    return paymentDetailsCell
                }
            
            case .Signature:
                
                if signatureCell == nil {
                    
                    signatureCell = tableView.dequeueReusableCell(withIdentifier: "signatureCell", for: indexPath) as! ReceiptVCSignatureTableCell
                    
                    signatureCell.showSignatureDetails(bookingDetails: bookingDetails)
                    
                }
                
                return signatureCell
        }
    }
}

extension ReceiptDetailsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let sectionType : ReceiptDetailsSections = ReceiptDetailsSections(rawValue: indexPath.section)!
        
        switch sectionType {
            
            case .AddressDetails:
                
                return UITableViewAutomaticDimension
                
            case .PaymentMethod:
                
                if bookingDetails.isPaidByWallet {
                    
                    return 120
                    
                } else {
                    
                    if bookingDetails.paymentTypeValue == 2 {
                        
                        return 80
                        
                    } else {
                        
                        return 40
                    }
                    
                }
            
            case .Signature:
                
                return 140
            
            default:
                return 30
                
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let sectionType : ReceiptDetailsSections = ReceiptDetailsSections(rawValue: section)!

        switch sectionType {
            
            case .AddressDetails, .Total:
                
                return 0.0
            
            case .FeesDetails, .PaymentMethod, .Signature:
                return 30
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if section == ReceiptDetailsSections.PaymentMethod.rawValue || section == ReceiptDetailsSections.Signature.rawValue {
            
            return 0.0
            
        } else if section == ReceiptDetailsSections.Total.rawValue {
            
            return 10.0
        }
        
        return 20.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell:BDHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! BDHeaderTableViewCell
        
        let sectionType : ReceiptDetailsSections = ReceiptDetailsSections(rawValue: section)!
        
        switch sectionType {
            
            case .AddressDetails, .Total:
                
                let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
                return view
            
            case .FeesDetails:
                headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.YourPaymentBreakDown
                return headerCell.contentView
            
            case .PaymentMethod:
                headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.PaymentMethod
                return headerCell.contentView
            
            case .Signature:
                headerCell.titleLabel.text = ALERTS.BOOKING_FLOW.TABLE_HEADER_TITLES.Signature
                return headerCell.contentView
        }
        
        

    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if section == ReceiptDetailsSections.PaymentMethod.rawValue || section == ReceiptDetailsSections.Signature.rawValue {
            
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
            return view
            
        } else {
            
            let footerCell:BDFooterTableViewCell = tableView.dequeueReusableCell(withIdentifier: "footerCell") as! BDFooterTableViewCell
            
            if section == ReceiptDetailsSections.Total.rawValue {
                
                footerCell.divider.isHidden = true
            }
            
            return footerCell.contentView
            
        }
        
    }
    
    
}
