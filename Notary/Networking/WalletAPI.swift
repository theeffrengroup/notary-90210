//
//  WalletAPI.swift
//  FlagitDriver
//
//  Created by Rahul Sharma on 03/04/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class WalletAPI {
    
    let disposebag = DisposeBag()
    let getWalletDetails_Response = PublishSubject<APIResponseModel>()
    let getWalletTransactions_Response = PublishSubject<APIResponseModel>()
    let rechargeWallet_Response = PublishSubject<APIResponseModel>()

    
    /// Method to call Get Wallet Details Service API
    func getWalletDetailsServiceAPICall(){
        
        let strURL = API.BASE_URL + API.METHOD.WALLET_DETAILS
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)

        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.GetWalletDetails)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    /// Method to Recharge Wallet Service API
    ///
    /// - Parameter cardTokenId: card token to add
    func rechargeWalletServiceAPICall(cardId:String,
                                      amount:String){
        
        let strURL = API.BASE_URL + API.METHOD.RECHARGE_WALLET
        
        let requestParams: [String: Any] = [
            
            SERVICE_REQUEST.WALLET.cardId:cardId,
            SERVICE_REQUEST.WALLET.amount:amount
        ]
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)

        RxAlamofire
            .requestJSON(.post, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.RechargeWallet)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
 
    
    /// Method to Get Wallet Transactions service API
    ///
    /// - Parameter cardId: card id to delete
    func getWalletTransactionsServiceAPICall(pageIndex:Int){
        
        let strURL = API.BASE_URL + API.METHOD.WALLET_TRANSACTION + "/\(pageIndex)"
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)

        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.GetWalletTransaction)
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
   
    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
                
            case HTTPSResponseCodes.BadRequest.rawValue:
                
                Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
                break
                
            case HTTPSResponseCodes.InternalServerError.rawValue:
                
                Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
                break
                
            case HTTPSResponseCodes.UserLoggedOut.rawValue:
                
                Helper.logOutMethod()
                if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                    
                    Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                    
                }
                break
                
                
            default:
                
                let responseModel:APIResponseModel!
                responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
                
                switch requestType {
                    
                    case .GetWalletDetails:
                        
                        self.getWalletDetails_Response.onNext(responseModel)
                    
                    case .RechargeWallet:
                        
                        self.rechargeWallet_Response.onNext(responseModel)
                    
                    case .GetWalletTransaction:
                        
                        self.getWalletTransactions_Response.onNext(responseModel)
                    
                    default:
                        break
                }
                
                break
        }
    }
 
}

