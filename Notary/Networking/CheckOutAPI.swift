//
//  CheckOutAPI.swift
//  Notary
//
//  Created by Rahul Sharma on 14/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class CheckOutAPI {
    
    let disposebag = DisposeBag()
    let getServices_Response = PublishSubject<APIResponseModel>()
    let getCurrentCart_Response = PublishSubject<APIResponseModel>()
    let addOrRemoveServiceFromCart_Response = PublishSubject<APIResponseModel>()

    
    
    /// Method to get Notary Services API
    ///
    /// - Parameter confirmBookingModel: model contails livebooking request details
    func getServicesServiceAPICall(confirmBookingModel:ConfirmBookingModel){
        
        var strURL = API.BASE_URL + API.METHOD.GET_SERVICES + "/\(confirmBookingModel.categoryId)"
        
        if confirmBookingModel.bookingModel == BookingModel.OnDemand {

            strURL = strURL + "/0"

        } else {

            strURL = strURL + "/\(confirmBookingModel.providerModel.providerId)"
        }
        
        Helper.showPI(_message: PROGRESS_MESSAGE.Loading)
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.GetServices)
                    
                } else {
                    
                    Helper.hidePI()
                }

                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    func getCurrentCartDetailsServiceAPICall(confirmBookingModel:ConfirmBookingModel){
        
        var strURL = API.BASE_URL + API.METHOD.GET_CART_DETAILS + "/\(confirmBookingModel.categoryId)"
        
        if confirmBookingModel.bookingModel == BookingModel.OnDemand {
            
            strURL = strURL + "/0"
            
        } else {
            
            strURL = strURL + "/\(confirmBookingModel.providerModel.providerId)"
        }
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.GetCurrentCartDetails)
                    
                }
                    
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    func addOrRemoveServicesFromCartServiceAPICall(confirmBookingModel:ConfirmBookingModel,
                                         serviceId:String,
                                         quantity:Int,
                                         action:Int){ //action 1:incress 2:decress 0:remove
        
        let strURL = API.BASE_URL + API.METHOD.ADD_OR_DELETE_SERVICES_FROM_CART
        
        var requestParams:[String : Any] = [
            
            SERVICE_REQUEST.ADD_OR_REMOVE_SERVICE_FROM_CART.categoryId:confirmBookingModel.categoryId,
            SERVICE_REQUEST.ADD_OR_REMOVE_SERVICE_FROM_CART.serviceId:serviceId,
            SERVICE_REQUEST.ADD_OR_REMOVE_SERVICE_FROM_CART.quntity:quantity,
            SERVICE_REQUEST.ADD_OR_REMOVE_SERVICE_FROM_CART.action:action,
            
        ]
        
        if confirmBookingModel.bookingModel == BookingModel.OnDemand {
            
            requestParams[SERVICE_REQUEST.ADD_OR_REMOVE_SERVICE_FROM_CART.providerId] = ""
            
        } else {
            
            requestParams[SERVICE_REQUEST.ADD_OR_REMOVE_SERVICE_FROM_CART.providerId] = confirmBookingModel.providerModel.providerId
        }
        
        
        var serviceType:HTTPMethod!
        serviceType = .post

        if action == 1 {
        
            Helper.showPI(_message: PROGRESS_MESSAGE.AddingService)
            
        } else {
            
            Helper.showPI(_message: PROGRESS_MESSAGE.RemovingService)
        }
        

        
        RxAlamofire
            .requestJSON(serviceType,
                         strURL,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.AddOrRemoveServiceFromCart)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            if requestType == RequestType.GetServices {
                
                Helper.hidePI()
            }
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            if requestType == RequestType.GetServices {
                
                Helper.hidePI()
            }
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            if requestType == RequestType.GetServices {
                
                Helper.hidePI()
            }
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
                case .GetServices:
                    
                    self.getServices_Response.onNext(responseModel)
                
                case .GetCurrentCartDetails:
                    
                    self.getCurrentCart_Response.onNext(responseModel)
                
                case .AddOrRemoveServiceFromCart:
                
                    self.addOrRemoveServiceFromCart_Response.onNext(responseModel)
                
                default:
                    break
            }
            
            break
        }
    }
    
}
