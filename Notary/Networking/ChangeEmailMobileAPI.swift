//
//  ChangeEmailAPI.swift
//  LiveM
//
//  Created by Rahul Sharma on 22/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class ChangeEmailMobileAPI {
    
    let disposebag = DisposeBag()
    let changeEmail_Response = PublishSubject<APIResponseModel>()
    let changePhoneNumber_Response = PublishSubject<APIResponseModel>()
    
   
    
    /// Method to call Change Email Service API
    ///
    /// - Parameter emailText: emailt to change
    func changeEmailServiceAPICall(emailText:String){
        
        let strURL = API.BASE_URL + API.METHOD.CHANGE_EMAIL
        
        let requestParams:[String:Any] = [
            
            SERVICE_REQUEST.OTP.userType     : "1" ,
            SERVICE_REQUEST.Validation.Email : emailText
        ]
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.ChangingEmail)
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.updateEmail)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    
    
    /// Method to call change phone number service API
    ///
    /// - Parameters:
    ///   - countryCode: countrycode of changing phone number
    ///   - phoneNumberText: phone number to change
    func changePhoneNumberServiceAPICall( countryCode:String, phoneNumberText:String){
        
        let strURL = API.BASE_URL + API.METHOD.CHANGE_MOBILE
        
        let requestParams:[String:Any] = [
            
            SERVICE_REQUEST.OTP.userType     : "1" ,
            SERVICE_REQUEST.Validation.Country_Code : countryCode,
            SERVICE_REQUEST.Validation.Phone :phoneNumberText,
        ]
        
        
        Helper.showPI(_message: PROGRESS_MESSAGE.ChangingPhoneNumber)
        
        RxAlamofire
            .requestJSON(.patch, strURL ,
                         parameters:requestParams,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict, requestType: RequestType.updateMobile)
                    
                }
                
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.hidePI()
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }

    
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    ///   - requestType: service Request type
    func checkResponse(statusCode:Int, responseDict: [String:Any], requestType:RequestType){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            Helper.alertVC(title: ALERTS.Error , message: responseDict[SERVICE_RESPONSE.ErrorMessage] as! String)
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            
            switch requestType {
                
            case .updateEmail:
                
                self.changeEmail_Response.onNext(responseModel)
                
            case .updateMobile:
                
                self.changePhoneNumber_Response.onNext(responseModel)
                
            default:
                break
            }
            
            break
        }
    }
    
}
