//
//  APIParameterConstants.swift
//  DayRunner
//
//  Created by Raghavendra V on 16/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

enum LoginType: Int {
    
    case Default = 1
    case Facebook = 2
    case Google = 3
}

enum BookingType: Int {
    
    case Default = 1
    case Schedule = 2
}

enum BookingModel: Int {
    
    case OnDemand  = 1
    case MarketPlace = 2
}

enum CurrencyPrefixOrSufix: String {
    
    case Prefix  = "Prefix"
    case Suffix = "Suffix"
}

enum HTTPSResponseCodes: Int {
    
    case TokenExpired = 440
    case UserLoggedOut = 498
    case SuccessResponse = 200
    case BadRequest = 400
    case InternalServerError = 500
    
    enum Login:Int {
        
        case WrongPassword = 401
        case EmailNotRegistered = 404
        case SuccessResponse = 200
    }
    
    enum Register:Int {
        
        case WrongPhoneNumber = 406
        case MissingPasswordFBLogin = 410
        case WrongInputData = 412
        case SuccessResponse = 200
    }

    enum ChangeEmailMobile:Int {
        
        case EmailMobileAlreadyExist = 412
        case TokenExpired = 440
        case SuccessResponse = 200
    }

    enum ForgotPassword:Int {
        
        case EmailMobileNotExist = 404
        case WrongPhoneNumber = 406
        case WrongEmail = 409
        case ToManyRequest = 429
        case SuccessResponse = 200
        
    }
    
    enum SetPassword:Int {
        
        case UserNotExist = 404
        case SuccessResponse = 200
        case TokenExpired = 440
    }

    enum MusicianList:Int {
        
        case NoMusicians = 404
    }
    
    enum Cart:Int {
        
        case CartNotFound = 416
        case CartAlreadyCreated = 201
    }
    
    enum LastDue:Int {
        
        case NoDues = 410
    }
}



enum RegisterType: Int {
    
    case Default = 1
    case Facebook = 2
    case Google = 3
}

enum DeviceType: Int {
    
    case IOS = 1
    case Android = 2
    case Web = 3
}


struct API
{
    static let BASE_IP              = "https://api.90210notary.com"
        
    static let BASE_URL             = "https://api.90210notary.com/"
    
    static let CHAT_URL             = "http://18.188.184.181:8899/"
    
    struct SOCKET {
        
        static let URL = BASE_IP
        static let PORT_NUMBER              = "9999"
        
        struct CHANNEL {
            static let CUSTOMER_STATUS      = "CustomerStatus"
            static let MESSAGE              = "Message"
        }
    }
    
    
    
    //API Method Names
    struct METHOD {
        
        static let Customer                    =   "customer/"
        
        //Master Login
        static let LOGIN                    =   Customer + "signIn"
        
        //Master signup
        static let SIGNUP                   =   Customer + "registerUser"

        //Customer getProfile or UpdateProfile
        static let GET_PROFILE              =   PROFILE + "me"
        
        //Customer Music Genres
        static let GET_MUSIC_GENRES         =   Customer + "serviceCateogries"
        
        static let PHONE_VALIDATION         =   Customer + "phoneNumberValidation"
        
        static let EMAIL_VALIDATION         =   Customer + "emailValidation"

        
        static let GET_ALL_CATEGORIES       =   Customer + "categories"
        
        static let GET_ALL_MUSICIANS        =   Customer + "provider"
        
        static let ALL_PROVIDERS_POST       =   Customer + "location"
        
        static let ADDRESS                  =   Customer + "address"
        
        
        static let GET_SERVICES             =   Customer + "services"

        static let GET_CART_DETAILS         =   Customer + "cart"

        static let ADD_OR_DELETE_SERVICES_FROM_CART     =   Customer + "cart"

        static let DELETE_CART =   Customer + "cart"

        
        static let LIVE_BOOKING             =   Customer + "booking"
        
        static let SCHEDULE_BOOKING         =   Customer + "schedule"
        
        static let LAST_DUES                =   Customer + "lastDues"
        
        static let VALIDATE_PROMO_CODE      =   Customer + "promoCodeValidation"

        
        static let PROFILE                  =   Customer + "profile/"
        
        static let CHECKPASSWORD            =   Customer + "checkpassword/"
        
        static let UPDATEPROFILE            =   Customer + "profile/me"
        
        static let CANCELBOOKING            =   Customer + "cancelBooking"
        
        static let GET_MUSICIAN_DETAILS     =   Customer + "providerDetails"
        
        
        static let SIGNUP_RESEND_OTP        =   Customer + "resendOtp"
        
        static let SIGNUP_VERIFYI_OTP       =   Customer + "verifyPhoneNumber"
        
        
        static let GET_BOOKINGS             =   Customer + "bookings"
        
        static let GET_BOOKING_DETAILS      =   Customer + "booking" //Customer/booking/{bookingId}/{num} :- 1:pending job 2:Upcoming job 3:Past job details
        
        static let GET_PENDING_REVIEWS      =   Customer + "reviewAndRatingPending"

        
        static let GET_BOOKING_DETAILS_INVOICE =   Customer + "booking/invoice"
        
        
        
        //Wallet
        static let RECHARGE_WALLET          = Customer + "wallet/recharge"
        
        static let WALLET_DETAILS           = Customer + "wallet"
        
        static let WALLET_TRANSACTION       = Customer + "wallet/transction"

        
        
        static let paymentGateway           =   ""//"paymentGateway/"
        
        static let ADD_CARD                 =   paymentGateway + "card"
        
        static let DELETE_CARD              =   paymentGateway + "card"
        
        static let GET_CARDS                =   paymentGateway + "card"//"cards"
        
        static let CARD_DEFAULT             =   paymentGateway + "card"//"card/default"
        
        
        
        
//        static let App                      =   "app/"
        
        static let SERVERTIME               =   "server/serverTime"
        
        static let CONFIG                   =   Customer + "config"
        
        static let SEND_OTP                 =   Customer + "signupOtp"
        
        
        static let CHANGE_EMAIL             =   Customer + "email"
        
        static let CHANGE_MOBILE            =   Customer + "phoneNumber"
        

        
        static let FORGET_PASSWORD          =   Customer + "forgotPassword"
        
        static let RESEND_OTP               =   Customer + "resendOtp"

        
        static let FORGET_PASSWORD_VERIFY_OTP   =   Customer + "verifyVerificationCode"
        
        
        static let CHANGE_PASSWORD          =   Customer + "password"
        
        static let UPDATE_PASSWORD          =   Customer + "password/me"
        
        static let ACESS_TOKEN              =   Customer + "accessToken"
        
        
        
        static let LOGOUT                   =   Customer + "logout"
        
        static let RATECARD                 =   Customer + "rateCard"
        
        static let CANCEL_REASONS           =   Customer + "cancelReasons"
        
        
        static let SUPPORT                  =   Customer + "support/1"
        
        
        static let thirdParty               =   "thirdParty/"
        
        static let GOOGLE                   =   thirdParty + "google"
        
        static let RIDEFAREESTIMATE         =   thirdParty + "rideFareEstimate"
        
        static let VALIDATE_PROMOCODE       =   Customer + "validatepromocode"
        
        static let GET_REFERRAL             =   Customer + "referralCode"
        
        static let VALIDATE_REFERRAL        =   Customer + "referralCodeValidation"
        
        
        static let ADD_ADDRESS              =   Customer + "address"
        
        static let DELETE_ADDRESS           =   Customer + "address"
        
        static let GET_ADDRESS              =   Customer + "address"
        
        static let UPDATE_ADDRESS           =   Customer + "address"
        
        
        static let REVIEWANDRATING          =   Customer + "reviewAndRating"
        
        
        static let REVIEW_AND_RATING_PROVIDER =   Customer + "providerReview"

        
        //ZenDesk
        static let ZenDesk                  =   "zendesk/"
    
        static let GetTicketDetails         =   ZenDesk + "user/ticket/"
        
        static let CreateTicket             =   ZenDesk + "ticket"
        
        static let GetTicketHistoryDetails  =   ZenDesk + "ticket/history/"

        static let CreateTicketComments     =   ZenDesk + "ticket/comments"
        
        static let GET_DISTANCE_ETA_GOOGLE  = "https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&key=%@"

    }
}




struct SERVICE_REQUEST {
    
    //Login & Signup
    static let Email_Phone     = "emailOrPhone" //email or phone number
    static let Password        = "password"
    static let DeviceID        = "deviceId"
    static let PushToken       = "pushToken"
    static let AppVersion      = "appVersion"
    static let DeviceMake      = "devMake"
    static let DeviceModel     = "devModel"
    static let DeviceType      = "devType" //1- IOS , 2- Android, 3- Web
    static let DeviceTime      = "deviceTime"
    static let LoginType       = "loginType" //1- normal login, 2- Fb , 3-google
    static let DeviceOSVersion = "deviceOsVersion"
    static let FaceBookID      = "facebookId"
    static let Change_Password = "password"
    static let New_Password    = "newPassword"
    static let Old_Password    = "oldPassword"
    static let IPAddress       = "ipAddress"

    
    //Register
    static let RegisterType    = "loginType" //1- normal login, 2- Fb , 3-google
    static let FirstName       = "firstName"
    static let LastName        = "lastName"
    static let Mobile          = "phone"
    static let CountryCode     = "countryCode"
    static let ZipCode         = "ent_zipcode"
    static let Lat             = "latitude"
    static let Long            = "longitude"
    static let ProfilePic      = "profilePic"
    static let DOB             = "dateOfBirth" //YYYY-MM-D
    static let DOB_FORMAT      = "YYYY-MM-DD"
    static let FacebookId      = "facebookId"
    static let GoogleId        = "googleId"
    static let TermsAndCondition = "termsAndCond" //0 - false,1 - true
    static let ReferralCode    = "referralCode"
    
    //Edit profile
    static let About           = "about"
    static let Genres          = "preferredGenres"
    
    
    //Validate Email & Phone
    static let validationType       = "validationType" //1- email, 2- mobile
    
    //Validate Email & Phone
    struct Validation {
         static let Country_Code       = "countryCode"
         static let Phone              = "phone"
         static let Email              = "email"
    }
    
    struct Profile {
        
        static let Password        = "password"
        static let Email           = "email"
        static let Mobile          = "mobile"
        static let Token           = "token"
        static let ProfilePic      = "profilePic"
        static let Name            = "fName"
    }
    
    
    struct validatePhoneNumber {
        static let Email           = "email"
        static let mobile          = "mobile"
    }
    
    
    struct OTP {
        static let Email_Phone     = "emailOrPhone"
        static let mobile          = "mobile"
        static let userType        = "userType" //1- Customer , 2- Master
        static let processType     = "processType" //1- forgotpassword , 2- Normal Signup & change number
        static let code            = "code"
        static let CountryCode     = "countryCode"
        static let userId          = "userId"
        static let trigger         = "trigger" //1 - Register,2 - Forgot Password,3-change number

    }
    
    
    struct Referal {
        
        static let Code            = "referralCode"
        static let UserType        = "userType"
        static let Lat             = "lat"
        static let Long            = "long"
    }
    
    
    struct ForgetPassword {
        
        static let Email_Phone     = "emailOrPhone" //email or phone number
        static let userType        = "userType" //1- Customer , 2- Master
        static let processType     = "type" //1- mobile , 2- email
    }
    
    
    struct Payment {
        
        static let cardToken       = "cardToken"
        static let cardId          = "cardId"
        static let email           = "email"
    }

    
    struct Address {
        
        static let addressId       = "id"
        static let address1        = "addLine1"
        static let address2        = "addLine2"
        static let city            = "city"
        static let state           = "state"
        static let country         = "country"
        static let pincode         = "pincode"
        static let latitude        = "latitude"
        static let longitude       = "longitude"
        static let taggedAs        = "taggedAs"
        static let userType        = "userType" //1- Customer , 2- Master
    }
    
    struct GetMusician {
        
        static let latitude       = "lat"
        static let longitude      = "long"
        static let bookingType    = "bookingType" //1- now , 2- scheduled
        static let iPAddress      = "ipAddress"
        static let categoryId     = "categoryId"
    }
    
    
    struct ADD_OR_REMOVE_SERVICE_FROM_CART {
        
        static let categoryId      = "categoryId"
        static let serviceId       = "serviceId"
        static let quntity         = "quntity"
        static let action          = "action" //1:incress 2:decress 0:remove
        static let providerId      = "providerId"
        
    }
    
    
    
    struct LiveBooking {
        
        static let bookingModel    = "bookingModel" //1- Market Place, 2- on-demand , 3-bid
        static let bookingType     = "bookingType" //1- now , 2- scheduled
        static let paymentMethod   = "paymentMethod" //1- cash, 2- card, 3-wallet
        static let address1        = "addLine1"
        static let address2        = "addLine2"
        static let city            = "city"
        static let state           = "state"
        static let country         = "country"
        static let pincode         = "pincode"
        static let latitude        = "latitude"
        static let longitude       = "longitude"
        static let providerId      = "providerId"
        static let gigTimeId       = "gigTimeId"
        static let eventId         = "eventId"
        static let promoCode       = "promoCode"
        static let paymentCardId   = "paymentCardId"
        
        static let categoryId      = "categoryId"
        static let cartId          = "cartId"
        static let jobDescription  = "jobDescription"
        static let deviceDateAndTime = "deviceTime"

    }
    
    
    struct ScheduleBooking {
        
        static let latitude        = "latitude"
        static let longitude       = "longitude"
        static let providerId      = "providerId"
        static let scheduleDate    = "bookingDate"
        static let scheduleTime    = "scheduleTime"
        static let deviceDateAndTime = "deviceTime"

    }
    
    struct Promocode {
        
        static let promoCode       = "couponCode"
        static let latitude        = "latitude"
        static let longitude       = "longitude"
        static let cartId          = "cartId"
        static let paymentMethod   = "paymentMethod"
        
    }
    
    struct SUBMIT_REVIEW {
        
        static let bookingId       = "bookingId"
        static let ratingValue     = "rating"
        static let reviewText      = "review"
    }
    
    struct CANCEL_BOOKING {
        
        static let bookingId       = "bookingId"
        static let reasonId        = "resonId"
    }

    struct ZENDESK {
        
        static let ticketId        = "id"
        static let ticketCommentBody = "body"
        static let ticketAuthorId  = "author_id"
        static let ticketSubject   = "subject"
        static let ticketStatus    = "status"
        static let ticketPriority  = "priority"
        static let ticketType      = "type"
        static let ticketGroupId   = "group_id"
        static let ticketRequesterId  = "requester_id"
        static let ticketAssignerId   = "assignee_id"
    }
    
    struct WALLET {
        
        static let cardId          = "cardId"
        static let amount          = "amount"
    }
    
}

struct SERVICE_RESPONSE {
    
    static let Error               = "error"
    static let ErrorMessage        = "message"
    
    static let FAQTileName         = "Name"
    static let FAQTileLink         = "link"
    static let FAQSubCat           = "subcat"
    
    static let DataResponse        = "data"
    static let CategoryArrayResponse = "catArr"
    static let CityDataResponse    = "cityData"


    //MyProfile Service Response
    static let About               = "about"
    static let Dob                 = "dateOfBirth"
    static let Genres              = "preferredGenres"
    static let Photo               = "profilePic"
    static let CountryCode         = "countryCode"
    
    
    //Login & signup Service Response
    static let Sid                 = "sid"
    static let Email               = "email"
    static let UserPassword        = "userPassword"
    static let PhoneNumber         = "phone"
    static let SessionToken        = "token"
    static let ReferralCode        = "referralCode"
    static let CurrencyCode        = "currencyCode"
    static let ProfilePic          = "profilePic"
    static let FirstName           = "firstName"
    static let LastName            = "lastName"
    static let StripeAPIKey        = "PublishableKey"
    static let FCMTopic            = "fcmTopic"
    static let ZenDeskRequesterID  = "requester_id"
    
    
    //Token
    static let Token               = "token"
    
    //Music Genres 
    static let Genres_Name         = "catName"
    static let Genres_Id           = "id"
    
    //Review
    static let reviews             = "reviews"
    static let reviewCount         = "reviewCount"
    static let averageRating       = "averageRating"
}


//MQTT Constants
struct MQTT_TOPIC {

    static let ListAllProviders    = "provider/"
    static let GetJobStatus        = "jobStatus/"
    static let ProviderLiveTrack   = "liveTrack/"
    static let FCMTopic            = "/topics/"
    static let ChatMessageTopic    = "message/"
}

//WebService Request Types
enum RequestType : Int {
    case updateEmail
    case updateMobile
    case updateAppVersion
    case verifyMobileNumber
    case verifyReferralCode
    case verifyEmail
    case sendOTP
    case signUpOTP
    case verifyOTP
    case forgotPasswordVerifyOTP
    case signUp
    case signIn
    case getAllApptDetails
    case getAllOngoingApptDetails
    case getParticularApptDetails
    case getInvoiceDetails
    case getChatMessages
    case sendChatMessages
    case updateReview
    case getProfileData
    case updateProfileData
    case forgetPassword
    case forgotPasswordVerifyPhoneNumber
    case changePassword
    case signOut
    case liveBooking
    case getCancelReasons
    case cancelBooking
    case addCard
    case cardDefault
    case getCardDetails
    case deleteCard
    case checkCoupon
    case addAddress
    case deleteAddress
    case GetAddress
    case UpdateAddress
    case GetSupportDetails
    case ChangeBookingStatus
    case musicGenres
    case refressAccessToken
    case updatePassword
    case getAllCategories
    case getAllMusicians
    case getProviderDetails
    case reviewAndRating
    case GetReviews
    case GetDistanceAndETA
    case GetPendingReviews
    
    case GetServices
    case GetCurrentCartDetails
    case AddOrRemoveServiceFromCart
    
    case AddServiceToCart
    case RemoveServiceFromCart

    case ScheduleBookingDetails
    case CustomerLastDues
    case ValidatePromoCode

    case GetWalletDetails
    case RechargeWallet
    case GetWalletTransaction

    case GetServerTime
    
    case GetReferralCode
    
    case GetConfig

}

