//
//  ReferralCodeAPI.swift
//  Notary
//
//  Created by 3Embed on 23/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import Alamofire

class ReferralCodeAPI {
    
    let disposebag = DisposeBag()
    let referralCode_Response = PublishSubject<APIResponseModel>()
    
    
    /// Method to call get Referral Code Service API
    func getReferralCodeServiceAPICall(){
        
        let strURL = API.BASE_URL + API.METHOD.GET_REFERRAL
        
        RxAlamofire
            .requestJSON(.get, strURL ,
                         parameters:nil,
                         encoding:JSONEncoding.default,
                         headers: NetworkHelper.sharedInstance.getAOTHHeader())
            .subscribe(onNext: { (r, json) in
                
                print("API Response \(strURL)\nStatusCode:\(r.statusCode)\nResponse:\(json)")
                if  let dict  = json as? [String:Any]{
                    
                    let statuscode:Int = r.statusCode
                    self.checkResponse(statusCode: statuscode, responseDict: dict)
                }
                Helper.hidePI()
                
            }, onError: {  (error) in
                
                Helper.hidePI()
                print("API Response \(strURL)\nError:\(error.localizedDescription)")
                Helper.alertVC(title: ALERTS.Error , message: error.localizedDescription)
                
            }).disposed(by: disposebag)
        
        
    }
    
    /// Method to parse Service API Response
    ///
    /// - Parameters:
    ///   - statusCode: HTTPS Response status code
    ///   - responseDict: service response dictionary
    func checkResponse(statusCode:Int, responseDict: [String:Any]){
        
        switch statusCode {
            
        case HTTPSResponseCodes.BadRequest.rawValue:
            
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
        case HTTPSResponseCodes.InternalServerError.rawValue:
            
            if let errorMessage = responseDict[SERVICE_RESPONSE.ErrorMessage] as? String {
                
                Helper.showAlert(head: ALERTS.Error, message: errorMessage)
                
            }
            break
            
        case HTTPSResponseCodes.UserLoggedOut.rawValue:
            
            Helper.logOutMethod()
            
            break
            
            
        default:
            
            let responseModel:APIResponseModel!
            responseModel = APIResponseModel.init(statusCode: statusCode, dataResponse: responseDict)
            self.referralCode_Response.onNext(responseModel)
            break
        }
    }
    
}
